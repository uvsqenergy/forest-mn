#ifndef FORESTMN_DVFSUNITUTILS_H
#define FORESTMN_DVFSUNITUTILS_H

#include "libdvfs/libdvfs.h"
#include "glog/logging.h"

struct FreqInfo {
   unsigned int frequency;
   float power;
};

class DVFSUnitUtils {
public:
   /**
    * Constructor
    *
    * @param The libdvfs structure giving information about the
    * frequency domain the MPI process is running on
    */
   DVFSUnitUtils (dvfs_unit *unit);
   ~DVFSUnitUtils () {}

   static unsigned int freq_max_id;

   /**
    * Returns the power computed in the offline phase for a given frequency
    * id
    *
    * @param freq_id The frequency id corresponding to the frequency requested
    * in the sorted frequency list of the frequency domain the MPI process is
    * running on
    */
   inline float get_power (unsigned int freq_id) const{
      CHECK (freq_id < freqInfo_.size ()) << "Error: Invalid frequency id #" << freq_id << "/" << freqInfo_.size ();
      return freqInfo_ [freq_id].power;
   }

   /**
    * Returns the frequency corresponding to the frequency id provided
    * in the sorted frequency list of the frequency domain the MPI process is
    * running on
    */
   inline unsigned int get_freq (unsigned int freq_id) const{
      CHECK (freq_id < freqInfo_.size ()) << "Error: Invalid frequency id #" << freq_id << "/" << freqInfo_.size ();
      return freqInfo_ [freq_id].frequency;
   }

   /**
    * Returns the number of frequencies able to be set on the considered DVFS
    * unit
    */
   inline unsigned int get_nb_freqs () const{
      return freqInfo_.size ();
   }

   /**
    * Returns the id of the DVFS Unit in the DVFS context, as described by libdvfs
    */
   inline unsigned int get_id () const{
      return unit_->id;
   }

   inline void set_freq (unsigned int freq_id) {
      CHECK (freq_id < freqInfo_.size ()) << "Error: Invalid freq_id (" << freq_id << "/" << freqInfo_.size () << ")";
      unsigned int freq = freqInfo_ [freq_id].frequency;
      unsigned int ret = dvfs_unit_set_freq (unit_, freq);
      CHECK (ret) << "Error: An error occured while setting frequency " << freq << " to DVFS Unit #" << unit_->id;
   }

   // TODO comment
   bool has_turbo_boost () const;

   // TODO comment
   inline unsigned int get_nb_cores () const{
      return unit_->nb_cores;
   }

   inline unsigned int get_linux_id (unsigned int core_id) const{
      CHECK (core_id < unit_->nb_cores) << "Error: Invalid core id (" << core_id << "/" << unit_->nb_cores << ")";
      return unit_->cores [core_id]->id;
   }

   /**
    * Retrieves information from the offline power file and store it in the freqInfo_
    * structure
    */
   void read_power ();

private:
   /** The vector containing information about the frequencies available on the
    * considered DVFS Unit: frequency value and power consumed (thanks to the
    * offline phase) */
   std::vector <FreqInfo> freqInfo_;

   /** The DVFS Unit information structure provided by libdvfs */
   dvfs_unit *unit_;

   /**
    * Retrieves information from libdvfs and store it in the freqInfo_ structure
    *
    * @param local_mpi_rank The MPI rank of the current process on the local node
    */
   void read_freqs ();
};

#endif
