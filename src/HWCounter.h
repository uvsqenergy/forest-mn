#ifndef FOREST_HWCOUNTERS_H
#define FOREST_HWCOUNTERS_H

#include <stdint.h>
#include <string>

class HWCounter {
   public:
      /**
       * Constructs a new hardware counter watcher.
       *
       * @param cname The hardware counter name
       * @param coreNum The processor core to watch. Can also take the special
       *  value -1 to watch all the cores used by the current process.
       */
      HWCounter(const std::string &cname, int core_num = -1);

      /**
       * Copy constructor.
       *
       * @param c The hardware counter to copy from.
       */
      HWCounter(const HWCounter &c);

      /**
       * Destructor.
       */
      ~HWCounter();

      /**
       * Assignment.
       *
       * @param c The hardware counter to copy from
       */
      HWCounter &operator= (const HWCounter &c);

      /**
       * Start measuring the value of the counter.
       */
      void start();

      /**
       * Stop measuring and return the counter value delta since the last call
       * to start.
       *
       * @return The hardware counter value evolution since the last call to
       *  start.
       */
      uint64_t stop();

      /**
       * Is the counter measured right now?
       *
       * @return True if the counter is running right now.
       */
      inline bool is_measuring() const { return is_measuring_; }

   private:
      /** Counter file descriptor */
      int fd_;

      /** Initial value of the counter when start is called. */
      uint64_t init_val_;

      /** True if the counter is measured right now */
      bool is_measuring_;
};

#endif
