#include <stdint.h>
#include <cstdlib>
#include "glog/logging.h"

#include "TaskProfiling.h"
#include "TaskGroup.h"

TaskProfiling& TaskProfiling::operator= (const TaskProfiling& tp) {
   if (this == &tp) {
      return *this;
   }
   this->start_ = tp.start_;
   this->sslack_ = tp.sslack_;
   this->scomm_ = tp.scomm_;
   this->end_ = tp.end_;
   this->iexec_comp_ = tp.iexec_comp_;
   this->iexec_comm_ = tp.iexec_comm_;
   this->egain_ = tp.egain_;

   return *this;
}

void TaskProfiling::set_end_task (uint64_t t) {
   if (sslack_ == start_) {
      set_start_slack (t);
   }
   if (scomm_ == start_) {
      set_start_comm (t);
   }

   DCHECK (t >= scomm_) << "Error: end task date (" << t << ") cannot be smaller than the corresponding start comm date (" << scomm_ << ")";

   end_ = t;
}

void TaskProfiling::print () const{
   DLOG (INFO) << "Task " << get_comp_time() << " + " << get_slack_time() << " + " << get_comm_time();
   DLOG (INFO) << "IPS: " << get_ips() << " eGain: " << get_energy_gain();
   DLOG (INFO) << "IexecComp: " << iexec_comp_ << ", IexecComm: " << iexec_comm_;
}
