#ifndef FOREST_MSG_H
#define FOREST_MSG_H

#include "Common.h"

#include <glog/logging.h>
#include <mpi.h>

template <typename T>
class Msg {
   public:

      /**
       * Empty constructor.
       */
      Msg();

      
      /**
       * Copy constructor
       */
      Msg(const Msg<T> &m);
      
      /**
       * Constructor for complete messages.
       *
       * @param id The task id (messages are often related to tasks in the task graph)
       * @param src The MPI rank of the source
       * @param dst The MPI rank of the destination
       * @param data The data carried in the message.
       */
      Msg(unsigned int id, int src, int dst, const T &data);

      /**
       * Destructor.
       */
      ~Msg();

      
      /** 
       * Assignment
       */
      Msg<T> & operator= (const Msg<T> &m);

      /**
       * Returns the message source.
       *
       * @return The MPI rank of the message source.
       */
      inline int get_src() const { return src_; }

      /**
       * Returns the message destination.
       *
       * @return The MPI rank of the message destination.
       */
      inline int get_dst() const { return dst_; }

      /**
       * Returns the id of the task related to this message.
       *
       * @return The task id enclosed in the message.
       */
      inline unsigned int get_tid() const { return id_; }

      /**
       * Returns a pointer to the data within the message.
       *
       * @return The data inside the message.
       */
      inline const T &get_data() const { return data_; }

      /**
       * Sends the message in the given communicator.
       *
       * @param c The communicator where to send the message.
       */
      void send(MPI_Comm &c);

      /**
       * Receives a message from the network.
       *
       * @param msg Where to store the data received.
       * @param c The Communicator where to wait for a message.
       * @param src The source rank from which the message must be sent.
       */
      static Msg<T> recv(MPI_Comm &c, int src = MPI_ANY_SOURCE);

   private:

      /** 
       * Message id (usually this is the number of communications with the 
       * source preceding the related task).
       */
      unsigned int id_;

      /** MPI rank of the source */
      int src_;

      /** MPI rank of the destination */
      int dst_;

      /** Message data */
      T data_;
};

template <typename T>
Msg<T>::Msg() : id_(0), src_(0), dst_(0), data_(0) {
}

template <typename T>
Msg<T>::Msg(const Msg<T> &m) : id_(m.id_), src_(m.src_), dst_(m.dst_), data_(m.data_) {
}

template <typename T>
Msg<T>::Msg(unsigned int id, int src, int dst, const T &data) :
   id_(id), src_(src), dst_(dst), data_(data)
{
}

template <typename T>
Msg<T>::~Msg() {
}

template <typename T>
Msg<T> & Msg<T>::operator= (const Msg<T> &m) {
   id_ = m.id_;
   src_ = m.src_;
   dst_ = m.dst_;
   data_ = m.data_;

   return *this;
}

template <typename T>
void Msg<T>::send(MPI_Comm &c) {
   PMPI_Send(this, sizeof(*this), MPI_CHAR, dst_, 0, c);
}

template <typename T>
Msg<T> Msg<T>::recv(MPI_Comm &c, int src) {
   Msg<T> msg;
   PMPI_Recv(&msg, sizeof(msg), MPI_CHAR, src, 0, c, MPI_STATUS_IGNORE);

   return msg;
}


#endif

