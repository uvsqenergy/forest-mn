#include "TaskSequence.h"

void TaskSequence::print() const{
   unsigned int tid = 0;
   for (unsigned int i = 0; i < tasks_groups_.size (); i++) {
      DLOG (INFO) << "Group #" << i;
      TaskGroup *tg = tasks_groups_ [i];
      DLOG (INFO) << "Group freq = " << tg->get_freq_id ();
      for (unsigned int j = 0; j < tg->get_nb_tasks (); j++) {
         Task *t = tg->get_task (j);
         DLOG (INFO) << "Task #" << tid++ << " (#" << j << " in group)";
         t->print ();
      }
   }
}
