#ifndef FORESTMN_OFFLINE_H
#define FORESTMN_OFFLINE_H

#include <stdint.h>
#include <vector>

#include "../NodeUtils.h"

struct BenchResult {
   double ratio;
   double time;
};

// TODO comment
class Offline {
   public:
      Offline ();
      ~Offline () {}

      void prompt_user () const;
      void run ();
      void save () const;

   private:
      void get_nb_iterations ();
      uint64_t nb_iterations (unsigned int ms);
      void run_benchmark (unsigned int nb_repets, unsigned int nb_cores, BenchResult& res);
      NodeUtils node_utils_;
      std::vector <unsigned int> nb_iters_;
      std::vector <BenchResult> results_;
};

#endif
