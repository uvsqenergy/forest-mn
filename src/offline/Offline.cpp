#include <stdint.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdint.h>
#include "glog/logging.h"
#include "libdvfs/libdvfs.h"

#include "../NodeUtils.h"
#include "Offline.h"
#include "utils.h"

Offline::Offline () {
   // We must have at least one unit on the local node
   DCHECK (node_utils_.nb_dvfs_units () > 0) << "Error: Architecture does not seem to have a valid dvfs unit";
}

void Offline::prompt_user () const{
   std::cout << "!" << std::endl
             << "!" << std::endl
             << "! In order to measure accurate values, please" << std::endl
             << "!        CLOSE ANY OTHER RUNNING PROGRAM" << std::endl
             << "!" << std::endl
             << "!" << std::endl
             << "Press Enter when you are ready...";
   fgetc (stdin);
}

void Offline::run () {
   // Get the appropriate number of iterations for each frequency,
   // so that we can have relevant power values
   get_nb_iterations ();

   DVFSUnitUtils *first_unit = node_utils_.get_dvfs_unit (0);
   unsigned int nb_freqs = first_unit->get_nb_freqs ();
   unsigned int nb_cores = first_unit->get_nb_cores ();
   BenchResult res;
 
   // Now we want to run the benchmark for some time and save the results in
   // the vector
   std::cout << "Launching benchmark";
   fflush (stdout);
   for (unsigned int i = 0; i < nb_freqs; i++) {
      first_unit->set_freq (i);
      run_benchmark (nb_iters_ [i], nb_cores, res);
      results_.push_back (res);
      std::cout << ".";
      fflush (stdout);
   }
   std::cout << "done" << std::endl;
}

void Offline::get_nb_iterations () {
   // We consider the architecture to be homogeneous, so each cpu is identical
   // Therefore, we compute iterations and run on the first dvfs unit
   DVFSUnitUtils *first_unit = node_utils_.get_dvfs_unit (0);
   unsigned int nb_freqs = first_unit->get_nb_freqs ();

   std::cout << "Determining optimal lPowerProbe configuration";
   fflush (stdout);

   if (first_unit->has_turbo_boost ()) {
      // Target 1 second for all frequencies but TB
      for (unsigned int i = 0; i < nb_freqs - 1; i++) {
         // Set the current frequency on the dvfs unit
         first_unit->set_freq (i);

         // Get the number of iterations for 1000ms
         uint64_t nb_iters = nb_iterations (1000);

         nb_iters_.push_back (nb_iters);
         std::cout << ".";
         fflush (stdout);
      }
      // Set freq max and target 5 seconds for turbo boost as it is not as stable as
      // other frequencies
      first_unit->set_freq (nb_freqs - 1);
      uint64_t nb_iters = nb_iterations (1000);
      nb_iters_.push_back (nb_iters);
      std::cout << ".";
      fflush (stdout);
   } else {
      // Target 1 second for all frequencies
      for (unsigned int i = 0; i < nb_freqs; i++) {
         // Set the current frequency on the dvfs unit
         first_unit->set_freq (i);

         // Get the number of iterations for 1000ms
         uint64_t nb_iters = nb_iterations (1000);

         nb_iters_.push_back (nb_iters);
         std::cout << ".";
         fflush (stdout);
      }
   }

   std::cout << "done" << std::endl;
}

uint64_t Offline::nb_iterations (unsigned int ms) {
   uint64_t nr = 10000;
   unsigned long int exectime = 0;
   BenchResult values;

   while (exectime < ms || exectime > 1.5 * ms) {
      run_benchmark (nr, 1, values);

      DCHECK (nr != 0) << "Error: number of repetitions cannot be 0";
      if (values.ratio == 0 || values.time == 0) {
         nr *= 10;
         continue;
      }

      exectime = values.time;

      if (exectime < ms) {
         // Small values are not relevant
         if (exectime < .1*ms) {
            nr *= 10;
         } else {
            // Assume linear impact of nr on exec time
            // target a little bit above t (1.1*t)
            nr *= ms / exectime;
            nr *= 1.1;
         }
      } else if (exectime > 1.5 * ms) {
         // assume linear impact of nr on exec time
         nr /= 2.;
      }
   }

   return nr;

}

void Offline::run_benchmark (unsigned int nb_repets, unsigned int nb_cores,
                         BenchResult& res) {
   std::ostringstream taskMask, cmd, libraries, lpp_path;
   unsigned int i = 0;
   DVFSUnitUtils *dvfs_unit = node_utils_.get_dvfs_unit (0);

   CHECK (nb_cores <= dvfs_unit->get_nb_cores ()) << "Error: Invalid number of cores (" << nb_cores << "/" << dvfs_unit->get_nb_cores () << ")";

   // Building the taskMask (for pinning the process on the corresponding cores)
   for (unsigned int i = 0; i < nb_cores; i++) {
      taskMask << dvfs_unit->get_linux_id (i);
      if ((i+1) < nb_cores) {
         taskMask << ";";
      }
   }
   lpp_path << "./lPowerProbe/";
   
   libraries << lpp_path.str () << "/probes/libwallclock/libwallclock.so;"
             << lpp_path.str () << "/probes/libenergymsr/libenergymsr.so;";


   cmd << lpp_path.str () << "lPowerProbe -r 5 -d " << nb_cores << " -p \"" << taskMask.str ()
       << "\" -o /tmp/results.csv -l \"" << libraries.str () << "\" src/offline/add " << nb_repets; 
   //std::cerr << cmd.str () << std::endl << std::endl;

   // Allow the user to cancel the the offline phase with Ctrl+C if needed
   usleep (10);
   
   FILE *stream = popen (cmd.str ().c_str (), "r");
   if (stream == NULL) {
      std::cerr << "Error: Could not execute command " << cmd.str () << std::endl;
      exit (EXIT_FAILURE);
   }

   char buf [256];
   while (fgets (buf, sizeof (buf), stream));
   pclose (stream);

   std::ifstream ifs ("/tmp/results.csv");
   
   if (!ifs.good ()) {
      std::cerr << "Error: Cannot open results file: /tmp/results.csv" << std::endl;
      exit (EXIT_FAILURE);
   }

   // Skip first line
   ifs.getline (buf, sizeof (buf));

   i = 0;
   std::vector <BenchResult> vres;
   while (ifs.getline (buf, sizeof (buf))) {
      std::vector <std::string> elems;
      split (buf, ';', elems); 
      assert (elems.size () >= 2);
      std::vector <double> dElems;
      vectorToDouble (elems, dElems);
      BenchResult bres;
      bres.ratio = dElems [1] / dElems [0] * 1e6;
      bres.time = dElems [0] / 1e3;

      vres.push_back (bres);
      i++;
   }
   ifs.close ();

   std::sort (vres.begin (), vres.end (), compareBenchResult);

   size_t half = vres.size () / 2;
   res.ratio = vres [half].ratio;
   res.time = vres [half].time;
}

void Offline::save () const{
   // Here we just want to print out the vector in the file
   // We assume all the cpus on the considered node are identical
   // So we print the same values for each cpu
   
   unsigned int nb_units = node_utils_.nb_dvfs_units ();
   DVFSUnitUtils *first_unit = node_utils_.get_dvfs_unit (0);
   unsigned int nb_freqs = first_unit->get_nb_freqs ();
   std::ofstream ofs("power.cfg");
   for (unsigned int i = 0; i < nb_units; i++) {
      for (unsigned int j = 0; j < nb_freqs; j++) {
         ofs << results_ [j].ratio << " ";
      }
      ofs << std::endl;
   }

   ofs.close ();
}
