#include <cstdlib>
#include "glog/logging.h"
#include "Offline.h"

int main (int argc, char *argv []) {
   google::InitGoogleLogging (argv [0]);
   google::LogToStderr ();

   // We want to write this OpenMPI value if not set
   CHECK (setenv ("OMPI_COMM_WORLD_LOCAL_RANK", "0", 0) == 0) << "Error: Cannot set environment variable";

   Offline offline;

   offline.prompt_user ();
   offline.run ();
   offline.save ();

   (void) argc;
   (void) argv;
   return 0;
}
