#include <cstdlib>

#include "glog/logging.h"
#include "libdvfs/libdvfs.h"

#include "NodeUtils.h"

NodeUtils::NodeUtils () {
   dvfs_context_ = dvfs_start (true);

   CHECK (dvfs_context_ != NULL) << "Error: Cannot initialize libdvfs";

   unsigned int ret = dvfs_set_gov (dvfs_context_, "userspace");

   CHECK (ret) << "Error: Cannot set governor: userspace";

   // Get the number of units on 
   unsigned int nb_units = dvfs_context_->nb_units;

   // Find the mpi rank on the local node
   find_local_mpi_rank ();

   // Now, we have to find the unit and the thread in the structures given by
   // libdvfs that match the local MPI rank
   bool found_thread = false;
   std::vector<dvfs_core*> libdvfs_cores;

   for (unsigned int i = 0; i < nb_units; i++) {
      dvfs_unit *unit = dvfs_context_->units [i];
      DCHECK (unit != NULL) << "Error: libdvfs return a null unit for #" << i;
      DVFSUnitUtils *new_unit = new DVFSUnitUtils (unit);
      dvfs_units_.push_back (new_unit);
      unsigned int nb_cores = unit->nb_cores;
      
      // For each core in the dvfs unit
      for (unsigned int j = 0; j < nb_cores; j++) {
         dvfs_core *core = unit->cores [j];
         DCHECK (core != NULL) << "Error: libdvfs return a null core for #" << j;
         ThreadUtils *new_thread = new ThreadUtils (*new_unit, core, local_mpi_rank_);
         
         // Push it in the thread vector
         threads_.push_back (new_thread);

         // Hey, seems like there's a match here!
         if (core->id == local_mpi_rank_ && !found_thread) {
            found_thread = true;
            running_dvfs_unit_ = new_unit;
            running_thread_ = new_thread;
         }
      }
   }

   // At this point we should have found a core matching the local mpi rank
   CHECK (found_thread) << "Error: Cannot find thread with rank #" << local_mpi_rank_;

   // Now that our Node Info is initialized, let's pin the process down on the
   // corresponding thread
   running_thread_->pin ();
}

NodeUtils::~NodeUtils () {
   // Free allocated memory
   for (unsigned int i = 0; i < dvfs_units_.size (); i++) {
      delete dvfs_units_ [i];
   }
   for (unsigned int i = 0; i < threads_.size (); i++) {
      delete threads_ [i];
   }

   // Restore the governor to ondemand
   if (dvfs_set_gov (dvfs_context_, "ondemand") == 0) {
      LOG (FATAL) << "Error: Cannot reset governor to ondemand";
   }

   // Stop the dvfs context
   dvfs_stop (dvfs_context_);
}

void NodeUtils::find_local_mpi_rank () {
   char *local_mpi_rank = getenv ("OMPI_COMM_WORLD_LOCAL_RANK"), *end;
   CHECK(local_mpi_rank != NULL) << "You do not seem to use OpenMPI";
  
   // Get local rank for this process
   // So that we can pin down the process on the corresponding local thread
   local_mpi_rank_ = strtol (local_mpi_rank, &end, 10);
   DCHECK (local_mpi_rank != end) << "Error: Cannot read the mpi rank through the environment variable OMPI_COMM_WORLD_LOCAL_RANK. Are you running the code with OpenMPI ?";
}
