#ifndef FORESTMN_MPITYPES_H
#define FORESTMN_MPITYPES_H

#include "mpi.h"

#ifndef ompi_fortran_logical_t
#define ompi_fortran_logical_t int
typedef int MPI_F_Grequest_query_function;
typedef int MPI_F_Grequest_free_function;
typedef int MPI_F_Grequest_cancel_function;
#endif

#endif // FORESTMN_MPITYPES_H
