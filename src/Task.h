#ifndef FOREST_TASK_H
#define FOREST_TASK_H

#include "Common.h"

#include <cstddef>
#include <sstream>
#include <set>
#include <glog/logging.h>

#include "TaskProfiling.h"
#include "DVFSUnitUtils.h"

class Task {
   public:
      /**
       * Standard constructor
       */
      Task();

      /**
       * Destructor.
       */
      ~Task();

      /**
       * Sets the time when the task starts to communicate.
       *
       * @param src Sources of the messages received. Pointer must be allocated
       *  with new, will be freed in this class.
       * @param dst Destinations of the messages sent. Pointer must be allocated
       *  with new, will be freed in this class.
       */
      void set_comm(const std::set<int>& src, const std::set<int>& dst);

      // TODO comment
      inline void set_comm_name (int comm_name) {
         comm_name_ = comm_name;
      }

      // TODO comment
      inline int get_comm_name () const{
         return comm_name_;
      }

      /**
       * Determines if the task ends with a send communication.
       *
       * @return True if the ending communication is a send.
       */
      inline bool is_sending() const {
         return dst_.size() > 0;
      }

      /**
       * Determines if the tasks ends with a receive communication.
       *
       * @return True if the ending communication is a receive.
       */
      inline bool is_receiving() const{
         return src_.size() > 0;
      }

      /**
       * Returns the set of nodes to which we are sending messages.
       *
       * @return The nodes to which we are sending data at the end of the task.
       */
      inline const std::set<int> &get_comm_receivers() {
         return dst_;
      }

      /**
       * Returns the set of nodes that send us data when we end.
       *
       * @return The nodes from which we are receiving data at the end of the task.
       */
      inline const std::set<int> &get_comm_emitters() {
         return src_;
      }

      /**
       * Prints the task in a stringstream
       */
      std::string printString () const;

      /**
       * Prints the task.
       */
      void print() const;

      inline TaskProfiling& get_profiling (unsigned int profiling_id) {
         DCHECK (profiling_id < profilings_.size ()) << "Error: Invalid profiling id (" << profiling_id << "/" << profilings_.size () << ")";
         return profilings_ [profiling_id];
      }

      inline void replace_profiling (const TaskProfiling& tp, unsigned int profiling_id) {
         DCHECK (profiling_id < profilings_.size ()) << "Error: Invalid profiling id (" << profiling_id << "/" << profilings_.size () << ")";
         profilings_ [profiling_id] = tp;
      }

      inline void set_max_freq (unsigned int freq_id) {
         max_freq_id_ = freq_id;
      }

      inline unsigned int get_max_freq () const{
         return max_freq_id_;
      }

      inline void set_virtual_freq_id (unsigned int freq_id) {
         virtual_freq_id_ = freq_id;
      }

      inline unsigned int get_virtual_freq_id () const{
         return virtual_freq_id_;
      }

      inline unsigned int get_min_eval_freq () const{
         int i;
         for (i = DVFSUnitUtils::freq_max_id; i >= 0; i--) {
            if (profilings_ [i].get_task_length () == 0) {
               break;
            }
         }

         return i+1;
      }

   private:
      /**
       * Disable copy constructor
       *
       * @param t Which instance to copy from
       */
      Task(const Task& t);

      /**
       * Prohibits assignment operator
       *
       * @param t The source operand.
       */
      Task &operator=(const Task &t);

      /** List of time and energy gain profilings for each frequency */
      std::vector <TaskProfiling> profilings_;

      /** Communication sources (sending to us) */
      std::set<int> src_;

      /** Communication destinations (receiving our message) */
      std::set<int> dst_;

      /** Communication name (MPI_RECV, etc...) */
      int comm_name_;

      // TODO comment
      unsigned int max_freq_id_;

      /** The task has an ideal freq considering its profiling. */
      unsigned int virtual_freq_id_;
};

#endif
