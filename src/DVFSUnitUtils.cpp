#include <vector>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstring>
#include "libdvfs/libdvfs.h"
#include "glog/logging.h"

#include "DVFSUnitUtils.h"

unsigned int DVFSUnitUtils::freq_max_id = 0;

DVFSUnitUtils::DVFSUnitUtils (dvfs_unit *unit) :
   unit_ (unit) {
   DCHECK (unit_ != NULL) << "Error: Unit cannot be NULL";

   // Retrieve the frequencies for this unit
   read_freqs ();

   // Just for convenience, we want to keep a static variable
   DVFSUnitUtils::freq_max_id = freqInfo_.size () - 1;
}

void DVFSUnitUtils::read_freqs () {
   // libdvfs gives us the list of frequencies in dvfs_core structures only
   // So let's consider the first core of the unit to retrive freq information
   const dvfs_core *core = unit_->cores [0];
   CHECK (core != NULL) << "Error: libdvfs says there is no core in the running unit (" << unit_->nb_cores << ")";
   unsigned int nb_freqs = dvfs_core_get_nb_freqs (core);

   FreqInfo freqInfo;
   freqInfo.frequency = 0;
   freqInfo.power = 0;
   
   // For each frequency in the unit, push it back in our vector
   for (unsigned int i = 0; i < nb_freqs; i++) {
      freqInfo.frequency = dvfs_core_get_freq (core, i);
      freqInfo_.push_back (freqInfo);
   }
}

void DVFSUnitUtils::read_power () {
   std::ifstream ifs("power.cfg");
   if (!ifs) {
      LOG(FATAL) << "Cannot find power.cfg";
   }

   std::string buf;

   // Go to the line corresponding to our DVFS Unit id
   unsigned int i = 0;
   while (i < unit_->id) {
      std::getline (ifs, buf);
      i++;
   }

   // Now we just have to read the corresponding power values
   for (unsigned int freqId = 0; freqId < freqInfo_.size (); freqId++) {
      float value;
      ifs >> value;

      if (!ifs) {
         LOG (FATAL) << "Unit #" << unit_->id << ": Ill-formed power.cfg file (freqId " << freqId << "/" << freqInfo_.size () << ")";
      }

      freqInfo_ [freqId].power = value;
   }
 
   ifs.close();
}

// TODO fix this functions ; it does not return whether the considered dvfs unit has tb, but rather says whether the first core on the machine has turbo boost
bool DVFSUnitUtils::has_turbo_boost () const{
   std::ifstream ifs;

   ifs.open ("/proc/cpuinfo");
   if (!ifs.good ()) {
      LOG (FATAL) << "Error: Cannot find /proc/cpuinfo" << std::endl;
   }
   
   char buf [2048];
   while (ifs.getline (buf, sizeof (buf))) {
      if (!strncmp (buf, "flags", 5)) {
         if (!strstr (buf, "ida")) {
            ifs.close ();
            return false;
         } else {
            ifs.close ();
            return true;
         }
      }
   }

   return false;

}
