#include "Common.h"
#include "LocalNode.h"

#include <glog/logging.h>
#include <cassert>
#include <cstdlib>
#include <dlfcn.h>
#include <map>
#include <mpi.h>
#include <set>
#include <set>
#include <utility>
#include <signal.h>
#include <cstring>
#include <sched.h>

#include "mpi.h"
#include "MPITypes.h"
#include "Common.h"
#include "Wrapper.h"

#if 0
#include <openmpi/openmpi/ompi_config.h>
#include <openmpi/openmpi/ompi/request/grequest.h>
#endif

#define MPI_STATUS_SIZE 6

static int *MPI_STATUS_IGNORE_F;
static int *MPI_STATUSES_IGNORE_F;
static int MPI_STATUS_SIZE_F;

extern "C" {

static inline uint64_t RequestChecksum (MPI_Request *request) {
   char *prequest = (char*)request;
   uint64_t sum = 0;

   for (unsigned int i = 0; i < sizeof (*request); i++) {
      sum += prequest [i];
   }

   return sum;
}

static inline uint64_t RequestChecksumi (int *request) {
   return *request;
}

static std::map<MPI_Request, CommInfo> req2comms;

typedef struct group2ranksCache {
   group2ranksCache() : cap(16), size(0), curLn(0) {};

   std::pair<MPI_Group, std::set<int> *> cache[16];
   const unsigned int cap;
   unsigned int size;
   unsigned int curLn;
} group2ranksCache;
static group2ranksCache g2rCache;

static bool in_loop = false;
static bool fortran = false;

/**
 * Utility function to retrieve the ranks (in the world) of processes in the
 * given group.
 *
 * @param group The group whose processes ranks are wanted.
 * @param out Where to append the result
 */
void getGroupRanks(MPI_Group group, std::set<int> &out) {
   int size;
   MPI_Group ugroup;

   for (unsigned int i = 0; i < g2rCache.size; i++) {
      if (g2rCache.cache[i].first == group) {
         out.insert(g2rCache.cache[i].second->begin(), g2rCache.cache[i].second->end());
         return;
      }
   }

   PMPI_Group_size(group, &size);
   PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);

   // fetch the processes in the group 
   int *granks = new int[size];
   int *uranks = new int[size];
   std::set<int> *unodes = new std::set<int>();

   for (int i = 0; i < size; i++) {
      granks[i] = i;
   }

   PMPI_Group_translate_ranks(group, size, granks, ugroup, uranks);
   //std::copy(uranks, uranks + size, unodes->begin());
   for (int i = 0; i < size; i++) {
      unodes->insert (uranks [i]);
   }
   delete[] granks;
   delete[] uranks;

   // we're going to reuse that, free the memory
   if (g2rCache.size >= g2rCache.cap) {
      delete g2rCache.cache[g2rCache.curLn].second;
   } else {
      // increase the size up to the capacity
      g2rCache.size++;
   }

   // remember this
   g2rCache.cache[g2rCache.curLn].first = group;
   g2rCache.cache[g2rCache.curLn].second = unodes;
   
   g2rCache.curLn = (g2rCache.curLn + 1) % g2rCache.cap;

   out.insert(unodes->begin(), unodes->end());
}

#ifdef NDEBUG
static void sigHandler (int nsig) {
   switch (nsig) {
   case SIGINT:
   case SIGTERM:
      DLOG(INFO) << "Ended by signal" << nsig << std::endl;
      LocalNode::destroy ();
      DLOG(INFO) << "LN destroyed" << std::endl;
      DLOG (FATAL) << "#" << sched_getcpu() << " catch signal SIGINT, exiting properly..." << std::endl;
      // Shouldn't occur
      exit (EXIT_FAILURE);
      break;
   default:
      DLOG (FATAL) << "Handling unknown signal, ignoring..." << std::endl;
   }
}
#endif

void FoRESTmn_newIter() {
   LocalNode::get_LN()->new_iter();
   in_loop = true;
}

void forestmn_newiter_ () {
   FoRESTmn_newIter ();
}

void FoRESTmn_endLoop() {
   in_loop = false;
}

void forestmn_endloop_ () {
   FoRESTmn_endLoop ();
}

int  MPI_Pcontrol (const int  level,  ...) {
	(void) (level);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_pcontrol_ (MPI_Fint * level) {
	const int* m_level = (const int*) level;

	MPI_Pcontrol(*m_level);
}

int  MPI_Testany (int  count, MPI_Request  array_of_requests[], int * index, int * flag, MPI_Status * status) {
	(void) (count);
	(void) (array_of_requests);
	(void) (index);
	(void) (flag);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_testany_ (void *count, void *array_of_requests, void *index, void *flag, void *status, void *ierr) {
	(void) (count);
	(void) (array_of_requests);
	(void) (index);
	(void) (flag);
	(void) (status);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Type_get_contents (MPI_Datatype  mtype, int  max_integers, int  max_addresses, int  max_datatypes, int  array_of_integers[], MPI_Aint  array_of_addresses[], MPI_Datatype  array_of_datatypes[]) {
	(void) (mtype);
	(void) (max_integers);
	(void) (max_addresses);
	(void) (max_datatypes);
	(void) (array_of_integers);
	(void) (array_of_addresses);
	(void) (array_of_datatypes);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_get_contents_ (MPI_Fint * mtype, MPI_Fint * max_integers, MPI_Fint * max_addresses, MPI_Fint * max_datatypes, MPI_Fint * array_of_integers, MPI_Aint * array_of_addresses, MPI_Fint * array_of_datatypes, MPI_Fint * ierr) {
	MPI_Datatype m_mtype = MPI_Type_f2c (*mtype);
	int* m_max_integers = (int*) max_integers;
	int* m_max_addresses = (int*) max_addresses;
	int* m_max_datatypes = (int*) max_datatypes;
	int* m_array_of_integers = (int*) array_of_integers;
	MPI_Aint* m_array_of_addresses = (MPI_Aint*) array_of_addresses;
	MPI_Datatype* m_array_of_datatypes = (MPI_Datatype*) array_of_datatypes;
	(void) (ierr);

	(*ierr) = MPI_Type_get_contents(m_mtype, *m_max_integers, *m_max_addresses, *m_max_datatypes, m_array_of_integers, m_array_of_addresses, m_array_of_datatypes);
}

int  MPI_Type_get_name (MPI_Datatype  type, char * type_name, int * resultlen) {
	(void) (type);
	(void) (type_name);
	(void) (resultlen);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_get_name_ (MPI_Fint * type, char * type_name, MPI_Fint * resultlen, MPI_Fint * ierr, int  name_len) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	int* m_resultlen = (int*) resultlen;
	(void) (ierr);
	(void) (name_len);

	MPI_Type_get_name(m_type, type_name, m_resultlen);
}

int  MPI_Type_extent (MPI_Datatype  type, MPI_Aint * extent) {
	(void) (type);
	(void) (extent);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_extent_ (MPI_Fint * type, MPI_Fint * extent, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	MPI_Aint* m_extent = (MPI_Aint*) extent;
	(void) (ierr);

	(*ierr) = MPI_Type_extent(m_type, m_extent);
}

int  MPI_Lookup_name (char * service_name, MPI_Info  info, char * port_name) {
	(void) (service_name);
	(void) (info);
	(void) (port_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_lookup_name_ (char * service_name, MPI_Fint * info, char * port_name, MPI_Fint * ierr, int  service_name_len, int  port_name_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (service_name_len);
	(void) (port_name_len);

	MPI_Lookup_name(service_name, m_info, port_name);
}

int  MPI_Win_set_attr (MPI_Win  win, int  win_keyval, void * attribute_val) {
	(void) (win);
	(void) (win_keyval);
	(void) (attribute_val);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_set_attr_ (MPI_Fint * win, MPI_Fint * win_keyval, MPI_Aint * attribute_val, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	int* m_win_keyval = (int*) win_keyval;
	void* m_attribute_val = (void*) attribute_val;
	(void) (ierr);

	(*ierr) = MPI_Win_set_attr(m_win, *m_win_keyval, m_attribute_val);
}

int  MPI_Type_vector (int  count, int  blocklength, int  stride, MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (blocklength);
	(void) (stride);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_vector_ (MPI_Fint * count, MPI_Fint * blocklength, MPI_Fint * stride, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_blocklength = (int*) blocklength;
	int* m_stride = (int*) stride;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_vector(*m_count, *m_blocklength, *m_stride, m_oldtype, m_newtype);
}

int  MPI_File_write_at_all_begin (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_at_all_begin_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_File_write_at_all_begin(m_fh, *m_offset, m_buf, *m_count, m_datatype);
}

int  MPI_Recv (void * buf, int  count, MPI_Datatype  datatype, int  source, int  tag, MPI_Comm  comm, MPI_Status * status) {
   MPI_Group cgroup, ugroup;
   int cres, source_urank;
   LocalNode *ln = LocalNode::get_LN ();
   if (!in_loop) {
      return PMPI_Recv (buf, count, datatype, source, tag, comm, status);
   }
   //DLOG (INFO) << "MPI_Recv call";

   if (source == MPI_ANY_SOURCE) {
      fprintf (stderr, "Recv from any source cannot be supported\n");
      exit (EXIT_FAILURE);
   }

   if (ln->step_wants_comm_info()) {
      PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
      PMPI_Comm_group(comm, &cgroup);
      PMPI_Group_translate_ranks(cgroup, 1, &source, ugroup, &source_urank);
      
      CommInfo commInfo;
      commInfo.insert_src (source_urank);

      ln->start_slack ();
      cres = PMPI_Recv (buf, count, datatype, source, tag, comm, status);

      ln->start_comm (CommInfo::MPI_RECV, commInfo.get_src (), commInfo.get_dst ());
   } else {
      cres = PMPI_Recv (buf, count, datatype, source, tag, comm, status);
   }

   ln->start_comp ();

   return cres;
}

void mpi_recv_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * source, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * status, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_source = (int*) source;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_Recv(m_buf, *m_count, m_datatype, *m_source, *m_tag, m_comm, m_status);
}

int  MPI_Unpack_external (char * datarep, void * inbuf, MPI_Aint  insize, MPI_Aint * position, void * outbuf, int  outcount, MPI_Datatype  datatype) {
	(void) (datarep);
	(void) (inbuf);
	(void) (insize);
	(void) (position);
	(void) (outbuf);
	(void) (outcount);
	(void) (datatype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_unpack_external_ (char * datarep, char * inbuf, MPI_Aint * insize, MPI_Aint * position, char * outbuf, MPI_Fint * outcount, MPI_Fint * datatype, MPI_Fint * ierr) {
	void* m_inbuf = (void*) inbuf;
	MPI_Aint* m_insize = (MPI_Aint*) insize;
	void* m_outbuf = (void*) outbuf;
	int* m_outcount = (int*) outcount;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_Unpack_external(datarep, m_inbuf, *m_insize, position, m_outbuf, *m_outcount, m_datatype);
}

int  MPI_File_read_at_all_begin (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_at_all_begin_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_File_read_at_all_begin(m_fh, *m_offset, m_buf, *m_count, m_datatype);
}

int  MPI_Graph_create (MPI_Comm  comm_old, int  nnodes, int * index, int * edges, int  reorder, MPI_Comm * comm_graph) {
	(void) (comm_old);
	(void) (nnodes);
	(void) (index);
	(void) (edges);
	(void) (reorder);
	(void) (comm_graph);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_graph_create_ (MPI_Fint * comm_old, MPI_Fint * nnodes, MPI_Fint * index, MPI_Fint * edges, ompi_fortran_logical_t * reorder, MPI_Fint * comm_graph, MPI_Fint * ierr) {
	MPI_Comm m_comm_old = MPI_Comm_f2c (*comm_old);
	int* m_nnodes = (int*) nnodes;
	int* m_index = (int*) index;
	int* m_edges = (int*) edges;
	int* m_reorder = (int*) reorder;
	MPI_Comm m_comm_graph = MPI_Comm_f2c (*comm_graph);
	(void) (ierr);

	(*ierr) = MPI_Graph_create(m_comm_old, *m_nnodes, m_index, m_edges, *m_reorder, &m_comm_graph);
	(*comm_graph) = MPI_Comm_c2f (m_comm_graph);
}

int  MPI_Type_create_hvector (int  count, int  blocklength, MPI_Aint  stride, MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (blocklength);
	(void) (stride);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_hvector_ (MPI_Fint * count, MPI_Fint * blocklength, MPI_Aint * stride, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_blocklength = (int*) blocklength;
	MPI_Aint* m_stride = (MPI_Aint*) stride;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_hvector(*m_count, *m_blocklength, *m_stride, m_oldtype, m_newtype);
}

int  MPI_Win_create (void * base, MPI_Aint  size, int  disp_unit, MPI_Info  info, MPI_Comm  comm, MPI_Win * win) {
	(void) (base);
	(void) (size);
	(void) (disp_unit);
	(void) (info);
	(void) (comm);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_create_ (char * base, MPI_Aint * size, MPI_Fint * disp_unit, MPI_Fint * info, MPI_Fint * comm, MPI_Fint * win, MPI_Fint * ierr) {
	void* m_base = (void*) base;
	MPI_Aint* m_size = (MPI_Aint*) size;
	int* m_disp_unit = (int*) disp_unit;
	MPI_Info m_info = MPI_Info_f2c (*info);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Win* m_win = (MPI_Win*) win;
	(void) (ierr);

	(*ierr) = MPI_Win_create(m_base, *m_size, *m_disp_unit, m_info, m_comm, m_win);
}

int  MPI_Type_dup (MPI_Datatype  type, MPI_Datatype * newtype) {
	(void) (type);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_dup_ (MPI_Fint * type, MPI_Fint * newtype, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_dup(m_type, m_newtype);
}

int  MPI_Test (MPI_Request * request, int * flag, MPI_Status * status) {
   return MPI_Test (request, flag, status);
}

void mpi_test_ (MPI_Fint * request, ompi_fortran_logical_t * flag, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_Request m_request = MPI_Request_f2c (*request);
	int* m_flag = (int*) flag;
	MPI_Status m_status;
   MPI_Status_f2c (status, &m_status);
	(void) (ierr);

	(*ierr) = MPI_Test(&m_request, m_flag, &m_status);

   *request = MPI_Request_c2f (m_request);
   MPI_Status_c2f (&m_status, status);
}

int  MPI_Type_commit (MPI_Datatype * type) {
	(void) (type);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_commit_ (MPI_Fint * type, MPI_Fint * ierr) {
	MPI_Datatype* m_type = (MPI_Datatype*) type;
	(void) (ierr);

	(*ierr) = MPI_Type_commit(m_type);
}

int  MPI_Comm_group (MPI_Comm  comm, MPI_Group * group) {

   return PMPI_Comm_group (comm, group);
}

void mpi_comm_group_ (MPI_Fint * comm, MPI_Fint * group, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Group m_group = MPI_Group_f2c (*group);
	(void) (ierr);

	(*ierr) = MPI_Comm_group(m_comm, &m_group);
   *group = MPI_Group_c2f (m_group);
}

int  MPI_Type_get_attr (MPI_Datatype  type, int  type_keyval, void * attribute_val, int * flag) {
	(void) (type);
	(void) (type_keyval);
	(void) (attribute_val);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_get_attr_ (MPI_Fint * type, MPI_Fint * type_keyval, MPI_Aint * attribute_val, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	int* m_type_keyval = (int*) type_keyval;
	void* m_attribute_val = (void*) attribute_val;
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Type_get_attr(m_type, *m_type_keyval, m_attribute_val, m_flag);
}

int  MPI_Type_free (MPI_Datatype * type) {
	(void) (type);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_free_ (MPI_Fint * type, MPI_Fint * ierr) {
	MPI_Datatype* m_type = (MPI_Datatype*) type;
	(void) (ierr);

	(*ierr) = MPI_Type_free(m_type);
}

double  MPI_Wtick (void ) {


	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

double mpi_wtick_ () {

	return MPI_Wtick();
}

int  MPI_Is_thread_main (int * flag) {
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_is_thread_main_ (ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Is_thread_main(m_flag);
}

int  MPI_Irecv (void * buf, int  count, MPI_Datatype  datatype, int  source, int  tag, MPI_Comm  comm, MPI_Request * request) {
   MPI_Group cgroup, ugroup;
   int  source_urank;
   
   if (!in_loop) {
      return PMPI_Irecv(buf, count, datatype, source, tag, comm, request);
   }
 
   if (source == MPI_ANY_SOURCE) {
      fprintf(stderr, "Irecv from any source cannot be supported\n");
      exit(EXIT_FAILURE);
   }

   PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
   PMPI_Comm_group(comm, &cgroup);
   PMPI_Group_translate_ranks(cgroup, 1, &source, ugroup, &source_urank);

   int cres = PMPI_Irecv(buf, count, datatype, source, tag, comm, request);
   //DLOG(INFO) << "Mpi_Irecv in loop: " << RequestChecksum (request);

   req2comms [*request] = CommInfo ();
   req2comms [*request].insert_src (source_urank);
   req2comms [*request].set_comm_name (CommInfo::MPI_IRECV);
   //DLOG(INFO) << "#" << getpid () << "adding a request Irecv: req2comms size = " << req2comms.size () << std::endl;

   return cres;
}

void mpi_irecv_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * source, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_source = (int*) source;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request m_request = MPI_Request_f2c(*request);
	(void) (ierr);

	(*ierr) = MPI_Irecv(m_buf, *m_count, m_datatype, *m_source, *m_tag, m_comm, &m_request);

   *request = MPI_Request_c2f(m_request);
   //DLOG (ERROR) << "Fmpi_irecv_ " << RequestChecksumi (request);
}

int  MPI_Info_set (MPI_Info  info, char * key, char * value) {
	(void) (info);
	(void) (key);
	(void) (value);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_set_ (MPI_Fint * info, char * key, char * value, MPI_Fint * ierr, int  key_len, int  value_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (key_len);
	(void) (value_len);

	MPI_Info_set(m_info, key, value);
}

int  MPI_Type_create_darray (int  size, int  rank, int  ndims, int  gsize_array[], int  distrib_array[], int  darg_array[], int  psize_array[], int  order, MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (size);
	(void) (rank);
	(void) (ndims);
	(void) (gsize_array);
	(void) (distrib_array);
	(void) (darg_array);
	(void) (psize_array);
	(void) (order);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_darray_ (MPI_Fint * size, MPI_Fint * rank, MPI_Fint * ndims, MPI_Fint * gsize_array, MPI_Fint * distrib_array, MPI_Fint * darg_array, MPI_Fint * psize_array, MPI_Fint * order, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_size = (int*) size;
	int* m_rank = (int*) rank;
	int* m_ndims = (int*) ndims;
	int* m_gsize_array = (int*) gsize_array;
	int* m_distrib_array = (int*) distrib_array;
	int* m_darg_array = (int*) darg_array;
	int* m_psize_array = (int*) psize_array;
	int* m_order = (int*) order;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_darray(*m_size, *m_rank, *m_ndims, m_gsize_array, m_distrib_array, m_darg_array, m_psize_array, *m_order, m_oldtype, m_newtype);
}

int  MPI_Initialized (int * flag) {
	(void) (flag);

	return PMPI_Initialized (flag);
}

void mpi_initialized_ (ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Initialized(m_flag);
}

int  MPI_File_open (MPI_Comm  comm, char * filename, int  amode, MPI_Info  info, MPI_File * fh) {
	(void) (comm);
	(void) (filename);
	(void) (amode);
	(void) (info);
	(void) (fh);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_open_ (MPI_Fint * comm, char * filename, MPI_Fint * amode, MPI_Fint * info, MPI_Fint * fh, MPI_Fint * ierr, int  name_len) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_amode = (int*) amode;
	MPI_Info m_info = MPI_Info_f2c (*info);
	MPI_File* m_fh = (MPI_File*) fh;
	(void) (ierr);
	(void) (name_len);

	MPI_File_open(m_comm, filename, *m_amode, m_info, m_fh);
}

int  MPI_Comm_remote_group (MPI_Comm  comm, MPI_Group * group) {
	(void) (comm);
	(void) (group);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_remote_group_ (MPI_Fint * comm, MPI_Fint * group, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Group* m_group = (MPI_Group*) group;
	(void) (ierr);

	(*ierr) = MPI_Comm_remote_group(m_comm, m_group);
}

int  MPI_Bsend (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_bsend_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Bsend(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm);
}

int  MPI_Type_lb (MPI_Datatype  type, MPI_Aint * lb) {
	(void) (type);
	(void) (lb);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_lb_ (MPI_Fint * type, MPI_Fint * lb, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	MPI_Aint* m_lb = (MPI_Aint*) lb;
	(void) (ierr);

	(*ierr) = MPI_Type_lb(m_type, m_lb);
}

int  MPI_Get_count (MPI_Status * status, MPI_Datatype  datatype, int * count) {
	(void) (status);
	(void) (datatype);
	(void) (count);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_get_count_ (MPI_Fint * status, MPI_Fint * datatype, MPI_Fint * count, MPI_Fint * ierr) {
	MPI_Status* m_status = (MPI_Status*) status;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_count = (int*) count;
	(void) (ierr);

	(*ierr) = MPI_Get_count(m_status, m_datatype, m_count);
}

int  MPI_Error_string (int  errorcode, char * string, int * resultlen) {
	(void) (errorcode);
	(void) (string);
	(void) (resultlen);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_error_string_ (MPI_Fint * errorcode, char * string, MPI_Fint * resultlen, MPI_Fint * ierr, int  string_len) {
	int* m_errorcode = (int*) errorcode;
	int* m_resultlen = (int*) resultlen;
	(void) (ierr);
	(void) (string_len);

	MPI_Error_string(*m_errorcode, string, m_resultlen);
}

int  MPI_Cart_sub (MPI_Comm  comm, int * remain_dims, MPI_Comm * new_comm) {
	(void) (comm);
	(void) (remain_dims);
	(void) (new_comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cart_sub_ (MPI_Fint * comm, ompi_fortran_logical_t * remain_dims, MPI_Fint * new_comm, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_remain_dims = (int*) remain_dims;
	MPI_Comm m_new_comm = MPI_Comm_f2c (*new_comm);
	(void) (ierr);

	(*ierr) = MPI_Cart_sub(m_comm, m_remain_dims, &m_new_comm);
	(*new_comm) = MPI_Comm_c2f (m_new_comm);
}

int  MPI_Cart_coords (MPI_Comm  comm, int  rank, int  maxdims, int * coords) {
	(void) (comm);
	(void) (rank);
	(void) (maxdims);
	(void) (coords);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cart_coords_ (MPI_Fint * comm, MPI_Fint * rank, MPI_Fint * maxdims, MPI_Fint * coords, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_rank = (int*) rank;
	int* m_maxdims = (int*) maxdims;
	int* m_coords = (int*) coords;
	(void) (ierr);

	(*ierr) = MPI_Cart_coords(m_comm, *m_rank, *m_maxdims, m_coords);
}

int  MPI_Init_thread (int * argc, char *** argv, int  required, int * provided) {
	(void) (argc);
	(void) (argv);
	(void) (required);
	(void) (provided);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_init_thread_ (void *required, void *provided, void *ierr) {
	(void) (required);
	(void) (provided);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Type_ub (MPI_Datatype  mtype, MPI_Aint * ub) {
	(void) (mtype);
	(void) (ub);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_ub_ (MPI_Fint * mtype, MPI_Fint * ub, MPI_Fint * ierr) {
	MPI_Datatype m_mtype = MPI_Type_f2c (*mtype);
	MPI_Aint* m_ub = (MPI_Aint*) ub;
	(void) (ierr);

	(*ierr) = MPI_Type_ub(m_mtype, m_ub);
}

int  MPI_File_get_byte_offset (MPI_File  fh, MPI_Offset  offset, MPI_Offset * disp) {
	(void) (fh);
	(void) (offset);
	(void) (disp);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_byte_offset_ (MPI_Fint * fh, MPI_Offset * offset, MPI_Offset * disp, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	(void) (ierr);

	(*ierr) = MPI_File_get_byte_offset(m_fh, *m_offset, disp);
}

int  MPI_Win_delete_attr (MPI_Win  win, int  win_keyval) {
	(void) (win);
	(void) (win_keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_delete_attr_ (MPI_Fint * win, MPI_Fint * win_keyval, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	int* m_win_keyval = (int*) win_keyval;
	(void) (ierr);

	(*ierr) = MPI_Win_delete_attr(m_win, *m_win_keyval);
}

int  MPI_File_write_at_all_end (MPI_File  fh, void * buf, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_at_all_end_ (MPI_Fint * fh, char * buf, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_at_all_end(m_fh, m_buf, m_status);
}

int  MPI_File_set_atomicity (MPI_File  fh, int  flag) {
	(void) (fh);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_set_atomicity_ (MPI_Fint * fh, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_File_set_atomicity(m_fh, *m_flag);
}

int  MPI_File_get_amode (MPI_File  fh, int * amode) {
	(void) (fh);
	(void) (amode);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_amode_ (MPI_Fint * fh, MPI_Fint * amode, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	int* m_amode = (int*) amode;
	(void) (ierr);

	(*ierr) = MPI_File_get_amode(m_fh, m_amode);
}

int  MPI_Group_range_incl (MPI_Group  group, int  n, int  ranges[][3], MPI_Group * newgroup) {
	(void) (group);
	(void) (n);
	(void) (ranges[3]);
	(void) (newgroup);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_range_incl_ (void *group, void *n, void *ranges, void *newgroup, void *ierr) {
	(void) (group);
	(void) (n);
	(void) (ranges);
	(void) (newgroup);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Get (void * origin_addr, int  origin_count, MPI_Datatype  origin_datatype, int  target_rank, MPI_Aint  target_disp, int  target_count, MPI_Datatype  target_datatype, MPI_Win  win) {
	(void) (origin_addr);
	(void) (origin_count);
	(void) (origin_datatype);
	(void) (target_rank);
	(void) (target_disp);
	(void) (target_count);
	(void) (target_datatype);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_get_ (char * origin_addr, MPI_Fint * origin_count, MPI_Fint * origin_datatype, MPI_Fint * target_rank, MPI_Aint * target_disp, MPI_Fint * target_count, MPI_Fint * target_datatype, MPI_Fint * win, MPI_Fint * ierr) {
	void* m_origin_addr = (void*) origin_addr;
	int* m_origin_count = (int*) origin_count;
	MPI_Datatype m_origin_datatype = MPI_Type_f2c (*origin_datatype);
	int* m_target_rank = (int*) target_rank;
	MPI_Aint* m_target_disp = (MPI_Aint*) target_disp;
	int* m_target_count = (int*) target_count;
	MPI_Datatype m_target_datatype = MPI_Type_f2c (*target_datatype);
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Get(m_origin_addr, *m_origin_count, m_origin_datatype, *m_target_rank, *m_target_disp, *m_target_count, m_target_datatype, m_win);
}

int  MPI_Group_excl (MPI_Group  group, int  n, int * ranks, MPI_Group * newgroup) {
	(void) (group);
	(void) (n);
	(void) (ranks);
	(void) (newgroup);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_excl_ (MPI_Fint * group, MPI_Fint * n, MPI_Fint * ranks, MPI_Fint * newgroup, MPI_Fint * ierr) {
	MPI_Group m_group = MPI_Group_f2c (*group);
	int* m_n = (int*) n;
	int* m_ranks = (int*) ranks;
	MPI_Group* m_newgroup = (MPI_Group*) newgroup;
	(void) (ierr);

	(*ierr) = MPI_Group_excl(m_group, *m_n, m_ranks, m_newgroup);
}

int  MPI_Graph_get (MPI_Comm  comm, int  maxindex, int  maxedges, int * index, int * edges) {
	(void) (comm);
	(void) (maxindex);
	(void) (maxedges);
	(void) (index);
	(void) (edges);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_graph_get_ (MPI_Fint * comm, MPI_Fint * maxindex, MPI_Fint * maxedges, MPI_Fint * index, MPI_Fint * edges, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_maxindex = (int*) maxindex;
	int* m_maxedges = (int*) maxedges;
	int* m_index = (int*) index;
	int* m_edges = (int*) edges;
	(void) (ierr);

	(*ierr) = MPI_Graph_get(m_comm, *m_maxindex, *m_maxedges, m_index, m_edges);
}

int  MPI_Ssend (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_ssend_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Ssend(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm);
}

int  MPI_Win_test (MPI_Win  win, int * flag) {
	(void) (win);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_test_ (MPI_Fint * win, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Win_test(m_win, m_flag);
}

int  MPI_Cart_rank (MPI_Comm  comm, int * coords, int * rank) {
	(void) (comm);
	(void) (coords);
	(void) (rank);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cart_rank_ (MPI_Fint * comm, MPI_Fint * coords, MPI_Fint * rank, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_coords = (int*) coords;
	int* m_rank = (int*) rank;
	(void) (ierr);

	(*ierr) = MPI_Cart_rank(m_comm, m_coords, m_rank);
}

int  MPI_Finalize (void ) {
   //DLOG (ERROR) << "Entering Finalize";
   // free some memory
   for (unsigned int i = 0; i < g2rCache.size; i++) {
      //DLOG (ERROR) << "Deleting cache [" << i << "].second";
      DCHECK (g2rCache.cache [i].second != NULL) << "Error: Trying to free a cache already freed before";
      delete g2rCache.cache[i].second;
   }

   //DLOG (ERROR) << "Error: Destroying LN";
   LocalNode *ln = LocalNode::get_LN ();
   ln->destroy ();

   //DLOG (ERROR) << "Calling PMPI_Finalize";

   return PMPI_Finalize();
}

void mpi_finalize_ (MPI_Fint * ierr) {
	(*ierr) = MPI_Finalize ();
}
int  MPI_Type_get_extent (MPI_Datatype  type, MPI_Aint * lb, MPI_Aint * extent) {
	(void) (type);
	(void) (lb);
	(void) (extent);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_get_extent_ (MPI_Fint * type, MPI_Aint * lb, MPI_Aint * extent, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	(void) (ierr);

	(*ierr) = MPI_Type_get_extent(m_type, lb, extent);
}

int  MPI_Comm_create (MPI_Comm  comm, MPI_Group  group, MPI_Comm * newcomm) {
   return PMPI_Comm_create (comm, group, newcomm);
}

void mpi_comm_create_ (MPI_Fint * comm, MPI_Fint * group, MPI_Fint * newcomm, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Group m_group = MPI_Group_f2c (*group);
	MPI_Comm m_newcomm = MPI_Comm_f2c (*newcomm);
	(void) (ierr);

	(*ierr) = MPI_Comm_create(m_comm, m_group, &m_newcomm);
	(*newcomm) = MPI_Comm_c2f (m_newcomm);
}

int  MPI_Comm_join (int  fd, MPI_Comm * intercomm) {
	(void) (fd);
	(void) (intercomm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_join_ (MPI_Fint * fd, MPI_Fint * intercomm, MPI_Fint * ierr) {
	int* m_fd = (int*) fd;
	MPI_Comm m_intercomm = MPI_Comm_f2c (*intercomm);
	(void) (ierr);

	(*ierr) = MPI_Comm_join(*m_fd, &m_intercomm);
	(*intercomm) = MPI_Comm_c2f (m_intercomm);
}

int  MPI_File_read_at (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_at_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_at(m_fh, *m_offset, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_Comm_accept (char * port_name, MPI_Info  info, int  root, MPI_Comm  comm, MPI_Comm * newcomm) {
	(void) (port_name);
	(void) (info);
	(void) (root);
	(void) (comm);
	(void) (newcomm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_accept_ (char * port_name, MPI_Fint * info, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * newcomm, MPI_Fint * ierr, int  port_name_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Comm m_newcomm = MPI_Comm_f2c (*newcomm);
	(void) (ierr);
	(void) (port_name_len);

	MPI_Comm_accept(port_name, m_info, *m_root, m_comm, &m_newcomm);
	(*newcomm) = MPI_Comm_c2f (m_newcomm);
}

int  MPI_Cart_shift (MPI_Comm  comm, int  direction, int  disp, int * rank_source, int * rank_dest) {
	(void) (comm);
	(void) (direction);
	(void) (disp);
	(void) (rank_source);
	(void) (rank_dest);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cart_shift_ (MPI_Fint * comm, MPI_Fint * direction, MPI_Fint * disp, MPI_Fint * rank_source, MPI_Fint * rank_dest, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_direction = (int*) direction;
	int* m_disp = (int*) disp;
	int* m_rank_source = (int*) rank_source;
	int* m_rank_dest = (int*) rank_dest;
	(void) (ierr);

	(*ierr) = MPI_Cart_shift(m_comm, *m_direction, *m_disp, m_rank_source, m_rank_dest);
}

int  MPI_Comm_size (MPI_Comm  comm, int * size) {
	(void) (comm);
	(void) (size);

	return PMPI_Comm_size (comm, size);
}

void mpi_comm_size_ (MPI_Fint * comm, MPI_Fint * size, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_size = (int*) size;
	(void) (ierr);

	(*ierr) = MPI_Comm_size(m_comm, m_size);
}

int  MPI_File_iread_shared (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Request * request) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_iread_shared_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * request, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_File_iread_shared(m_fh, m_buf, *m_count, m_datatype, m_request);
}

int  MPI_File_get_atomicity (MPI_File  fh, int * flag) {
	(void) (fh);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_atomicity_ (MPI_Fint * fh, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_File_get_atomicity(m_fh, m_flag);
}

int  MPI_File_write_ordered_end (MPI_File  fh, void * buf, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_ordered_end_ (MPI_Fint * fh, char * buf, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_ordered_end(m_fh, m_buf, m_status);
}

int  MPI_Type_create_hindexed (int  count, int  array_of_blocklengths[], MPI_Aint  array_of_displacements[], MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (array_of_blocklengths);
	(void) (array_of_displacements);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_hindexed_ (MPI_Fint * count, MPI_Fint * array_of_blocklengths, MPI_Aint * array_of_displacements, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_array_of_blocklengths = (int*) array_of_blocklengths;
	MPI_Aint* m_array_of_displacements = (MPI_Aint*) array_of_displacements;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_hindexed(*m_count, m_array_of_blocklengths, m_array_of_displacements, m_oldtype, m_newtype);
}

int  MPI_File_read_ordered (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_ordered_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_ordered(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_Testall (int  count, MPI_Request  array_of_requests[], int * flag, MPI_Status  array_of_statuses[]) {
	(void) (count);
	(void) (array_of_requests);
	(void) (flag);
	(void) (array_of_statuses);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_testall_ (MPI_Fint * count, MPI_Fint * array_of_requests, ompi_fortran_logical_t * flag, MPI_Fint * array_of_statuses, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	MPI_Request* m_array_of_requests = (MPI_Request*) array_of_requests;
	int* m_flag = (int*) flag;
	MPI_Status* m_array_of_statuses = (MPI_Status*) array_of_statuses;
	(void) (ierr);

	(*ierr) = MPI_Testall(*m_count, m_array_of_requests, m_flag, m_array_of_statuses);
}

int  MPI_Init (int * argc, char *** argv) {
	int cres, rank;

   if (fortran) {
      char program_name [] = "benchmark";
      google::InitGoogleLogging(program_name);
   } else { 
      google::InitGoogleLogging(*argv[0]);
   }
   google::LogToStderr();

   cres = PMPI_Init(argc, argv);

   // Allocate the node data with the corresponding rank
   LocalNode *ln = LocalNode::instantiate ();
   DCHECK (ln != NULL);
   (void) ln;

   // Set debug level for rank 0
   PMPI_Comm_rank(MPI_COMM_WORLD, &rank);
   if (rank > 0) {
      fLI::FLAGS_minloglevel = 2;   // only log ERROR or above for non master
   }

#ifdef NDEBUG
   // Handle exit signal to exit properly
   struct sigaction sig;
   memset (&sig, 0, sizeof (sig));
   sig.sa_handler = sigHandler;

   sigaction (SIGINT, &sig, NULL);
   sigaction (SIGTERM, &sig, NULL);
#endif

   return cres;
}

void mpi_init_ (MPI_Fint * ierr) {
   fortran = true;

	/*dlerror ();
	MPI_STATUS_IGNORE_F = (int *) dlsym(RTLD_NEXT, "mpi_status_ignore_");
	assert (dlerror () == NULL);
	MPI_STATUSES_IGNORE_F = (int *) dlsym(RTLD_NEXT, "mpi_statuses_ignore_");
	assert (dlerror () == NULL);
	MPI_STATUS_SIZE_F = *((int *) dlsym(RTLD_NEXT, "mpi_status_size_"));
	assert (dlerror () == NULL);*/
   MPI_STATUS_IGNORE_F = NULL;
   MPI_STATUSES_IGNORE_F = NULL;
   MPI_STATUS_SIZE_F = 6;
	(*ierr) = MPI_Init (0, NULL);
}

int  MPI_Type_set_name (MPI_Datatype  type, char * type_name) {
	(void) (type);
	(void) (type_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_set_name_ (MPI_Fint * type, char * type_name, MPI_Fint * ierr, int  name_len) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	(void) (ierr);
	(void) (name_len);

	MPI_Type_set_name(m_type, type_name);
}

int  MPI_Win_fence (int  assert, MPI_Win  win) {
	(void) (assert);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_fence_ (MPI_Fint * assert, MPI_Fint * win, MPI_Fint * ierr) {
	int* m_assert = (int*) assert;
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Win_fence(*m_assert, m_win);
}

int  MPI_Group_difference (MPI_Group  group1, MPI_Group  group2, MPI_Group * newgroup) {
	(void) (group1);
	(void) (group2);
	(void) (newgroup);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_difference_ (MPI_Fint * group1, MPI_Fint * group2, MPI_Fint * newgroup, MPI_Fint * ierr) {
	MPI_Group m_group1 = MPI_Group_f2c (*group1);
	MPI_Group m_group2 = MPI_Group_f2c (*group2);
	MPI_Group* m_newgroup = (MPI_Group*) newgroup;
	(void) (ierr);

	(*ierr) = MPI_Group_difference(m_group1, m_group2, m_newgroup);
}

int  MPI_Attr_get (MPI_Comm  comm, int  keyval, void * attribute_val, int * flag) {
	(void) (comm);
	(void) (keyval);
	(void) (attribute_val);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_attr_get_ (MPI_Fint * comm, MPI_Fint * keyval, MPI_Fint * attribute_val, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_keyval = (int*) keyval;
	void* m_attribute_val = (void*) attribute_val;
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Attr_get(m_comm, *m_keyval, m_attribute_val, m_flag);
}

int  MPI_Reduce_scatter (void * sendbuf, void * recvbuf, int * recvcounts, MPI_Datatype  datatype, MPI_Op  op, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (recvbuf);
	(void) (recvcounts);
	(void) (datatype);
	(void) (op);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_reduce_scatter_ (char * sendbuf, char * recvbuf, MPI_Fint * recvcounts, MPI_Fint * datatype, MPI_Fint * op, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcounts = (int*) recvcounts;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Op m_op = MPI_Op_f2c (*op);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Reduce_scatter(m_sendbuf, m_recvbuf, m_recvcounts, m_datatype, m_op, m_comm);
}

int  MPI_Type_hindexed (int  count, int  array_of_blocklengths[], MPI_Aint  array_of_displacements[], MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (array_of_blocklengths);
	(void) (array_of_displacements);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_hindexed_ (MPI_Fint * count, MPI_Fint * array_of_blocklengths, MPI_Fint * array_of_displacements, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_array_of_blocklengths = (int*) array_of_blocklengths;
	MPI_Aint* m_array_of_displacements = (MPI_Aint*) array_of_displacements;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_hindexed(*m_count, m_array_of_blocklengths, m_array_of_displacements, m_oldtype, m_newtype);
}

int  MPI_File_close (MPI_File * fh) {
	(void) (fh);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_close_ (MPI_Fint * fh, MPI_Fint * ierr) {
	MPI_File* m_fh = (MPI_File*) fh;
	(void) (ierr);

	(*ierr) = MPI_File_close(m_fh);
}

int  MPI_Comm_dup (MPI_Comm  comm, MPI_Comm * newcomm) {
   return PMPI_Comm_dup(comm, newcomm);
}

void mpi_comm_dup_ (MPI_Fint * comm, MPI_Fint * newcomm, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Comm m_newcomm = MPI_Comm_f2c (*newcomm);
	(void) (ierr);

	(*ierr) = MPI_Comm_dup(m_comm, &m_newcomm);
	(*newcomm) = MPI_Comm_c2f (m_newcomm);
}

int  MPI_Comm_delete_attr (MPI_Comm  comm, int  comm_keyval) {
	(void) (comm);
	(void) (comm_keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_delete_attr_ (MPI_Fint * comm, MPI_Fint * comm_keyval, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_comm_keyval = (int*) comm_keyval;
	(void) (ierr);

	(*ierr) = MPI_Comm_delete_attr(m_comm, *m_comm_keyval);
}

int  MPI_File_call_errhandler (MPI_File  fh, int  errorcode) {
	(void) (fh);
	(void) (errorcode);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_call_errhandler_ (MPI_Fint * fh, MPI_Fint * errorcode, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	int* m_errorcode = (int*) errorcode;
	(void) (ierr);

	(*ierr) = MPI_File_call_errhandler(m_fh, *m_errorcode);
}

int  MPI_Win_unlock (int  rank, MPI_Win  win) {
	(void) (rank);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_unlock_ (MPI_Fint * rank, MPI_Fint * win, MPI_Fint * ierr) {
	int* m_rank = (int*) rank;
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Win_unlock(*m_rank, m_win);
}

int  MPI_Group_compare (MPI_Group  group1, MPI_Group  group2, int * result) {
	(void) (group1);
	(void) (group2);
	(void) (result);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_compare_ (MPI_Fint * group1, MPI_Fint * group2, MPI_Fint * result, MPI_Fint * ierr) {
	MPI_Group m_group1 = MPI_Group_f2c (*group1);
	MPI_Group m_group2 = MPI_Group_f2c (*group2);
	int* m_result = (int*) result;
	(void) (ierr);

	(*ierr) = MPI_Group_compare(m_group1, m_group2, m_result);
}

int  MPI_Barrier (MPI_Comm  comm) {
   MPI_Group cgroup, dist_group;
   int is_inter = 0;
   LocalNode *ln = LocalNode::get_LN ();

   // TODO handle inter-communicators ?

   if (!in_loop) {
      return PMPI_Barrier (comm);
   }

   //DLOG(INFO) << "MPI_Barrier in loop" << std::endl;

   int ret;
   if (ln->step_wants_comm_info()) {
      // First barrier call for slack
      ln->start_slack ();
      ret = PMPI_Barrier(comm);

      PMPI_Comm_test_inter (comm, &is_inter);

      std::set<int> nodes;
      PMPI_Comm_group (comm, &cgroup);
      getGroupRanks (cgroup, nodes);

      // inter communicator impacts more nodes
      if (is_inter) {
         PMPI_Comm_remote_group (comm, &dist_group);
         getGroupRanks (dist_group, nodes);
      }

      ln->start_comm (CommInfo::MPI_BARRIER, nodes, nodes);
   } else {
      ret = PMPI_Barrier(comm);
   }

   // Start a new task
   ln->start_comp ();

	return ret;
}

void mpi_barrier_ (MPI_Fint * comm, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Barrier(m_comm);
}

int  MPI_Get_elements (MPI_Status * status, MPI_Datatype  datatype, int * count) {
	(void) (status);
	(void) (datatype);
	(void) (count);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_get_elements_ (MPI_Fint * status, MPI_Fint * datatype, MPI_Fint * count, MPI_Fint * ierr) {
	MPI_Status* m_status = (MPI_Status*) status;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_count = (int*) count;
	(void) (ierr);

	(*ierr) = MPI_Get_elements(m_status, m_datatype, m_count);
}

int  MPI_Win_set_name (MPI_Win  win, char * win_name) {
	(void) (win);
	(void) (win_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_set_name_ (MPI_Fint * win, char * win_name, MPI_Fint * ierr, int  name_len) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);
	(void) (name_len);

	MPI_Win_set_name(m_win, win_name);
}

int  MPI_Ssend_init (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_ssend_init_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_Ssend_init(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, m_request);
}

int  MPI_Add_error_string (int  errorcode, char * string) {
	(void) (errorcode);
	(void) (string);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_add_error_string_ (MPI_Fint * errorcode, char * string, MPI_Fint * ierr, int  l) {
	int* m_errorcode = (int*) errorcode;
	(void) (ierr);
	(void) (l);

	MPI_Add_error_string(*m_errorcode, string);
}

int  MPI_Type_create_struct (int  count, int  array_of_block_lengths[], MPI_Aint  array_of_displacements[], MPI_Datatype  array_of_types[], MPI_Datatype * newtype) {
	(void) (count);
	(void) (array_of_block_lengths);
	(void) (array_of_displacements);
	(void) (array_of_types);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_struct_ (MPI_Fint * count, MPI_Fint * array_of_block_lengths, MPI_Aint * array_of_displacements, MPI_Fint * array_of_types, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_array_of_block_lengths = (int*) array_of_block_lengths;
	MPI_Aint* m_array_of_displacements = (MPI_Aint*) array_of_displacements;
	MPI_Datatype* m_array_of_types = (MPI_Datatype*) array_of_types;
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_struct(*m_count, m_array_of_block_lengths, m_array_of_displacements, m_array_of_types, m_newtype);
}

int  MPI_Op_free (MPI_Op * op) {
	(void) (op);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_op_free_ (MPI_Fint * op, MPI_Fint * ierr) {
	MPI_Op* m_op = (MPI_Op*) op;
	(void) (ierr);

	(*ierr) = MPI_Op_free(m_op);
}

int  MPI_Win_post (MPI_Group  group, int  assert, MPI_Win  win) {
	(void) (group);
	(void) (assert);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_post_ (MPI_Fint * group, MPI_Fint * assert, MPI_Fint * win, MPI_Fint * ierr) {
	MPI_Group m_group = MPI_Group_f2c (*group);
	int* m_assert = (int*) assert;
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Win_post(m_group, *m_assert, m_win);
}

int  MPI_Accumulate (void * origin_addr, int  origin_count, MPI_Datatype  origin_datatype, int  target_rank, MPI_Aint  target_disp, int  target_count, MPI_Datatype  target_datatype, MPI_Op  op, MPI_Win  win) {
	(void) (origin_addr);
	(void) (origin_count);
	(void) (origin_datatype);
	(void) (target_rank);
	(void) (target_disp);
	(void) (target_count);
	(void) (target_datatype);
	(void) (op);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_accumulate_ (char * origin_addr, MPI_Fint * origin_count, MPI_Fint * origin_datatype, MPI_Fint * target_rank, MPI_Aint * target_disp, MPI_Fint * target_count, MPI_Fint * target_datatype, MPI_Fint * op, MPI_Fint * win, MPI_Fint * ierr) {
	void* m_origin_addr = (void*) origin_addr;
	int* m_origin_count = (int*) origin_count;
	MPI_Datatype m_origin_datatype = MPI_Type_f2c (*origin_datatype);
	int* m_target_rank = (int*) target_rank;
	MPI_Aint* m_target_disp = (MPI_Aint*) target_disp;
	int* m_target_count = (int*) target_count;
	MPI_Datatype m_target_datatype = MPI_Type_f2c (*target_datatype);
	MPI_Op m_op = MPI_Op_f2c (*op);
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Accumulate(m_origin_addr, *m_origin_count, m_origin_datatype, *m_target_rank, *m_target_disp, *m_target_count, m_target_datatype, m_op, m_win);
}

int  MPI_Comm_get_name (MPI_Comm  comm, char * comm_name, int * resultlen) {
	(void) (comm);
	(void) (comm_name);
	(void) (resultlen);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_get_name_ (MPI_Fint * comm, char * comm_name, MPI_Fint * resultlen, MPI_Fint * ierr, int  name_len) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_resultlen = (int*) resultlen;
	(void) (ierr);
	(void) (name_len);

	MPI_Comm_get_name(m_comm, comm_name, m_resultlen);
}

int  MPI_Cart_create (MPI_Comm  old_comm, int  ndims, int * dims, int * periods, int  reorder, MPI_Comm * comm_cart) {
	(void) (old_comm);
	(void) (ndims);
	(void) (dims);
	(void) (periods);
	(void) (reorder);
	(void) (comm_cart);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cart_create_ (MPI_Fint * old_comm, MPI_Fint * ndims, MPI_Fint * dims, ompi_fortran_logical_t * periods, ompi_fortran_logical_t * reorder, MPI_Fint * comm_cart, MPI_Fint * ierr) {
	MPI_Comm m_old_comm = MPI_Comm_f2c (*old_comm);
	int* m_ndims = (int*) ndims;
	int* m_dims = (int*) dims;
	int* m_periods = (int*) periods;
	int* m_reorder = (int*) reorder;
	MPI_Comm m_comm_cart = MPI_Comm_f2c (*comm_cart);
	(void) (ierr);

	(*ierr) = MPI_Cart_create(m_old_comm, *m_ndims, m_dims, m_periods, *m_reorder, &m_comm_cart);
	(*comm_cart) = MPI_Comm_c2f (m_comm_cart);
}

int  MPI_Status_set_cancelled (MPI_Status * status, int  flag) {
	(void) (status);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_status_set_cancelled_ (MPI_Fint * status, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Status* m_status = (MPI_Status*) status;
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Status_set_cancelled(m_status, *m_flag);
}

int  MPI_Type_struct (int  count, int  array_of_blocklengths[], MPI_Aint  array_of_displacements[], MPI_Datatype  array_of_types[], MPI_Datatype * newtype) {
	(void) (count);
	(void) (array_of_blocklengths);
	(void) (array_of_displacements);
	(void) (array_of_types);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_struct_ (MPI_Fint * count, MPI_Fint * array_of_blocklengths, MPI_Fint * array_of_displacements, MPI_Fint * array_of_types, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_array_of_blocklengths = (int*) array_of_blocklengths;
	MPI_Aint* m_array_of_displacements = (MPI_Aint*) array_of_displacements;
	MPI_Datatype* m_array_of_types = (MPI_Datatype*) array_of_types;
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_struct(*m_count, m_array_of_blocklengths, m_array_of_displacements, m_array_of_types, m_newtype);
}

int  MPI_Graph_neighbors_count (MPI_Comm  comm, int  rank, int * nneighbors) {
	(void) (comm);
	(void) (rank);
	(void) (nneighbors);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_graph_neighbors_count_ (MPI_Fint * comm, MPI_Fint * rank, MPI_Fint * nneighbors, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_rank = (int*) rank;
	int* m_nneighbors = (int*) nneighbors;
	(void) (ierr);

	(*ierr) = MPI_Graph_neighbors_count(m_comm, *m_rank, m_nneighbors);
}

int  MPI_Comm_set_errhandler (MPI_Comm  comm, MPI_Errhandler  errhandler) {
	(void) (comm);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_set_errhandler_ (MPI_Fint * comm, MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_Comm_set_errhandler(m_comm, *m_errhandler);
}

int  MPI_Allgatherv (void * sendbuf, int  sendcount, MPI_Datatype  sendtype, void * recvbuf, int * recvcounts, int * displs, MPI_Datatype  recvtype, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (sendcount);
	(void) (sendtype);
	(void) (recvbuf);
	(void) (recvcounts);
	(void) (displs);
	(void) (recvtype);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_allgatherv_ (char * sendbuf, MPI_Fint * sendcount, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcounts, MPI_Fint * displs, MPI_Fint * recvtype, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcount = (int*) sendcount;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcounts = (int*) recvcounts;
	int* m_displs = (int*) displs;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Allgatherv(m_sendbuf, *m_sendcount, m_sendtype, m_recvbuf, m_recvcounts, m_displs, m_recvtype, m_comm);
}

int  MPI_File_read_ordered_begin (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_ordered_begin_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_File_read_ordered_begin(m_fh, m_buf, *m_count, m_datatype);
}

int  MPI_Iprobe (int  source, int  tag, MPI_Comm  comm, int * flag, MPI_Status * status) {
	(void) (source);
	(void) (tag);
	(void) (comm);
	(void) (flag);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_iprobe_ (MPI_Fint * source, MPI_Fint * tag, MPI_Fint * comm, ompi_fortran_logical_t * flag, MPI_Fint * status, MPI_Fint * ierr) {
	int* m_source = (int*) source;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_flag = (int*) flag;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_Iprobe(*m_source, *m_tag, m_comm, m_flag, m_status);
}

int  MPI_Type_set_attr (MPI_Datatype  type, int  type_keyval, void * attr_val) {
	(void) (type);
	(void) (type_keyval);
	(void) (attr_val);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_set_attr_ (MPI_Fint * type, MPI_Fint * type_keyval, MPI_Aint * attr_val, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	int* m_type_keyval = (int*) type_keyval;
	void* m_attr_val = (void*) attr_val;
	(void) (ierr);

	(*ierr) = MPI_Type_set_attr(m_type, *m_type_keyval, m_attr_val);
}

int  MPI_File_write_all_end (MPI_File  fh, void * buf, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_all_end_ (MPI_Fint * fh, char * buf, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_all_end(m_fh, m_buf, m_status);
}

int  MPI_Reduce_local (void * inbuf, void * inoutbuf, int  count, MPI_Datatype  datatype, MPI_Op  op) {
	(void) (inbuf);
	(void) (inoutbuf);
	(void) (count);
	(void) (datatype);
	(void) (op);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_reduce_local_ (char * inbuf, char * inoutbuf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * op, MPI_Fint * ierr) {
	void* m_inbuf = (void*) inbuf;
	void* m_inoutbuf = (void*) inoutbuf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Op m_op = MPI_Op_f2c (*op);
	(void) (ierr);

	(*ierr) = MPI_Reduce_local(m_inbuf, m_inoutbuf, *m_count, m_datatype, m_op);
}

int  MPI_Dims_create (int  nnodes, int  ndims, int * dims) {
	(void) (nnodes);
	(void) (ndims);
	(void) (dims);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_dims_create_ (MPI_Fint * nnodes, MPI_Fint * ndims, MPI_Fint * dims, MPI_Fint * ierr) {
	int* m_nnodes = (int*) nnodes;
	int* m_ndims = (int*) ndims;
	int* m_dims = (int*) dims;
	(void) (ierr);

	(*ierr) = MPI_Dims_create(*m_nnodes, *m_ndims, m_dims);
}

int  MPI_File_iread (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Request * request) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_iread_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * request, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_File_iread(m_fh, m_buf, *m_count, m_datatype, m_request);
}

int  MPI_Win_wait (MPI_Win  win) {
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_wait_ (MPI_Fint * win, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Win_wait(m_win);
}

int  MPI_Comm_set_attr (MPI_Comm  comm, int  comm_keyval, void * attribute_val) {
	(void) (comm);
	(void) (comm_keyval);
	(void) (attribute_val);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_set_attr_ (MPI_Fint * comm, MPI_Fint * comm_keyval, MPI_Aint * attribute_val, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_comm_keyval = (int*) comm_keyval;
	void* m_attribute_val = (void*) attribute_val;
	(void) (ierr);

	(*ierr) = MPI_Comm_set_attr(m_comm, *m_comm_keyval, m_attribute_val);
}

int  MPI_Comm_free_keyval (int * comm_keyval) {
	(void) (comm_keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_free_keyval_ (MPI_Fint * comm_keyval, MPI_Fint * ierr) {
	int* m_comm_keyval = (int*) comm_keyval;
	(void) (ierr);

	(*ierr) = MPI_Comm_free_keyval(m_comm_keyval);
}

int  MPI_Win_set_errhandler (MPI_Win  win, MPI_Errhandler  errhandler) {
	(void) (win);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_set_errhandler_ (MPI_Fint * win, MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_Win_set_errhandler(m_win, *m_errhandler);
}

int  MPI_Info_get_nkeys (MPI_Info  info, int * nkeys) {
	(void) (info);
	(void) (nkeys);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_get_nkeys_ (MPI_Fint * info, MPI_Fint * nkeys, MPI_Fint * ierr) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	int* m_nkeys = (int*) nkeys;
	(void) (ierr);

	(*ierr) = MPI_Info_get_nkeys(m_info, m_nkeys);
}

int  MPI_Type_match_size (int  typeclass, int  size, MPI_Datatype * type) {
	(void) (typeclass);
	(void) (size);
	(void) (type);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_match_size_ (MPI_Fint * typeclass, MPI_Fint * size, MPI_Fint * type, MPI_Fint * ierr) {
	int* m_typeclass = (int*) typeclass;
	int* m_size = (int*) size;
	MPI_Datatype* m_type = (MPI_Datatype*) type;
	(void) (ierr);

	(*ierr) = MPI_Type_match_size(*m_typeclass, *m_size, m_type);
}

int  MPI_File_seek (MPI_File  fh, MPI_Offset  offset, int  whence) {
	(void) (fh);
	(void) (offset);
	(void) (whence);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_seek_ (MPI_Fint * fh, MPI_Offset * offset, MPI_Fint * whence, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	int* m_whence = (int*) whence;
	(void) (ierr);

	(*ierr) = MPI_File_seek(m_fh, *m_offset, *m_whence);
}

int  MPI_File_write_all_begin (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_all_begin_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_File_write_all_begin(m_fh, m_buf, *m_count, m_datatype);
}

int  MPI_Rsend_init (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_rsend_init_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_Rsend_init(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, m_request);
}

int  MPI_Publish_name (char * service_name, MPI_Info  info, char * port_name) {
	(void) (service_name);
	(void) (info);
	(void) (port_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_publish_name_ (char * service_name, MPI_Fint * info, char * port_name, MPI_Fint * ierr, int  service_name_len, int  port_name_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (service_name_len);
	(void) (port_name_len);

	MPI_Publish_name(service_name, m_info, port_name);
}

int  MPI_File_get_group (MPI_File  fh, MPI_Group * group) {
	(void) (fh);
	(void) (group);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_group_ (MPI_Fint * fh, MPI_Fint * group, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Group* m_group = (MPI_Group*) group;
	(void) (ierr);

	(*ierr) = MPI_File_get_group(m_fh, m_group);
}

int  MPI_Type_hvector (int  count, int  blocklength, MPI_Aint  stride, MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (blocklength);
	(void) (stride);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_hvector_ (MPI_Fint * count, MPI_Fint * blocklength, MPI_Fint * stride, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_blocklength = (int*) blocklength;
	MPI_Aint* m_stride = (MPI_Aint*) stride;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_hvector(*m_count, *m_blocklength, *m_stride, m_oldtype, m_newtype);
}

int  MPI_Scan (void * sendbuf, void * recvbuf, int  count, MPI_Datatype  datatype, MPI_Op  op, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (recvbuf);
	(void) (count);
	(void) (datatype);
	(void) (op);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_scan_ (char * sendbuf, char * recvbuf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * op, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	void* m_recvbuf = (void*) recvbuf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Op m_op = MPI_Op_f2c (*op);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Scan(m_sendbuf, m_recvbuf, *m_count, m_datatype, m_op, m_comm);
}

int  MPI_Startall (int  count, MPI_Request * array_of_requests) {
	(void) (count);
	(void) (array_of_requests);

   //DLOG (INFO) << "MPI_Startall in/out loop";

   for (int i = 0; i < count; i++) {
      DCHECK (req2comms.find (array_of_requests [i]) != req2comms.end ()) << "Error: Could not find MPI_Start request";
   }

   return PMPI_Startall (count, array_of_requests);
}

void mpi_startall_ (MPI_Fint * count, MPI_Fint * array_of_requests, MPI_Fint * ierr) {
	int* m_count = (int*) count;
   MPI_Request m_array_of_requests [*m_count];

   for (int i = 0; i < *m_count; i++) {
      m_array_of_requests [i] = MPI_Request_f2c (array_of_requests [i]);
   }
	(void) (ierr);

	(*ierr) = MPI_Startall(*m_count, m_array_of_requests);

   for (int i = 0; i < *m_count; i++) {
      array_of_requests [i] = MPI_Request_c2f (m_array_of_requests [i]);
   }
}

int  MPI_Attr_delete (MPI_Comm  comm, int  keyval) {
	(void) (comm);
	(void) (keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_attr_delete_ (MPI_Fint * comm, MPI_Fint * keyval, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_keyval = (int*) keyval;
	(void) (ierr);

	(*ierr) = MPI_Attr_delete(m_comm, *m_keyval);
}

int  MPI_Type_get_envelope (MPI_Datatype  type, int * num_integers, int * num_addresses, int * num_datatypes, int * combiner) {
	(void) (type);
	(void) (num_integers);
	(void) (num_addresses);
	(void) (num_datatypes);
	(void) (combiner);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_get_envelope_ (MPI_Fint * type, MPI_Fint * num_integers, MPI_Fint * num_addresses, MPI_Fint * num_datatypes, MPI_Fint * combiner, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	int* m_num_integers = (int*) num_integers;
	int* m_num_addresses = (int*) num_addresses;
	int* m_num_datatypes = (int*) num_datatypes;
	int* m_combiner = (int*) combiner;
	(void) (ierr);

	(*ierr) = MPI_Type_get_envelope(m_type, m_num_integers, m_num_addresses, m_num_datatypes, m_combiner);
}

int  MPI_Comm_free (MPI_Comm * comm) {
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_free_ (MPI_Fint * comm, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Comm_free(&m_comm);
	(*comm) = MPI_Comm_c2f (m_comm);
}

int  MPI_File_get_info (MPI_File  fh, MPI_Info * info_used) {
	(void) (fh);
	(void) (info_used);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_info_ (MPI_Fint * fh, MPI_Fint * info_used, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Info* m_info_used = (MPI_Info*) info_used;
	(void) (ierr);

	(*ierr) = MPI_File_get_info(m_fh, m_info_used);
}

int  MPI_Errhandler_get (MPI_Comm  comm, MPI_Errhandler * errhandler) {
	(void) (comm);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_errhandler_get_ (MPI_Fint * comm, MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_Errhandler_get(m_comm, m_errhandler);
}

int  MPI_Error_class (int  errorcode, int * errorclass) {
	(void) (errorcode);
	(void) (errorclass);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_error_class_ (MPI_Fint * errorcode, MPI_Fint * errorclass, MPI_Fint * ierr) {
	int* m_errorcode = (int*) errorcode;
	int* m_errorclass = (int*) errorclass;
	(void) (ierr);

	(*ierr) = MPI_Error_class(*m_errorcode, m_errorclass);
}

int  MPI_Free_mem (void * base) {
	(void) (base);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_free_mem_ (char * base, MPI_Fint * ierr) {
	void* m_base = (void*) base;
	(void) (ierr);

	(*ierr) = MPI_Free_mem(m_base);
}

double  MPI_Wtime (void ) {
   return PMPI_Wtime ();
}

double mpi_wtime_ () {
	return MPI_Wtime();
}

int  MPI_Win_get_errhandler (MPI_Win  win, MPI_Errhandler * errhandler) {
	(void) (win);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_get_errhandler_ (MPI_Fint * win, MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_Win_get_errhandler(m_win, m_errhandler);
}

#ifndef ompi_fortran_logical_t
int  MPI_Comm_create_errhandler (MPI_Comm_errhandler_function * function, MPI_Errhandler * errhandler) {
	(void) (function);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}
#endif

void mpi_comm_create_errhandler_ (void *function, void *errhandler, void *ierr) {
	(void) (function);
	(void) (errhandler);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Add_error_class (int * errorclass) {
	(void) (errorclass);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_add_error_class_ (MPI_Fint * errorclass, MPI_Fint * ierr) {
	int* m_errorclass = (int*) errorclass;
	(void) (ierr);

	(*ierr) = MPI_Add_error_class(m_errorclass);
}

int  MPI_File_seek_shared (MPI_File  fh, MPI_Offset  offset, int  whence) {
	(void) (fh);
	(void) (offset);
	(void) (whence);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_seek_shared_ (MPI_Fint * fh, MPI_Offset * offset, MPI_Fint * whence, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	int* m_whence = (int*) whence;
	(void) (ierr);

	(*ierr) = MPI_File_seek_shared(m_fh, *m_offset, *m_whence);
}

int  MPI_File_get_type_extent (MPI_File  fh, MPI_Datatype  datatype, MPI_Aint * extent) {
	(void) (fh);
	(void) (datatype);
	(void) (extent);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_type_extent_ (MPI_Fint * fh, MPI_Fint * datatype, MPI_Aint * extent, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_File_get_type_extent(m_fh, m_datatype, extent);
}

int  MPI_Buffer_detach (void * buffer, int * size) {
	(void) (buffer);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_buffer_detach_ (char * buffer, MPI_Fint * size, MPI_Fint * ierr) {
	void* m_buffer = (void*) buffer;
	int* m_size = (int*) size;
	(void) (ierr);

	(*ierr) = MPI_Buffer_detach(m_buffer, m_size);
}

int  MPI_Keyval_free (int * keyval) {
	(void) (keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_keyval_free_ (MPI_Fint * keyval, MPI_Fint * ierr) {
	int* m_keyval = (int*) keyval;
	(void) (ierr);

	(*ierr) = MPI_Keyval_free(m_keyval);
}

int  MPI_Comm_get_attr (MPI_Comm  comm, int  comm_keyval, void * attribute_val, int * flag) {
	(void) (comm);
	(void) (comm_keyval);
	(void) (attribute_val);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_get_attr_ (MPI_Fint * comm, MPI_Fint * comm_keyval, MPI_Aint * attribute_val, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_comm_keyval = (int*) comm_keyval;
	void* m_attribute_val = (void*) attribute_val;
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Comm_get_attr(m_comm, *m_comm_keyval, m_attribute_val, m_flag);
}

int  MPI_Ibsend (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_ibsend_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_Ibsend(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, m_request);
}

int  MPI_File_write_at_all (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_at_all_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_at_all(m_fh, *m_offset, m_buf, *m_count, m_datatype, m_status);
}

//static int i = 0;
int  MPI_Allreduce (void * sendbuf, void * recvbuf, int  count, MPI_Datatype  datatype, MPI_Op  op, MPI_Comm  comm) {
   MPI_Group cgroup, dist_group;

   if (!in_loop) {
      //DLOG (ERROR) << "#" << sched_getcpu () << ": MPI_Allreduce outside of loop";
      return PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
   }

   //DLOG(ERROR) << "#" << sched_getcpu () << ": MPI_Allreduce in loop" << std::endl;

   LocalNode *ln = LocalNode::get_LN();
   if (ln->step_wants_comm_info()) {
      int is_inter = 0;

      ln->start_slack();
   
      // barrier to evaluate slack
      PMPI_Barrier(comm);

      PMPI_Comm_test_inter(comm, &is_inter);

      std::set<int> nodes;
      PMPI_Comm_group(comm, &cgroup);
      getGroupRanks(cgroup, nodes);

      // inter communicator impacts more nodes
      if (is_inter) {
         PMPI_Comm_remote_group(comm, &dist_group);
         getGroupRanks(dist_group, nodes);
      }

      ln->start_comm(CommInfo::MPI_ALLREDUCE, nodes, nodes);
   }

   //DCHECK (0) << "Error: here's it's gonna deadlock... Hehe";
   
   // original call
   //DLOG (ERROR) << "#" << sched_getcpu () << ": Just before Allreduce call";
   int cres = PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);

   ln->start_comp();

   //DLOG (ERROR) << "#" << sched_getcpu () << ": End of Allreduce";

   return cres;
}

void mpi_allreduce_ (char * sendbuf, char * recvbuf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * op, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	void* m_recvbuf = (void*) recvbuf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Op m_op = MPI_Op_f2c (*op);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Allreduce(m_sendbuf, m_recvbuf, *m_count, m_datatype, m_op, m_comm);
}

int  MPI_Comm_create_keyval (MPI_Comm_copy_attr_function * comm_copy_attr_fn, MPI_Comm_delete_attr_function * comm_delete_attr_fn, int * comm_keyval, void * extra_state) {
	(void) (comm_copy_attr_fn);
	(void) (comm_delete_attr_fn);
	(void) (comm_keyval);
	(void) (extra_state);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_create_keyval_ (void *comm_copy_attr_fn, void *comm_delete_attr_fn, void *comm_keyval, void *extra_state, void *ierr) {
	(void) (comm_copy_attr_fn);
	(void) (comm_delete_attr_fn);
	(void) (comm_keyval);
	(void) (extra_state);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Grequest_start (MPI_Grequest_query_function * query_fn, MPI_Grequest_free_function * free_fn, MPI_Grequest_cancel_function * cancel_fn, void * extra_state, MPI_Request * request) {
	(void) (query_fn);
	(void) (free_fn);
	(void) (cancel_fn);
	(void) (extra_state);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_grequest_start_ (MPI_F_Grequest_query_function* query_fn, MPI_F_Grequest_free_function* free_fn, MPI_F_Grequest_cancel_function* cancel_fn, MPI_Aint * extra_state, MPI_Fint * request, MPI_Fint * ierr) {
	MPI_Grequest_query_function* m_query_fn = (MPI_Grequest_query_function*) query_fn;
	MPI_Grequest_free_function* m_free_fn = (MPI_Grequest_free_function*) free_fn;
	MPI_Grequest_cancel_function* m_cancel_fn = (MPI_Grequest_cancel_function*) cancel_fn;
	void* m_extra_state = (void*) extra_state;
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_Grequest_start(m_query_fn, m_free_fn, m_cancel_fn, m_extra_state, m_request);
}

int  MPI_Comm_remote_size (MPI_Comm  comm, int * size) {
	(void) (comm);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_remote_size_ (MPI_Fint * comm, MPI_Fint * size, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_size = (int*) size;
	(void) (ierr);

	(*ierr) = MPI_Comm_remote_size(m_comm, m_size);
}

int  MPI_Probe (int  source, int  tag, MPI_Comm  comm, MPI_Status * status) {
	(void) (source);
	(void) (tag);
	(void) (comm);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_probe_ (MPI_Fint * source, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * status, MPI_Fint * ierr) {
	int* m_source = (int*) source;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_Probe(*m_source, *m_tag, m_comm, m_status);
}

int  MPI_Info_get (MPI_Info  info, char * key, int  valuelen, char * value, int * flag) {
	(void) (info);
	(void) (key);
	(void) (valuelen);
	(void) (value);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_get_ (MPI_Fint * info, char * key, MPI_Fint * valuelen, char * value, ompi_fortran_logical_t * flag, MPI_Fint * ierr, int  key_len, int  value_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	int* m_valuelen = (int*) valuelen;
	int* m_flag = (int*) flag;
	(void) (ierr);
	(void) (key_len);
	(void) (value_len);

	MPI_Info_get(m_info, key, *m_valuelen, value, m_flag);
}

int  MPI_File_iwrite_at (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype, MPI_Request * request) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_iwrite_at_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * request, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_File_iwrite_at(m_fh, *m_offset, m_buf, *m_count, m_datatype, m_request);
}

int  MPI_Reduce (void * sendbuf, void * recvbuf, int  count, MPI_Datatype  datatype, MPI_Op  op, int  root, MPI_Comm  comm) {
   int crank, root_urank;
   MPI_Group cgroup, ugroup, dist_group;
   int is_inter = 0;
   bool is_receiver;
   LocalNode *ln = LocalNode::get_LN();

   if (!in_loop) {
      return PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);
   }

   //DLOG (INFO) << "MPI_Reduce in loop";
   if (ln->step_wants_comm_info()) {
      ln->start_slack();

      PMPI_Barrier(comm);

      PMPI_Comm_test_inter(comm, &is_inter);
      PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
      PMPI_Comm_group(comm, &cgroup);
      PMPI_Group_rank(cgroup, &crank);

      // supporting intercommunicator reduce implies to determine the root id in
      // the first group, which we do not do right now.
      if (is_inter) {
         printf("MPI_Reduce on intercommunicators not supported so far.\n");
         exit(1);
      }

      is_receiver = (is_inter && root == MPI_ROOT) || (!is_inter && root == crank);
      
      CommInfo commInfo;
      // distinguish between sending and receiving nodes
      if (is_receiver) {
         // everyone sends me something
         std::set<int> nodes;
         //DLOG (INFO) << "is_receiver, so getGroupRanks" << std::endl;
         getGroupRanks(cgroup, nodes);
         if (is_inter) {
            PMPI_Comm_remote_group(comm, &dist_group);
            getGroupRanks(dist_group, nodes);
         }

         // I send something to myself
         PMPI_Group_translate_ranks(cgroup, 1, &root, ugroup, &root_urank);

         commInfo.insert_src (nodes);
         commInfo.insert_dst (root_urank);
         
         //DLOG (INFO) << "is_receiver, startcomm";
      } else {
         // I send to the root
         //DLOG (INFO) << "is not receiver, group translate ranks";
         PMPI_Group_translate_ranks(cgroup, 1, &root, ugroup, &root_urank);
         //DLOG (INFO) << "is_not_receiver node new std set";

         commInfo.insert_dst (root_urank);
         //DLOG (INFO) << "is_not_receiver start_comm";
      }
      ln->start_comm (CommInfo::MPI_REDUCE, commInfo.get_src (), commInfo.get_dst ());
   }
   
   // original call
   int cres = PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);

   ln->start_comp();

   return cres;  
}

void mpi_reduce_ (char * sendbuf, char * recvbuf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * op, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	void* m_recvbuf = (void*) recvbuf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Op m_op = MPI_Op_f2c (*op);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Reduce(m_sendbuf, m_recvbuf, *m_count, m_datatype, m_op, *m_root, m_comm);
}

int  MPI_File_iwrite (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Request * request) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_iwrite_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * request, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_File_iwrite(m_fh, m_buf, *m_count, m_datatype, m_request);
}

int  MPI_Pack_size (int  incount, MPI_Datatype  datatype, MPI_Comm  comm, int * size) {
   return PMPI_Pack_size (incount, datatype, comm, size);
}

void mpi_pack_size_ (MPI_Fint * incount, MPI_Fint * datatype, MPI_Fint * comm, MPI_Fint * size, MPI_Fint * ierr) {
	int* m_incount = (int*) incount;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_size = (int*) size;
	(void) (ierr);

	(*ierr) = MPI_Pack_size(*m_incount, m_datatype, m_comm, m_size);
}

int  MPI_Unpublish_name (char * service_name, MPI_Info  info, char * port_name) {
	(void) (service_name);
	(void) (info);
	(void) (port_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_unpublish_name_ (char * service_name, MPI_Fint * info, char * port_name, MPI_Fint * ierr, int  service_name_len, int  port_name_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (service_name_len);
	(void) (port_name_len);

	MPI_Unpublish_name(service_name, m_info, port_name);
}

int  MPI_Type_create_indexed_block (int  count, int  blocklength, int  array_of_displacements[], MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (blocklength);
	(void) (array_of_displacements);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_indexed_block_ (MPI_Fint * count, MPI_Fint * blocklength, MPI_Fint * array_of_displacements, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_blocklength = (int*) blocklength;
	int* m_array_of_displacements = (int*) array_of_displacements;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_indexed_block(*m_count, *m_blocklength, m_array_of_displacements, m_oldtype, m_newtype);
}

int  MPI_Abort (MPI_Comm  comm, int  errorcode) {
	(void) (comm);
	(void) (errorcode);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_abort_ (MPI_Fint * comm, MPI_Fint * errorcode, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_errorcode = (int*) errorcode;
	(void) (ierr);

	(*ierr) = MPI_Abort(m_comm, *m_errorcode);
}

int  MPI_File_preallocate (MPI_File  fh, MPI_Offset  size) {
	(void) (fh);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_preallocate_ (MPI_Fint * fh, MPI_Offset * size, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_size = (MPI_Offset*) size;
	(void) (ierr);

	(*ierr) = MPI_File_preallocate(m_fh, *m_size);
}

int  MPI_File_get_position (MPI_File  fh, MPI_Offset * offset) {
	(void) (fh);
	(void) (offset);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_position_ (MPI_Fint * fh, MPI_Offset * offset, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	(void) (ierr);

	(*ierr) = MPI_File_get_position(m_fh, offset);
}

int  MPI_Isend (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
   MPI_Group cgroup, ugroup;
   int dst_urank;
   LocalNode *ln = LocalNode::get_LN();
   
   if (!in_loop) {
      return PMPI_Isend(buf, count, datatype, dest, tag, comm, request);
   }


   int cres;
   if (ln->step_wants_comm_info()) {
      PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
      PMPI_Comm_group(comm, &cgroup);
      PMPI_Group_translate_ranks(cgroup, 1, &dest, ugroup, &dst_urank);

      cres = PMPI_Isend(buf, count, datatype, dest, tag, comm, request);
      //DLOG (INFO) << "MPI_Isend in loop " << RequestChecksum (request);

      req2comms[*request] = CommInfo ();
      req2comms [*request].insert_dst (dst_urank);
      req2comms [*request].set_comm_name (CommInfo::MPI_ISEND);
   } else {
      cres = PMPI_Isend(buf, count, datatype, dest, tag, comm, request);
   }
   
   return cres;
}

void mpi_isend_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request m_request = MPI_Request_f2c(*request);
	(void) (ierr);

	(*ierr) = MPI_Isend(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, &m_request);
   *request = MPI_Request_c2f(m_request);
   //DLOG (ERROR) << "Fmpi_isend_ " << RequestChecksumi (request);
}

int  MPI_Wait (MPI_Request * request, MPI_Status * status) {
   LocalNode *ln = LocalNode::get_LN();
   int cres;

   if (!in_loop || *request == MPI_REQUEST_NULL) {
      return PMPI_Wait (request, status);
   }

   //DLOG (ERROR) << "Wait: " << RequestChecksum (request);

   if (ln->step_wants_comm_info()) {
      // Assert that the process is ready to receive data
      DCHECK (req2comms.find (*request) != req2comms.end ()) << "Error: Cannot find request ";

      const CommInfo& commInfo = req2comms [*request];

      // WARNING: we assume all the wait time is due to stall which is not really
      // exact as it may also be due to comms
   
      ln->start_slack();

      cres = PMPI_Wait(request, status);
      
      ln->start_comm(commInfo.get_comm_name (), commInfo.get_src (), commInfo.get_dst ());
      
      // if comm is not persistent, we won't need it anymore
      if (!commInfo.persistent ()) {
         req2comms.erase (*request);
      }
   } else {
      cres = PMPI_Wait(request, status);
   }

   ln->start_comp();

   return cres; 
}

void mpi_wait_ (MPI_Fint * request, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_Request m_request = MPI_Request_f2c (*request);
	MPI_Status *m_status;
	MPI_Status m_status_r;
	if (status == MPI_STATUS_IGNORE_F) {
		m_status = MPI_STATUS_IGNORE;
	} else {
		MPI_Status_f2c (status, &m_status_r);
		m_status = &m_status_r;
	}
   //DLOG (ERROR) << "Fmpi_wait_ " << RequestChecksumi (request);
	(void) (ierr);

	(*ierr) = MPI_Wait(&m_request, m_status);
	(*request) = MPI_Request_c2f (m_request);
	if (m_status != MPI_STATUS_IGNORE) {
		MPI_Status_c2f (m_status, status);
	}
}

int  MPI_Win_free (MPI_Win * win) {
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_free_ (MPI_Fint * win, MPI_Fint * ierr) {
	MPI_Win* m_win = (MPI_Win*) win;
	(void) (ierr);

	(*ierr) = MPI_Win_free(m_win);
}

int  MPI_Start (MPI_Request * request) {
	(void) (request);

   DCHECK (req2comms.find (*request) != req2comms.end ()) << "Error: Could not find MPI_Start request";

   return PMPI_Start (request);
}

void mpi_start_ (MPI_Fint * request, MPI_Fint * ierr) {
	MPI_Request m_request = MPI_Request_f2c (*request);
	(void) (ierr);

   //DLOG (INFO) << "Fmpi_start_ " << RequestChecksumi (request);

	(*ierr) = MPI_Start(&m_request);
   *request = MPI_Request_c2f (m_request);
}

int  MPI_Errhandler_set (MPI_Comm  comm, MPI_Errhandler  errhandler) {
	(void) (comm);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_errhandler_set_ (MPI_Fint * comm, MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_Errhandler_set(m_comm, *m_errhandler);
}

int  MPI_Comm_get_errhandler (MPI_Comm  comm, MPI_Errhandler * erhandler) {
	(void) (comm);
	(void) (erhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_get_errhandler_ (MPI_Fint * comm, MPI_Fint * erhandler, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Errhandler* m_erhandler = (MPI_Errhandler*) erhandler;
	(void) (ierr);

	(*ierr) = MPI_Comm_get_errhandler(m_comm, m_erhandler);
}

int  MPI_Group_translate_ranks (MPI_Group  group1, int  n, int * ranks1, MPI_Group  group2, int * ranks2) {
	(void) (group1);
	(void) (n);
	(void) (ranks1);
	(void) (group2);
	(void) (ranks2);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_translate_ranks_ (MPI_Fint * group1, MPI_Fint * n, MPI_Fint * ranks1, MPI_Fint * group2, MPI_Fint * ranks2, MPI_Fint * ierr) {
	MPI_Group m_group1 = MPI_Group_f2c (*group1);
	int* m_n = (int*) n;
	int* m_ranks1 = (int*) ranks1;
	MPI_Group m_group2 = MPI_Group_f2c (*group2);
	int* m_ranks2 = (int*) ranks2;
	(void) (ierr);

	(*ierr) = MPI_Group_translate_ranks(m_group1, *m_n, m_ranks1, m_group2, m_ranks2);
}

int  MPI_Testsome (int  incount, MPI_Request  array_of_requests[], int * outcount, int  array_of_indices[], MPI_Status  array_of_statuses[]) {
	(void) (incount);
	(void) (array_of_requests);
	(void) (outcount);
	(void) (array_of_indices);
	(void) (array_of_statuses);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_testsome_ (MPI_Fint * incount, MPI_Fint * array_of_requests, MPI_Fint * outcount, MPI_Fint * array_of_indices, MPI_Fint * array_of_statuses, MPI_Fint * ierr) {
	int* m_incount = (int*) incount;
	MPI_Request* m_array_of_requests = (MPI_Request*) array_of_requests;
	int* m_outcount = (int*) outcount;
	int* m_array_of_indices = (int*) array_of_indices;
	MPI_Status* m_array_of_statuses = (MPI_Status*) array_of_statuses;
	(void) (ierr);

	(*ierr) = MPI_Testsome(*m_incount, m_array_of_requests, m_outcount, m_array_of_indices, m_array_of_statuses);
}

int  MPI_Recv_init (void * buf, int  count, MPI_Datatype  datatype, int  source, int  tag, MPI_Comm  comm, MPI_Request * request) {
   MPI_Group cgroup, ugroup;
   int source_urank;

   //DLOG (INFO) << "MPI_Recv_init in/out loop";

   //DCHECK (req2comms.find (*request) == req2comms.end ()) << "Error: Already existing request in internal map";
   
   if (source == MPI_ANY_SOURCE) {
      fprintf (stderr, "Recv_init from any source cannot be supported\n");
      exit (EXIT_FAILURE);
   }

   PMPI_Comm_group (MPI_COMM_WORLD, &ugroup);
   PMPI_Comm_group (comm, &cgroup);
   PMPI_Group_translate_ranks (cgroup, 1, &source, ugroup, &source_urank);

   int cres = PMPI_Recv_init (buf, count, datatype, source, tag, comm, request);
   
   req2comms [*request] = CommInfo ();
   req2comms [*request].insert_src (source_urank);
   req2comms [*request].set_persistent (true);
   req2comms [*request].set_comm_name (CommInfo::MPI_RECV_INIT);

   return cres;
}

void mpi_recv_init_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * source, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_source = (int*) source;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request m_request = MPI_Request_f2c (*request);
	(void) (ierr);

	(*ierr) = MPI_Recv_init(m_buf, *m_count, m_datatype, *m_source, *m_tag, m_comm, &m_request);
   *request = MPI_Request_c2f (m_request);
   //DLOG (ERROR) << "Fmpi_recv_init_ " << RequestChecksumi (request);
}

int  MPI_Win_lock (int  lock_type, int  rank, int  assert, MPI_Win  win) {
	(void) (lock_type);
	(void) (rank);
	(void) (assert);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_lock_ (MPI_Fint * lock_type, MPI_Fint * rank, MPI_Fint * assert, MPI_Fint * win, MPI_Fint * ierr) {
	int* m_lock_type = (int*) lock_type;
	int* m_rank = (int*) rank;
	int* m_assert = (int*) assert;
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Win_lock(*m_lock_type, *m_rank, *m_assert, m_win);
}

int  MPI_Send_init (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
   MPI_Group cgroup, ugroup;
   int source_urank;

   //DLOG (INFO) << "MPI_Send_init in/out loop";
   
   //DCHECK (req2comms.find (*request) == req2comms.end ()) << "Error: Already existing request in internal map";
   
   PMPI_Comm_group (MPI_COMM_WORLD, &ugroup);
   PMPI_Comm_group (comm, &cgroup);
   PMPI_Group_translate_ranks (cgroup, 1, &dest, ugroup, &source_urank);

   int cres = PMPI_Send_init (buf, count, datatype, dest, tag, comm, request);
   
   req2comms [*request] = CommInfo ();
   req2comms [*request].insert_dst (source_urank);
   req2comms [*request].set_persistent (true);
   req2comms [*request].set_comm_name (CommInfo::MPI_SEND_INIT);

   return cres;
}

void mpi_send_init_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request m_request = MPI_Request_f2c (*request);
	(void) (ierr);

	(*ierr) = MPI_Send_init(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, &m_request);
   *request = MPI_Request_c2f (m_request);
   //DLOG (ERROR) << "Fmpi_send_init_ " << RequestChecksumi (request);
}

int  MPI_Scatterv (void * sendbuf, int * sendcounts, int * displs, MPI_Datatype  sendtype, void * recvbuf, int  recvcount, MPI_Datatype  recvtype, int  root, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (sendcounts);
	(void) (displs);
	(void) (sendtype);
	(void) (recvbuf);
	(void) (recvcount);
	(void) (recvtype);
	(void) (root);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_scatterv_ (char * sendbuf, MPI_Fint * sendcounts, MPI_Fint * displs, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcount, MPI_Fint * recvtype, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcounts = (int*) sendcounts;
	int* m_displs = (int*) displs;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcount = (int*) recvcount;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Scatterv(m_sendbuf, m_sendcounts, m_displs, m_sendtype, m_recvbuf, *m_recvcount, m_recvtype, *m_root, m_comm);
}

int  MPI_Info_get_nthkey (MPI_Info  info, int  n, char * key) {
	(void) (info);
	(void) (n);
	(void) (key);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_get_nthkey_ (MPI_Fint * info, MPI_Fint * n, char * key, MPI_Fint * ierr, int  key_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	int* m_n = (int*) n;
	(void) (ierr);
	(void) (key_len);

	MPI_Info_get_nthkey(m_info, *m_n, key);
}

int  MPI_Add_error_code (int  errorclass, int * errorcode) {
	(void) (errorclass);
	(void) (errorcode);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_add_error_code_ (MPI_Fint * errorclass, MPI_Fint * errorcode, MPI_Fint * ierr) {
	int* m_errorclass = (int*) errorclass;
	int* m_errorcode = (int*) errorcode;
	(void) (ierr);

	(*ierr) = MPI_Add_error_code(*m_errorclass, m_errorcode);
}

int  MPI_Type_create_resized (MPI_Datatype  oldtype, MPI_Aint  lb, MPI_Aint  extent, MPI_Datatype * newtype) {
	(void) (oldtype);
	(void) (lb);
	(void) (extent);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_resized_ (MPI_Fint * oldtype, MPI_Aint * lb, MPI_Aint * extent, MPI_Fint * newtype, MPI_Fint * ierr) {
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Aint* m_lb = (MPI_Aint*) lb;
	MPI_Aint* m_extent = (MPI_Aint*) extent;
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_resized(m_oldtype, *m_lb, *m_extent, m_newtype);
}

int  MPI_File_get_size (MPI_File  fh, MPI_Offset * size) {
	(void) (fh);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_size_ (MPI_Fint * fh, MPI_Offset * size, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	(void) (ierr);

	(*ierr) = MPI_File_get_size(m_fh, size);
}

int  MPI_Finalized (int * flag) {
	(void) (flag);

	return PMPI_Finalized (flag);
}

void mpi_finalized_ (ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Finalized(m_flag);
}

int  MPI_Get_address (void * location, MPI_Aint * address) {
	(void) (location);
	(void) (address);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_get_address_ (char * location, MPI_Aint * address, MPI_Fint * ierr) {
	void* m_location = (void*) location;
	(void) (ierr);

	(*ierr) = MPI_Get_address(m_location, address);
}

int  MPI_Waitall (int  count, MPI_Request * array_of_requests, MPI_Status * array_of_statuses) {
   LocalNode *ln = LocalNode::get_LN();
   int cres;
   
   if (!in_loop) {
      return PMPI_Waitall (count, array_of_requests, array_of_statuses);
   }
   //DLOG (INFO) << "MPI_Waitall in loop ";

   if (ln->step_wants_comm_info()) {
      // Assert that the process is ready to receive data
      CommInfo globalReqInfo;

      for (int i = 0; i < count; i++) {
         if (array_of_requests [i] == MPI_REQUEST_NULL) {
            continue;
         }

         //DLOG (INFO) << "Request " << RequestChecksum (array_of_requests+i);
         DCHECK (req2comms.find(array_of_requests[i]) != req2comms.end());

         const CommInfo& commInfo = req2comms[array_of_requests[i]];

         globalReqInfo.insert_src (commInfo.get_src ());
         globalReqInfo.insert_dst (commInfo.get_dst ());

         if (!commInfo.persistent ()) {
            req2comms.erase(array_of_requests[i]);
         }
      }

      // WARNING: we assume all the wait time is due to stall which is not really
      // exact as it may also be due to comms

      ln->start_slack();

      cres = PMPI_Waitall(count, array_of_requests, array_of_statuses);

      ln->start_comm(CommInfo::MPI_WAITALL, globalReqInfo.get_src (), globalReqInfo.get_dst ());
   } else {
      cres = PMPI_Waitall(count, array_of_requests, array_of_statuses);
   }

   ln->start_comp();

   return cres;
}

void mpi_waitall_ (MPI_Fint * count, MPI_Fint * array_of_requests, MPI_Fint * array_of_statuses, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	MPI_Request m_array_of_requests [*m_count];
   MPI_Status st_buf[*m_count];
   MPI_Status *m_array_of_statuses;

   if (array_of_statuses == MPI_STATUSES_IGNORE_F || array_of_statuses == MPI_STATUS_IGNORE_F) {
      m_array_of_statuses = MPI_STATUSES_IGNORE;
   } else {
      m_array_of_statuses = st_buf;
   }

   for (int i = 0; i < *m_count; ++i) {
      //DLOG (ERROR) << "Fmpi_waitall_[" << i << "] " << RequestChecksumi (array_of_requests+i);

      m_array_of_requests[i] = MPI_Request_f2c(array_of_requests[i]);

      if (m_array_of_statuses != MPI_STATUSES_IGNORE) {
         MPI_Status_f2c(array_of_statuses + (i * MPI_STATUS_SIZE), m_array_of_statuses + i);
      }
   }

	(*ierr) = MPI_Waitall(*m_count, m_array_of_requests, m_array_of_statuses);

   for (int i = 0; i < *m_count; ++i) {
      array_of_requests[i] = MPI_Request_c2f(m_array_of_requests[i]);

      if (m_array_of_statuses != MPI_STATUSES_IGNORE) {
         MPI_Status_c2f(m_array_of_statuses + i, array_of_statuses + (i * MPI_STATUS_SIZE));
      }
   }
}

int  MPI_Waitany (int  count, MPI_Request * array_of_requests, int * index, MPI_Status * status) {
   if (!in_loop) {
      return PMPI_Waitany (count, array_of_requests, index, status);
   }

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_waitany_ (MPI_Fint * count, MPI_Fint * array_of_requests, MPI_Fint * index, MPI_Fint * status, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	MPI_Request* m_array_of_requests = (MPI_Request*) array_of_requests;
	int* m_index = (int*) index;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_Waitany(*m_count, m_array_of_requests, m_index, m_status);
}

int  MPI_Get_version (int * version, int * subversion) {
	(void) (version);
	(void) (subversion);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_get_version_ (MPI_Fint * version, MPI_Fint * subversion, MPI_Fint * ierr) {
	int* m_version = (int*) version;
	int* m_subversion = (int*) subversion;
	(void) (ierr);

	(*ierr) = MPI_Get_version(m_version, m_subversion);
}

int  MPI_Pack (void * inbuf, int  incount, MPI_Datatype  datatype, void * outbuf, int  outsize, int * position, MPI_Comm  comm) {
   return PMPI_Pack (inbuf, incount, datatype, outbuf, outsize, position, comm);
}

void mpi_pack_ (char * inbuf, MPI_Fint * incount, MPI_Fint * datatype, char * outbuf, MPI_Fint * outsize, MPI_Fint * position, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_inbuf = (void*) inbuf;
	int* m_incount = (int*) incount;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	void* m_outbuf = (void*) outbuf;
	int* m_outsize = (int*) outsize;
	int* m_position = (int*) position;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Pack(m_inbuf, *m_incount, m_datatype, m_outbuf, *m_outsize, m_position, m_comm);
}

int  MPI_Op_commutative (MPI_Op  op, int * commute) {
	(void) (op);
	(void) (commute);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_op_commutative_ (MPI_Fint * op, MPI_Fint * commute, MPI_Fint * ierr) {
	MPI_Op m_op = MPI_Op_f2c (*op);
	int* m_commute = (int*) commute;
	(void) (ierr);

	(*ierr) = MPI_Op_commutative(m_op, m_commute);
}

int  MPI_Gather (void * sendbuf, int  sendcount, MPI_Datatype  sendtype, void * recvbuf, int  recvcount, MPI_Datatype  recvtype, int  root, MPI_Comm  comm) {
   int crank, root_urank;
   MPI_Group cgroup, ugroup, dist_group;
   int is_inter = 0;
   bool is_receiver;
   LocalNode *ln = LocalNode::get_LN();

   if (!in_loop) {
      return PMPI_Gather (sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);
   }

   if (ln->step_wants_comm_info()) {
      ln->start_slack();

      PMPI_Barrier(comm);

      PMPI_Comm_test_inter(comm, &is_inter);
      PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
      PMPI_Comm_group(comm, &cgroup);
      PMPI_Group_rank(cgroup, &crank);

      // supporting intercommunicator reduce implies to determine the root id in
      // the first group, which we do not do right now.
      if (is_inter) {
         printf("MPI_Gather on intercommunicators not supported so far.\n");
         exit(1);
      }

      is_receiver = (is_inter && root == MPI_ROOT) || (!is_inter && root == crank);
      
      CommInfo commInfo;
      // distinguish between sending and receiving nodes
      if (is_receiver) {
         // everyone sends me something
         std::set<int> nodes;
         //DLOG (INFO) << "is_receiver, so getGroupRanks" << std::endl;
         getGroupRanks(cgroup, nodes);
         if (is_inter) {
            PMPI_Comm_remote_group(comm, &dist_group);
            getGroupRanks(dist_group, nodes);
         }

         // I send something to myself
         PMPI_Group_translate_ranks(cgroup, 1, &root, ugroup, &root_urank);

         commInfo.insert_src (nodes);
         commInfo.insert_dst (root_urank);
         
         //DLOG (INFO) << "is_receiver, startcomm";
      } else {
         // I send to the root
         //DLOG (INFO) << "is not receiver, group translate ranks";
         PMPI_Group_translate_ranks(cgroup, 1, &root, ugroup, &root_urank);
         //DLOG (INFO) << "is_not_receiver node new std set";

         commInfo.insert_dst (root_urank);
         //DLOG (INFO) << "is_not_receiver start_comm";
      }
      ln->start_comm (CommInfo::MPI_GATHER, commInfo.get_src (), commInfo.get_dst ());
   }
   
   // original call
   int cres = PMPI_Gather (sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);
   ln->start_comp();

   return cres;  
}

void mpi_gather_ (char * sendbuf, MPI_Fint * sendcount, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcount, MPI_Fint * recvtype, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcount = (int*) sendcount;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcount = (int*) recvcount;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Gather(m_sendbuf, *m_sendcount, m_sendtype, m_recvbuf, *m_recvcount, m_recvtype, *m_root, m_comm);
}

int  MPI_File_write (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_File_read_at_all_end (MPI_File  fh, void * buf, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_at_all_end_ (MPI_Fint * fh, char * buf, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_at_all_end(m_fh, m_buf, m_status);
}

int  MPI_Cartdim_get (MPI_Comm  comm, int * ndims) {
	(void) (comm);
	(void) (ndims);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cartdim_get_ (MPI_Fint * comm, MPI_Fint * ndims, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_ndims = (int*) ndims;
	(void) (ierr);

	(*ierr) = MPI_Cartdim_get(m_comm, m_ndims);
}

int  MPI_Allgather (void * sendbuf, int  sendcount, MPI_Datatype  sendtype, void * recvbuf, int  recvcount, MPI_Datatype  recvtype, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (sendcount);
	(void) (sendtype);
	(void) (recvbuf);
	(void) (recvcount);
	(void) (recvtype);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_allgather_ (char * sendbuf, MPI_Fint * sendcount, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcount, MPI_Fint * recvtype, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcount = (int*) sendcount;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcount = (int*) recvcount;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Allgather(m_sendbuf, *m_sendcount, m_sendtype, m_recvbuf, *m_recvcount, m_recvtype, m_comm);
}

int  MPI_File_set_view (MPI_File  fh, MPI_Offset  disp, MPI_Datatype  etype, MPI_Datatype  filetype, char * datarep, MPI_Info  info) {
	(void) (fh);
	(void) (disp);
	(void) (etype);
	(void) (filetype);
	(void) (datarep);
	(void) (info);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_set_view_ (MPI_Fint * fh, MPI_Offset * disp, MPI_Fint * etype, MPI_Fint * filetype, char * datarep, MPI_Fint * info, MPI_Fint * ierr, int  datarep_len) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_disp = (MPI_Offset*) disp;
	MPI_Datatype m_etype = MPI_Type_f2c (*etype);
	MPI_Datatype m_filetype = MPI_Type_f2c (*filetype);
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (datarep_len);

	MPI_File_set_view(m_fh, *m_disp, m_etype, m_filetype, datarep, m_info);
}

int  MPI_Win_complete (MPI_Win  win) {
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_complete_ (MPI_Fint * win, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Win_complete(m_win);
}

int  MPI_File_sync (MPI_File  fh) {
	(void) (fh);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_sync_ (MPI_Fint * fh, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	(void) (ierr);

	(*ierr) = MPI_File_sync(m_fh);
}

int  MPI_Send (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm) {
	MPI_Group cgroup, ugroup;
   int dst_urank;
   LocalNode *ln = LocalNode::get_LN();
   int cres;

   if (!in_loop) {
      return PMPI_Send(buf, count, datatype, dest, tag, comm);
   }
   DLOG (INFO) << "MPI_Send in loop";
   
   if (ln->step_wants_comm_info()) {
      PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
      PMPI_Comm_group(comm, &cgroup);
      PMPI_Group_translate_ranks(cgroup, 1, &dest, ugroup, &dst_urank);
      CommInfo commInfo;

      commInfo.insert_dst (dst_urank);

      ln->start_slack();
      ln->start_comm(CommInfo::MPI_SEND, commInfo.get_src (), commInfo.get_dst ());
   }

   cres = PMPI_Send(buf, count, datatype, dest, tag, comm);

   ln->start_comp();

   return cres;
}

void mpi_send_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Send(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm);
}

int  MPI_Status_set_elements (MPI_Status * status, MPI_Datatype  datatype, int  count) {
	(void) (status);
	(void) (datatype);
	(void) (count);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_status_set_elements_ (MPI_Fint * status, MPI_Fint * datatype, MPI_Fint * count, MPI_Fint * ierr) {
	MPI_Status* m_status = (MPI_Status*) status;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_count = (int*) count;
	(void) (ierr);

	(*ierr) = MPI_Status_set_elements(m_status, m_datatype, *m_count);
}

int  MPI_Rsend (void * ibuf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm) {
	(void) (ibuf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_rsend_ (char * ibuf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_ibuf = (void*) ibuf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Rsend(m_ibuf, *m_count, m_datatype, *m_dest, *m_tag, m_comm);
}

int  MPI_File_write_all (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_all_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_all(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_File_set_info (MPI_File  fh, MPI_Info  info) {
	(void) (fh);
	(void) (info);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_set_info_ (MPI_Fint * fh, MPI_Fint * info, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);

	(*ierr) = MPI_File_set_info(m_fh, m_info);
}

#ifndef ompi_fortran_logical_t
int  MPI_File_create_errhandler (MPI_File_errhandler_function * function, MPI_Errhandler * errhandler) {
	(void) (function);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}
#endif

void mpi_file_create_errhandler_ (void *function, void *errhandler, void *ierr) {
	(void) (function);
	(void) (errhandler);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Grequest_complete (MPI_Request  request) {
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_grequest_complete_ (MPI_Fint * request, MPI_Fint * ierr) {
	MPI_Request m_request = MPI_Request_f2c (*request);
	(void) (ierr);

	(*ierr) = MPI_Grequest_complete(m_request);
}

int  MPI_Comm_connect (char * port_name, MPI_Info  info, int  root, MPI_Comm  comm, MPI_Comm * newcomm) {
	(void) (port_name);
	(void) (info);
	(void) (root);
	(void) (comm);
	(void) (newcomm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_connect_ (char * port_name, MPI_Fint * info, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * newcomm, MPI_Fint * ierr, int  port_name_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Comm m_newcomm = MPI_Comm_f2c (*newcomm);
	(void) (ierr);
	(void) (port_name_len);

	MPI_Comm_connect(port_name, m_info, *m_root, m_comm, &m_newcomm);
	(*newcomm) = MPI_Comm_c2f (m_newcomm);
}

int  MPI_Info_delete (MPI_Info  info, char * key) {
	(void) (info);
	(void) (key);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_delete_ (MPI_Fint * info, char * key, MPI_Fint * ierr, int  key_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (key_len);

	MPI_Info_delete(m_info, key);
}

int  MPI_Type_free_keyval (int * type_keyval) {
	(void) (type_keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_free_keyval_ (MPI_Fint * type_keyval, MPI_Fint * ierr) {
	int* m_type_keyval = (int*) type_keyval;
	(void) (ierr);

	(*ierr) = MPI_Type_free_keyval(m_type_keyval);
}

int  MPI_File_read (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_Pack_external_size (char * datarep, int  incount, MPI_Datatype  datatype, MPI_Aint * size) {
	(void) (datarep);
	(void) (incount);
	(void) (datatype);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_pack_external_size_ (char * datarep, MPI_Fint * incount, MPI_Fint * datatype, MPI_Aint * size, MPI_Fint * ierr) {
	int* m_incount = (int*) incount;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_Pack_external_size(datarep, *m_incount, m_datatype, size);
}

int  MPI_Comm_set_name (MPI_Comm  comm, char * comm_name) {
	(void) (comm);
	(void) (comm_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_set_name_ (MPI_Fint * comm, char * comm_name, MPI_Fint * ierr, int  name_len) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);
	(void) (name_len);

	MPI_Comm_set_name(m_comm, comm_name);
}

int  MPI_Comm_disconnect (MPI_Comm * comm) {
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_disconnect_ (MPI_Fint * comm, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Comm_disconnect(&m_comm);
	(*comm) = MPI_Comm_c2f (m_comm);
}

int  MPI_File_read_shared (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_shared_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_shared(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_File_write_ordered (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_ordered_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_ordered(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_File_read_all_begin (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_all_begin_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_File_read_all_begin(m_fh, m_buf, *m_count, m_datatype);
}

int  MPI_Alloc_mem (MPI_Aint  size, MPI_Info  info, void * baseptr) {
	(void) (size);
	(void) (info);
	(void) (baseptr);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_alloc_mem_ (MPI_Aint * size, MPI_Fint * info, char * baseptr, MPI_Fint * ierr) {
	MPI_Aint* m_size = (MPI_Aint*) size;
	MPI_Info m_info = MPI_Info_f2c (*info);
	void* m_baseptr = (void*) baseptr;
	(void) (ierr);

	(*ierr) = MPI_Alloc_mem(*m_size, m_info, m_baseptr);
}

int  MPI_Issend (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
   MPI_Group cgroup, ugroup;
   int dst_urank;
   LocalNode *ln = LocalNode::get_LN();
   
   if (!in_loop) {
      return PMPI_Issend(buf, count, datatype, dest, tag, comm, request);
   }


   int cres;
   if (ln->step_wants_comm_info()) {
      PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
      PMPI_Comm_group(comm, &cgroup);
      PMPI_Group_translate_ranks(cgroup, 1, &dest, ugroup, &dst_urank);

      cres = PMPI_Issend(buf, count, datatype, dest, tag, comm, request);
      //DLOG (INFO) << "MPI_Isend in loop " << RequestChecksum (request);

      req2comms[*request] = CommInfo ();
      req2comms [*request].insert_dst (dst_urank);
      req2comms [*request].set_comm_name (CommInfo::MPI_ISSEND);
   } else {
      cres = PMPI_Issend(buf, count, datatype, dest, tag, comm, request);
   }
   
   return cres;
}

void mpi_issend_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request m_request = MPI_Request_f2c (*request);
	(void) (ierr);

	(*ierr) = MPI_Issend(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, &m_request);
   *request = MPI_Request_c2f (m_request);
}

int  MPI_Type_size (MPI_Datatype  type, int * size) {
	(void) (type);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_size_ (MPI_Fint * type, MPI_Fint * size, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	int* m_size = (int*) size;
	(void) (ierr);

	(*ierr) = MPI_Type_size(m_type, m_size);
}

int  MPI_Open_port (MPI_Info  info, char * port_name) {
	(void) (info);
	(void) (port_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_open_port_ (MPI_Fint * info, char * port_name, MPI_Fint * ierr, int  port_name_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (port_name_len);

	MPI_Open_port(m_info, port_name);
}

int  MPI_File_set_errhandler (MPI_File  file, MPI_Errhandler  errhandler) {
	(void) (file);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_set_errhandler_ (MPI_Fint * file, MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_File m_file = MPI_File_f2c (*file);
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_File_set_errhandler(m_file, *m_errhandler);
}

int  MPI_Register_datarep (char * datarep, MPI_Datarep_conversion_function * read_conversion_fn, MPI_Datarep_conversion_function * write_conversion_fn, MPI_Datarep_extent_function * dtype_file_extent_fn, void * extra_state) {
	(void) (datarep);
	(void) (read_conversion_fn);
	(void) (write_conversion_fn);
	(void) (dtype_file_extent_fn);
	(void) (extra_state);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_register_datarep_ (void *datarep, void *read_conversion_fn, void *write_conversion_fn, void *dtype_file_extent_fn, void *extra_state, void *ierr, void *datarep_len) {
	(void) (datarep);
	(void) (read_conversion_fn);
	(void) (write_conversion_fn);
	(void) (dtype_file_extent_fn);
	(void) (extra_state);
	(void) (ierr);
	(void) (datarep_len);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Request_free (MPI_Request * request) {
	(void) (request);

   //DLOG (INFO) << "MPI_Request_free " << RequestChecksum (request);
   DCHECK (req2comms.find (*request) != req2comms.end ()) << "Error: Cannot find request in MPI_Request_free";
   req2comms.erase (*request);

   return PMPI_Request_free (request);
}

void mpi_request_free_ (MPI_Fint * request, MPI_Fint * ierr) {
	MPI_Request m_request = MPI_Request_f2c (*request);
	(void) (ierr);

	(*ierr) = MPI_Request_free(&m_request);
   *request = MPI_Request_c2f (m_request);
   //DLOG (INFO) << "Fmpi_request_free_ " << RequestChecksumi (request);
}

int  MPI_File_write_at (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_at_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_at(m_fh, *m_offset, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_Type_create_keyval (MPI_Type_copy_attr_function * type_copy_attr_fn, MPI_Type_delete_attr_function * type_delete_attr_fn, int * type_keyval, void * extra_state) {
	(void) (type_copy_attr_fn);
	(void) (type_delete_attr_fn);
	(void) (type_keyval);
	(void) (extra_state);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_keyval_ (void *type_copy_attr_fn, void *type_delete_attr_fn, void *type_keyval, void *extra_state, void *ierr) {
	(void) (type_copy_attr_fn);
	(void) (type_delete_attr_fn);
	(void) (type_keyval);
	(void) (extra_state);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Info_get_valuelen (MPI_Info  info, char * key, int * valuelen, int * flag) {
	(void) (info);
	(void) (key);
	(void) (valuelen);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_get_valuelen_ (MPI_Fint * info, char * key, MPI_Fint * valuelen, ompi_fortran_logical_t * flag, MPI_Fint * ierr, int  key_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	int* m_valuelen = (int*) valuelen;
	int* m_flag = (int*) flag;
	(void) (ierr);
	(void) (key_len);

	MPI_Info_get_valuelen(m_info, key, m_valuelen, m_flag);
}

int  MPI_Pack_external (char * datarep, void * inbuf, int  incount, MPI_Datatype  datatype, void * outbuf, MPI_Aint  outsize, MPI_Aint * position) {
	(void) (datarep);
	(void) (inbuf);
	(void) (incount);
	(void) (datatype);
	(void) (outbuf);
	(void) (outsize);
	(void) (position);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_pack_external_ (char * datarep, char * inbuf, MPI_Fint * incount, MPI_Fint * datatype, char * outbuf, MPI_Aint * outsize, MPI_Aint * position, MPI_Fint * ierr) {
	void* m_inbuf = (void*) inbuf;
	int* m_incount = (int*) incount;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	void* m_outbuf = (void*) outbuf;
	MPI_Aint* m_outsize = (MPI_Aint*) outsize;
	(void) (ierr);

	(*ierr) = MPI_Pack_external(datarep, m_inbuf, *m_incount, m_datatype, m_outbuf, *m_outsize, position);
}

int  MPI_Win_start (MPI_Group  group, int  assert, MPI_Win  win) {
	(void) (group);
	(void) (assert);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_start_ (MPI_Fint * group, MPI_Fint * assert, MPI_Fint * win, MPI_Fint * ierr) {
	MPI_Group m_group = MPI_Group_f2c (*group);
	int* m_assert = (int*) assert;
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Win_start(m_group, *m_assert, m_win);
}

int  MPI_Close_port (char * port_name) {
	(void) (port_name);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_close_port_ (char * port_name, MPI_Fint * ierr, int  port_name_len) {
	(void) (ierr);
	(void) (port_name_len);

	MPI_Close_port(port_name);
}

int  MPI_Type_create_f90_integer (int  r, MPI_Datatype * newtype) {
	(void) (r);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_f90_integer_ (MPI_Fint * r, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_r = (int*) r;
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_f90_integer(*m_r, m_newtype);
}

int  MPI_Type_create_subarray (int  ndims, int  size_array[], int  subsize_array[], int  start_array[], int  order, MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (ndims);
	(void) (size_array);
	(void) (subsize_array);
	(void) (start_array);
	(void) (order);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_subarray_ (MPI_Fint * ndims, MPI_Fint * size_array, MPI_Fint * subsize_array, MPI_Fint * start_array, MPI_Fint * order, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_ndims = (int*) ndims;
	int* m_size_array = (int*) size_array;
	int* m_subsize_array = (int*) subsize_array;
	int* m_start_array = (int*) start_array;
	int* m_order = (int*) order;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_subarray(*m_ndims, m_size_array, m_subsize_array, m_start_array, *m_order, m_oldtype, m_newtype);
}

int  MPI_Type_create_f90_real (int  p, int  r, MPI_Datatype * newtype) {
	(void) (p);
	(void) (r);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_f90_real_ (MPI_Fint * p, MPI_Fint * r, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_p = (int*) p;
	int* m_r = (int*) r;
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_f90_real(*m_p, *m_r, m_newtype);
}

int  MPI_File_delete (char * filename, MPI_Info  info) {
	(void) (filename);
	(void) (info);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_delete_ (char * filename, MPI_Fint * info, MPI_Fint * ierr, int  filename_len) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	(void) (ierr);
	(void) (filename_len);

	MPI_File_delete(filename, m_info);
}

int  MPI_Test_cancelled (MPI_Status * status, int * flag) {
	(void) (status);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_test_cancelled_ (MPI_Fint * status, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Status* m_status = (MPI_Status*) status;
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Test_cancelled(m_status, m_flag);
}

int  MPI_File_read_all (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_all_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_all(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_Win_get_group (MPI_Win  win, MPI_Group * group) {
	(void) (win);
	(void) (group);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_get_group_ (MPI_Fint * win, MPI_Fint * group, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	MPI_Group* m_group = (MPI_Group*) group;
	(void) (ierr);

	(*ierr) = MPI_Win_get_group(m_win, m_group);
}

int  MPI_Group_incl (MPI_Group  group, int  n, int * ranks, MPI_Group * newgroup) {
   return PMPI_Group_incl (group, n, ranks, newgroup);
}

void mpi_group_incl_ (MPI_Fint * group, MPI_Fint * n, MPI_Fint * ranks, MPI_Fint * newgroup, MPI_Fint * ierr) {
	MPI_Group m_group = MPI_Group_f2c (*group);
	int* m_n = (int*) n;
	int* m_ranks = (int*) ranks;
	MPI_Group m_newgroup = MPI_Group_f2c (*newgroup);
	(void) (ierr);

	(*ierr) = MPI_Group_incl(m_group, *m_n, m_ranks, &m_newgroup);
   *newgroup = MPI_Group_c2f (m_newgroup);
}

int  MPI_Irsend (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_irsend_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_Irsend(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, m_request);
}

int  MPI_Topo_test (MPI_Comm  comm, int * status) {
	(void) (comm);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_topo_test_ (MPI_Fint * comm, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_status = (int*) status;
	(void) (ierr);

	(*ierr) = MPI_Topo_test(m_comm, m_status);
}

int  MPI_File_read_all_end (MPI_File  fh, void * buf, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_all_end_ (MPI_Fint * fh, char * buf, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_all_end(m_fh, m_buf, m_status);
}

int  MPI_Get_processor_name (char * name, int * resultlen) {
   return PMPI_Get_processor_name (name, resultlen);
}

void mpi_get_processor_name_ (char * name, MPI_Fint * resultlen, MPI_Fint * ierr, int  name_len) {
	int* m_resultlen = (int*) resultlen;
	(void) (ierr);
	(void) (name_len);

	MPI_Get_processor_name(name, m_resultlen);
}

int  MPI_Group_size (MPI_Group  group, int * size) {
	(void) (group);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_size_ (MPI_Fint * group, MPI_Fint * size, MPI_Fint * ierr) {
	MPI_Group m_group = MPI_Group_f2c (*group);
	int* m_size = (int*) size;
	(void) (ierr);

	(*ierr) = MPI_Group_size(m_group, m_size);
}

int  MPI_File_get_errhandler (MPI_File  file, MPI_Errhandler * errhandler) {
	(void) (file);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_errhandler_ (MPI_Fint * file, MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_File m_file = MPI_File_f2c (*file);
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_File_get_errhandler(m_file, m_errhandler);
}

int  MPI_Attr_put (MPI_Comm  comm, int  keyval, void * attribute_val) {
	(void) (comm);
	(void) (keyval);
	(void) (attribute_val);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_attr_put_ (MPI_Fint * comm, MPI_Fint * keyval, MPI_Fint * attribute_val, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_keyval = (int*) keyval;
	void* m_attribute_val = (void*) attribute_val;
	(void) (ierr);

	(*ierr) = MPI_Attr_put(m_comm, *m_keyval, m_attribute_val);
}

int  MPI_Address (void * location, MPI_Aint * address) {
	(void) (location);
	(void) (address);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_address_ (char * location, MPI_Fint * address, MPI_Fint * ierr) {
	void* m_location = (void*) location;
	MPI_Aint* m_address = (MPI_Aint*) address;
	(void) (ierr);

	(*ierr) = MPI_Address(m_location, m_address);
}

int  MPI_Comm_compare (MPI_Comm  comm1, MPI_Comm  comm2, int * result) {
	(void) (comm1);
	(void) (comm2);
	(void) (result);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_compare_ (MPI_Fint * comm1, MPI_Fint * comm2, MPI_Fint * result, MPI_Fint * ierr) {
	MPI_Comm m_comm1 = MPI_Comm_f2c (*comm1);
	MPI_Comm m_comm2 = MPI_Comm_f2c (*comm2);
	int* m_result = (int*) result;
	(void) (ierr);

	(*ierr) = MPI_Comm_compare(m_comm1, m_comm2, m_result);
}

int  MPI_Win_get_attr (MPI_Win  win, int  win_keyval, void * attribute_val, int * flag) {
	(void) (win);
	(void) (win_keyval);
	(void) (attribute_val);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_get_attr_ (MPI_Fint * win, MPI_Fint * win_keyval, MPI_Aint * attribute_val, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	int* m_win_keyval = (int*) win_keyval;
	void* m_attribute_val = (void*) attribute_val;
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Win_get_attr(m_win, *m_win_keyval, m_attribute_val, m_flag);
}

int  MPI_Query_thread (int * provided) {
	(void) (provided);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_query_thread_ (MPI_Fint * provided, MPI_Fint * ierr) {
	int* m_provided = (int*) provided;
	(void) (ierr);

	(*ierr) = MPI_Query_thread(m_provided);
}

int  MPI_Alltoallw (void * sendbuf, int * sendcounts, int * sdispls, MPI_Datatype * sendtypes, void * recvbuf, int * recvcounts, int * rdispls, MPI_Datatype * recvtypes, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (sendcounts);
	(void) (sdispls);
	(void) (sendtypes);
	(void) (recvbuf);
	(void) (recvcounts);
	(void) (rdispls);
	(void) (recvtypes);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_alltoallw_ (char * sendbuf, MPI_Fint * sendcounts, MPI_Fint * sdispls, MPI_Fint * sendtypes, char * recvbuf, MPI_Fint * recvcounts, MPI_Fint * rdispls, MPI_Fint * recvtypes, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcounts = (int*) sendcounts;
	int* m_sdispls = (int*) sdispls;
	MPI_Datatype* m_sendtypes = (MPI_Datatype*) sendtypes;
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcounts = (int*) recvcounts;
	int* m_rdispls = (int*) rdispls;
	MPI_Datatype* m_recvtypes = (MPI_Datatype*) recvtypes;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Alltoallw(m_sendbuf, m_sendcounts, m_sdispls, m_sendtypes, m_recvbuf, m_recvcounts, m_rdispls, m_recvtypes, m_comm);
}

int  MPI_Alltoallv (void * sendbuf, int * sendcounts, int * sdispls, MPI_Datatype  sendtype, void * recvbuf, int * recvcounts, int * rdispls, MPI_Datatype  recvtype, MPI_Comm  comm) {
   MPI_Group cgroup, dist_group;
   int is_inter = 0;
   LocalNode *ln = LocalNode::get_LN();

   if (!in_loop) {
      return PMPI_Alltoallv(sendbuf, sendcounts, sdispls, sendtype, recvbuf, recvcounts, rdispls, recvtype, comm);
   }
   //DLOG (INFO) << "MPI_Alltoallv in loop";

   if (ln->step_wants_comm_info()) {
      ln->start_slack();

      // barrier to evaluate slack
      PMPI_Barrier(comm);

      PMPI_Comm_test_inter(comm, &is_inter);

      std::set<int> nodes;
      CommInfo commInfo;

      PMPI_Comm_group(comm, &cgroup);
      getGroupRanks(cgroup, nodes);
      if (is_inter) {
         PMPI_Comm_remote_group(comm, &dist_group);
         getGroupRanks(dist_group, nodes);
      }

      // filter out elements with whom we do not communicate
      unsigned int i = 0;
      for (std::set<int>::iterator it = nodes.begin(); it != nodes.end(); it++) {
         if (sendcounts[i] > 0) {
            commInfo.insert_dst (*it);
         }
         if (recvcounts[i] > 0) {
            commInfo.insert_src (*it);
         }
         i++;
      }

      ln->start_comm(CommInfo::MPI_ALLTOALLV, commInfo.get_src (), commInfo.get_dst ());
   }
   
   // original call
   int cres = PMPI_Alltoallv(sendbuf, sendcounts, sdispls, sendtype, recvbuf, recvcounts, rdispls, recvtype, comm);

   ln->start_comp();

   return cres;
}

void mpi_alltoallv_ (char * sendbuf, MPI_Fint * sendcounts, MPI_Fint * sdispls, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcounts, MPI_Fint * rdispls, MPI_Fint * recvtype, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcounts = (int*) sendcounts;
	int* m_sdispls = (int*) sdispls;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcounts = (int*) recvcounts;
	int* m_rdispls = (int*) rdispls;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Alltoallv(m_sendbuf, m_sendcounts, m_sdispls, m_sendtype, m_recvbuf, m_recvcounts, m_rdispls, m_recvtype, m_comm);
}

int  MPI_Bsend_init (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  tag, MPI_Comm  comm, MPI_Request * request) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (tag);
	(void) (comm);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_bsend_init_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * tag, MPI_Fint * comm, MPI_Fint * request, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_tag = (int*) tag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_Bsend_init(m_buf, *m_count, m_datatype, *m_dest, *m_tag, m_comm, m_request);
}

int  MPI_Exscan (void * sendbuf, void * recvbuf, int  count, MPI_Datatype  datatype, MPI_Op  op, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (recvbuf);
	(void) (count);
	(void) (datatype);
	(void) (op);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_exscan_ (char * sendbuf, char * recvbuf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * op, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	void* m_recvbuf = (void*) recvbuf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Op m_op = MPI_Op_f2c (*op);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Exscan(m_sendbuf, m_recvbuf, *m_count, m_datatype, m_op, m_comm);
}

int  MPI_Win_create_keyval (MPI_Win_copy_attr_function * win_copy_attr_fn, MPI_Win_delete_attr_function * win_delete_attr_fn, int * win_keyval, void * extra_state) {
	(void) (win_copy_attr_fn);
	(void) (win_delete_attr_fn);
	(void) (win_keyval);
	(void) (extra_state);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_create_keyval_ (void *win_copy_attr_fn, void *win_delete_attr_fn, void *win_keyval, void *extra_state, void *ierr) {
	(void) (win_copy_attr_fn);
	(void) (win_delete_attr_fn);
	(void) (win_keyval);
	(void) (extra_state);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Waitsome (int  incount, MPI_Request * array_of_requests, int * outcount, int * array_of_indices, MPI_Status * array_of_statuses) {
	(void) (incount);
	(void) (array_of_requests);
	(void) (outcount);
	(void) (array_of_indices);
	(void) (array_of_statuses);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_waitsome_ (MPI_Fint * incount, MPI_Fint * array_of_requests, MPI_Fint * outcount, MPI_Fint * array_of_indices, MPI_Fint * array_of_statuses, MPI_Fint * ierr) {
	int* m_incount = (int*) incount;
	MPI_Request* m_array_of_requests = (MPI_Request*) array_of_requests;
	int* m_outcount = (int*) outcount;
	int* m_array_of_indices = (int*) array_of_indices;
	MPI_Status* m_array_of_statuses = (MPI_Status*) array_of_statuses;
	(void) (ierr);

	(*ierr) = MPI_Waitsome(*m_incount, m_array_of_requests, m_outcount, m_array_of_indices, m_array_of_statuses);
}

int  MPI_Win_call_errhandler (MPI_Win  win, int  errorcode) {
	(void) (win);
	(void) (errorcode);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_call_errhandler_ (MPI_Fint * win, MPI_Fint * errorcode, MPI_Fint * ierr) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	int* m_errorcode = (int*) errorcode;
	(void) (ierr);

	(*ierr) = MPI_Win_call_errhandler(m_win, *m_errorcode);
}

int  MPI_Type_indexed (int  count, int  array_of_blocklengths[], int  array_of_displacements[], MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (array_of_blocklengths);
	(void) (array_of_displacements);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_indexed_ (MPI_Fint * count, MPI_Fint * array_of_blocklengths, MPI_Fint * array_of_displacements, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	int* m_array_of_blocklengths = (int*) array_of_blocklengths;
	int* m_array_of_displacements = (int*) array_of_displacements;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_indexed(*m_count, m_array_of_blocklengths, m_array_of_displacements, m_oldtype, m_newtype);
}

int  MPI_Errhandler_create (MPI_Handler_function * function, MPI_Errhandler * errhandler) {
	(void) (function);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_errhandler_create_ (void *function, void *errhandler, void *ierr) {
	(void) (function);
	(void) (errhandler);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Group_range_excl (MPI_Group  group, int  n, int  ranges[][3], MPI_Group * newgroup) {
	(void) (group);
	(void) (n);
	(void) (ranges[3]);
	(void) (newgroup);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_range_excl_ (void *group, void *n, void *ranges, void *newgroup, void *ierr) {
	(void) (group);
	(void) (n);
	(void) (ranges);
	(void) (newgroup);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Comm_split (MPI_Comm  comm, int  color, int  key, MPI_Comm * newcomm) {
	(void) (comm);
	(void) (color);
	(void) (key);
	(void) (newcomm);

	return PMPI_Comm_split (comm, color, key, newcomm);
}

void mpi_comm_split_ (MPI_Fint * comm, MPI_Fint * color, MPI_Fint * key, MPI_Fint * newcomm, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_color = (int*) color;
	int* m_key = (int*) key;
	MPI_Comm m_newcomm = MPI_Comm_f2c (*newcomm);
	(void) (ierr);

	(*ierr) = MPI_Comm_split(m_comm, *m_color, *m_key, &m_newcomm);
	(*newcomm) = MPI_Comm_c2f (m_newcomm);
}

int  MPI_File_get_view (MPI_File  fh, MPI_Offset * disp, MPI_Datatype * etype, MPI_Datatype * filetype, char * datarep) {
	(void) (fh);
	(void) (disp);
	(void) (etype);
	(void) (filetype);
	(void) (datarep);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_view_ (MPI_Fint * fh, MPI_Offset * disp, MPI_Fint * etype, MPI_Fint * filetype, char * datarep, MPI_Fint * ierr, int  datarep_len) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Datatype* m_etype = (MPI_Datatype*) etype;
	MPI_Datatype* m_filetype = (MPI_Datatype*) filetype;
	(void) (ierr);
	(void) (datarep_len);

	MPI_File_get_view(m_fh, disp, m_etype, m_filetype, datarep);
}

int  MPI_Request_get_status (MPI_Request  request, int * flag, MPI_Status * status) {
	(void) (request);
	(void) (flag);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_request_get_status_ (MPI_Fint * request, ompi_fortran_logical_t * flag, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_Request m_request = MPI_Request_f2c (*request);
	int* m_flag = (int*) flag;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_Request_get_status(m_request, m_flag, m_status);
}

int  MPI_Group_free (MPI_Group * group) {
   return PMPI_Group_free (group);
}

void mpi_group_free_ (MPI_Fint * group, MPI_Fint * ierr) {
	MPI_Group m_group = MPI_Group_f2c (*group);
	(void) (ierr);

	(*ierr) = MPI_Group_free(&m_group);
   *group = MPI_Group_c2f (m_group);
}

int  MPI_File_write_ordered_begin (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_ordered_begin_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_File_write_ordered_begin(m_fh, m_buf, *m_count, m_datatype);
}

int  MPI_File_get_position_shared (MPI_File  fh, MPI_Offset * offset) {
	(void) (fh);
	(void) (offset);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_get_position_shared_ (MPI_Fint * fh, MPI_Offset * offset, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	(void) (ierr);

	(*ierr) = MPI_File_get_position_shared(m_fh, offset);
}

int  MPI_Graphdims_get (MPI_Comm  comm, int * nnodes, int * nedges) {
	(void) (comm);
	(void) (nnodes);
	(void) (nedges);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_graphdims_get_ (MPI_Fint * comm, MPI_Fint * nnodes, MPI_Fint * nedges, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_nnodes = (int*) nnodes;
	int* m_nedges = (int*) nedges;
	(void) (ierr);

	(*ierr) = MPI_Graphdims_get(m_comm, m_nnodes, m_nedges);
}

int  MPI_Comm_rank (MPI_Comm  comm, int * rank) {
	(void) (comm);
	(void) (rank);

	return PMPI_Comm_rank (comm, rank);
}

void mpi_comm_rank_ (MPI_Fint * comm, MPI_Fint * rank, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_rank = (int*) rank;
	(void) (ierr);

	(*ierr) = MPI_Comm_rank(m_comm, m_rank);
}

int  MPI_Graph_neighbors (MPI_Comm  comm, int  rank, int  maxneighbors, int * neighbors) {
	(void) (comm);
	(void) (rank);
	(void) (maxneighbors);
	(void) (neighbors);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_graph_neighbors_ (MPI_Fint * comm, MPI_Fint * rank, MPI_Fint * maxneighbors, MPI_Fint * neighbors, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_rank = (int*) rank;
	int* m_maxneighbors = (int*) maxneighbors;
	int* m_neighbors = (int*) neighbors;
	(void) (ierr);

	(*ierr) = MPI_Graph_neighbors(m_comm, *m_rank, *m_maxneighbors, m_neighbors);
}

int  MPI_Cancel (MPI_Request * request) {
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cancel_ (MPI_Fint * request, MPI_Fint * ierr) {
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_Cancel(m_request);
}

int  MPI_Cart_map (MPI_Comm  comm, int  ndims, int * dims, int * periods, int * newrank) {
	(void) (comm);
	(void) (ndims);
	(void) (dims);
	(void) (periods);
	(void) (newrank);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cart_map_ (MPI_Fint * comm, MPI_Fint * ndims, MPI_Fint * dims, ompi_fortran_logical_t * periods, MPI_Fint * newrank, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_ndims = (int*) ndims;
	int* m_dims = (int*) dims;
	int* m_periods = (int*) periods;
	int* m_newrank = (int*) newrank;
	(void) (ierr);

	(*ierr) = MPI_Cart_map(m_comm, *m_ndims, m_dims, m_periods, m_newrank);
}

int  MPI_Group_rank (MPI_Group  group, int * rank) {
	(void) (group);
	(void) (rank);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_rank_ (MPI_Fint * group, MPI_Fint * rank, MPI_Fint * ierr) {
	MPI_Group m_group = MPI_Group_f2c (*group);
	int* m_rank = (int*) rank;
	(void) (ierr);

	(*ierr) = MPI_Group_rank(m_group, m_rank);
}

int  MPI_Scatter (void * sendbuf, int  sendcount, MPI_Datatype  sendtype, void * recvbuf, int  recvcount, MPI_Datatype  recvtype, int  root, MPI_Comm  comm) {
	(void) (sendbuf);
	(void) (sendcount);
	(void) (sendtype);
	(void) (recvbuf);
	(void) (recvcount);
	(void) (recvtype);
	(void) (root);
	(void) (comm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_scatter_ (char * sendbuf, MPI_Fint * sendcount, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcount, MPI_Fint * recvtype, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcount = (int*) sendcount;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcount = (int*) recvcount;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Scatter(m_sendbuf, *m_sendcount, m_sendtype, m_recvbuf, *m_recvcount, m_recvtype, *m_root, m_comm);
}

int  MPI_Buffer_attach (void * buffer, int  size) {
	(void) (buffer);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_buffer_attach_ (char * buffer, MPI_Fint * size, MPI_Fint * ierr) {
	void* m_buffer = (void*) buffer;
	int* m_size = (int*) size;
	(void) (ierr);

	(*ierr) = MPI_Buffer_attach(m_buffer, *m_size);
}

int  MPI_Errhandler_free (MPI_Errhandler * errhandler) {
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_errhandler_free_ (MPI_Fint * errhandler, MPI_Fint * ierr) {
	MPI_Errhandler* m_errhandler = (MPI_Errhandler*) errhandler;
	(void) (ierr);

	(*ierr) = MPI_Errhandler_free(m_errhandler);
}

int  MPI_Op_create (MPI_User_function * function, int  commute, MPI_Op * op) {
	(void) (function);
	(void) (commute);
	(void) (op);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_op_create_ (void *function, void *commute, void *op, void *ierr) {
	(void) (function);
	(void) (commute);
	(void) (op);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Type_contiguous (int  count, MPI_Datatype  oldtype, MPI_Datatype * newtype) {
	(void) (count);
	(void) (oldtype);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_contiguous_ (MPI_Fint * count, MPI_Fint * oldtype, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_count = (int*) count;
	MPI_Datatype m_oldtype = MPI_Type_f2c (*oldtype);
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_contiguous(*m_count, m_oldtype, m_newtype);
}

int  MPI_Type_delete_attr (MPI_Datatype  type, int  type_keyval) {
	(void) (type);
	(void) (type_keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_delete_attr_ (MPI_Fint * type, MPI_Fint * type_keyval, MPI_Fint * ierr) {
	MPI_Datatype m_type = MPI_Type_f2c (*type);
	int* m_type_keyval = (int*) type_keyval;
	(void) (ierr);

	(*ierr) = MPI_Type_delete_attr(m_type, *m_type_keyval);
}

int  MPI_Info_free (MPI_Info * info) {
	(void) (info);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_free_ (MPI_Fint * info, MPI_Fint * ierr) {
	MPI_Info* m_info = (MPI_Info*) info;
	(void) (ierr);

	(*ierr) = MPI_Info_free(m_info);
}

int  MPI_Bcast (void * buffer, int  count, MPI_Datatype  datatype, int  root, MPI_Comm  comm) {
   int crank, root_urank;
   MPI_Group cgroup, ugroup, dist_group;
   int is_inter = 0;
   bool is_sender;
   LocalNode *ln = LocalNode::get_LN();

   if (!in_loop) {
      return PMPI_Bcast(buffer, count, datatype, root, comm);
   }
   //DLOG (INFO) << "MPI_Bcast in loop";
   CommInfo commInfo;
   std::set<int> nodes;

   if (ln->step_wants_comm_info()) {
      ln->start_slack();

      PMPI_Barrier(comm);

      PMPI_Comm_test_inter(comm, &is_inter);

      // non communicating nodes in the group of the sender
      if (is_inter && root == MPI_PROC_NULL) {
         //int cres;

         //ln->start_comm(com, NULL);

         //cres = PMPI_Bcast(buffer, count, datatype, root, comm);

         //ln->start_comp();

         //return cres;
      } else {
   
         PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
         PMPI_Comm_group(comm, &cgroup);
         PMPI_Group_rank(cgroup, &crank);
   
         is_sender = (is_inter && root == MPI_ROOT) || (!is_inter && root == crank);
   
         // distinguish between sending and receiving nodes
         if (is_sender) {
            if (is_inter) {
               PMPI_Comm_remote_group(comm, &dist_group);
            } else {
               dist_group = cgroup;
            }
            getGroupRanks(dist_group, nodes);
            commInfo.insert_dst (nodes);
         } else {
            PMPI_Group_translate_ranks(cgroup, 1, &root, ugroup, &root_urank);
   
            commInfo.insert_src (root_urank);
         }
      }
   }

   ln->start_comm (CommInfo::MPI_BCAST, commInfo.get_src (), commInfo.get_dst ());
   
   // original call
   int cres = PMPI_Bcast(buffer, count, datatype, root, comm);

   ln->start_comp();

   return cres;
}

void mpi_bcast_ (char * buffer, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_buffer = (void*) buffer;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Bcast(m_buffer, *m_count, m_datatype, *m_root, m_comm);
}

int  MPI_Type_get_true_extent (MPI_Datatype  datatype, MPI_Aint * true_lb, MPI_Aint * true_extent) {
	(void) (datatype);
	(void) (true_lb);
	(void) (true_extent);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_get_true_extent_ (MPI_Fint * datatype, MPI_Aint * true_lb, MPI_Aint * true_extent, MPI_Fint * ierr) {
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	(void) (ierr);

	(*ierr) = MPI_Type_get_true_extent(m_datatype, true_lb, true_extent);
}

int  MPI_Sendrecv_replace (void * buf, int  count, MPI_Datatype  datatype, int  dest, int  sendtag, int  source, int  recvtag, MPI_Comm  comm, MPI_Status * status) {
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (dest);
	(void) (sendtag);
	(void) (source);
	(void) (recvtag);
	(void) (comm);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_sendrecv_replace_ (char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * dest, MPI_Fint * sendtag, MPI_Fint * source, MPI_Fint * recvtag, MPI_Fint * comm, MPI_Fint * status, MPI_Fint * ierr) {
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	int* m_dest = (int*) dest;
	int* m_sendtag = (int*) sendtag;
	int* m_source = (int*) source;
	int* m_recvtag = (int*) recvtag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_Sendrecv_replace(m_buf, *m_count, m_datatype, *m_dest, *m_sendtag, *m_source, *m_recvtag, m_comm, m_status);
}

int  MPI_Info_create (MPI_Info * info) {
	(void) (info);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_create_ (MPI_Fint * info, MPI_Fint * ierr) {
	MPI_Info* m_info = (MPI_Info*) info;
	(void) (ierr);

	(*ierr) = MPI_Info_create(m_info);
}

int  MPI_File_iwrite_shared (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Request * request) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_iwrite_shared_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * request, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_File_iwrite_shared(m_fh, m_buf, *m_count, m_datatype, m_request);
}

int  MPI_Put (void * origin_addr, int  origin_count, MPI_Datatype  origin_datatype, int  target_rank, MPI_Aint  target_disp, int  target_count, MPI_Datatype  target_datatype, MPI_Win  win) {
	(void) (origin_addr);
	(void) (origin_count);
	(void) (origin_datatype);
	(void) (target_rank);
	(void) (target_disp);
	(void) (target_count);
	(void) (target_datatype);
	(void) (win);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_put_ (char * origin_addr, MPI_Fint * origin_count, MPI_Fint * origin_datatype, MPI_Fint * target_rank, MPI_Aint * target_disp, MPI_Fint * target_count, MPI_Fint * target_datatype, MPI_Fint * win, MPI_Fint * ierr) {
	void* m_origin_addr = (void*) origin_addr;
	int* m_origin_count = (int*) origin_count;
	MPI_Datatype m_origin_datatype = MPI_Type_f2c (*origin_datatype);
	int* m_target_rank = (int*) target_rank;
	MPI_Aint* m_target_disp = (MPI_Aint*) target_disp;
	int* m_target_count = (int*) target_count;
	MPI_Datatype m_target_datatype = MPI_Type_f2c (*target_datatype);
	MPI_Win m_win = MPI_Win_f2c (*win);
	(void) (ierr);

	(*ierr) = MPI_Put(m_origin_addr, *m_origin_count, m_origin_datatype, *m_target_rank, *m_target_disp, *m_target_count, m_target_datatype, m_win);
}

int  MPI_File_read_ordered_end (MPI_File  fh, void * buf, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_ordered_end_ (MPI_Fint * fh, char * buf, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_ordered_end(m_fh, m_buf, m_status);
}

int  MPI_Comm_call_errhandler (MPI_Comm  comm, int  errorcode) {
	(void) (comm);
	(void) (errorcode);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_call_errhandler_ (MPI_Fint * comm, MPI_Fint * errorcode, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_errorcode = (int*) errorcode;
	(void) (ierr);

	(*ierr) = MPI_Comm_call_errhandler(m_comm, *m_errorcode);
}

int  MPI_Comm_spawn (char * command, char ** argv, int  maxprocs, MPI_Info  info, int  root, MPI_Comm  comm, MPI_Comm * intercomm, int * array_of_errcodes) {
	(void) (command);
	(void) (argv);
	(void) (maxprocs);
	(void) (info);
	(void) (root);
	(void) (comm);
	(void) (intercomm);
	(void) (array_of_errcodes);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_spawn_ (char * command, char * argv, MPI_Fint * maxprocs, MPI_Fint * info, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * intercomm, MPI_Fint * array_of_errcodes, MPI_Fint * ierr, int  command_len, int  argv_len) {
	char** m_argv = (char**) argv;
	int* m_maxprocs = (int*) maxprocs;
	MPI_Info m_info = MPI_Info_f2c (*info);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Comm m_intercomm = MPI_Comm_f2c (*intercomm);
	int* m_array_of_errcodes = (int*) array_of_errcodes;
	(void) (ierr);
	(void) (command_len);
	(void) (argv_len);

	MPI_Comm_spawn(command, m_argv, *m_maxprocs, m_info, *m_root, m_comm, &m_intercomm, m_array_of_errcodes);
	(*intercomm) = MPI_Comm_c2f (m_intercomm);
}

int  MPI_Info_dup (MPI_Info  info, MPI_Info * newinfo) {
	(void) (info);
	(void) (newinfo);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_info_dup_ (MPI_Fint * info, MPI_Fint * newinfo, MPI_Fint * ierr) {
	MPI_Info m_info = MPI_Info_f2c (*info);
	MPI_Info* m_newinfo = (MPI_Info*) newinfo;
	(void) (ierr);

	(*ierr) = MPI_Info_dup(m_info, m_newinfo);
}

int  MPI_Comm_test_inter (MPI_Comm  comm, int * flag) {
	(void) (comm);
	(void) (flag);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_test_inter_ (MPI_Fint * comm, ompi_fortran_logical_t * flag, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_flag = (int*) flag;
	(void) (ierr);

	(*ierr) = MPI_Comm_test_inter(m_comm, m_flag);
}

#ifndef ompi_fortran_logical_t
int  MPI_Win_create_errhandler (MPI_Win_errhandler_function * function, MPI_Errhandler * errhandler) {
	(void) (function);
	(void) (errhandler);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}
#endif

void mpi_win_create_errhandler_ (void *function, void *errhandler, void *ierr) {
	(void) (function);
	(void) (errhandler);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Cart_get (MPI_Comm  comm, int  maxdims, int * dims, int * periods, int * coords) {
	(void) (comm);
	(void) (maxdims);
	(void) (dims);
	(void) (periods);
	(void) (coords);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_cart_get_ (MPI_Fint * comm, MPI_Fint * maxdims, MPI_Fint * dims, ompi_fortran_logical_t * periods, MPI_Fint * coords, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_maxdims = (int*) maxdims;
	int* m_dims = (int*) dims;
	int* m_periods = (int*) periods;
	int* m_coords = (int*) coords;
	(void) (ierr);

	(*ierr) = MPI_Cart_get(m_comm, *m_maxdims, m_dims, m_periods, m_coords);
}

int  MPI_Intercomm_merge (MPI_Comm  intercomm, int  high, MPI_Comm * newintercomm) {
	(void) (intercomm);
	(void) (high);
	(void) (newintercomm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_intercomm_merge_ (MPI_Fint * intercomm, ompi_fortran_logical_t * high, MPI_Fint * newintercomm, MPI_Fint * ierr) {
	MPI_Comm m_intercomm = MPI_Comm_f2c (*intercomm);
	int* m_high = (int*) high;
	MPI_Comm m_newintercomm = MPI_Comm_f2c (*newintercomm);
	(void) (ierr);

	(*ierr) = MPI_Intercomm_merge(m_intercomm, *m_high, &m_newintercomm);
	(*newintercomm) = MPI_Comm_c2f (m_newintercomm);
}

int  MPI_Win_free_keyval (int * win_keyval) {
	(void) (win_keyval);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_free_keyval_ (MPI_Fint * win_keyval, MPI_Fint * ierr) {
	int* m_win_keyval = (int*) win_keyval;
	(void) (ierr);

	(*ierr) = MPI_Win_free_keyval(m_win_keyval);
}

int  MPI_Keyval_create (MPI_Copy_function * copy_fn, MPI_Delete_function * delete_fn, int * keyval, void * extra_state) {
	(void) (copy_fn);
	(void) (delete_fn);
	(void) (keyval);
	(void) (extra_state);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_keyval_create_ (void *copy_fn, void *delete_fn, void *keyval, void *extra_state, void *ierr) {
	(void) (copy_fn);
	(void) (delete_fn);
	(void) (keyval);
	(void) (extra_state);
	(void) (ierr);

	fprintf (stderr, "Unsupported Fortran Wrapper: %s\n", __func__);
	exit (EXIT_FAILURE);
}

int  MPI_Type_create_f90_complex (int  p, int  r, MPI_Datatype * newtype) {
	(void) (p);
	(void) (r);
	(void) (newtype);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_type_create_f90_complex_ (MPI_Fint * p, MPI_Fint * r, MPI_Fint * newtype, MPI_Fint * ierr) {
	int* m_p = (int*) p;
	int* m_r = (int*) r;
	MPI_Datatype* m_newtype = (MPI_Datatype*) newtype;
	(void) (ierr);

	(*ierr) = MPI_Type_create_f90_complex(*m_p, *m_r, m_newtype);
}

int  MPI_File_write_shared (MPI_File  fh, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_write_shared_ (MPI_Fint * fh, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_write_shared(m_fh, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_Graph_map (MPI_Comm  comm, int  nnodes, int * index, int * edges, int * newrank) {
	(void) (comm);
	(void) (nnodes);
	(void) (index);
	(void) (edges);
	(void) (newrank);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_graph_map_ (MPI_Fint * comm, MPI_Fint * nnodes, MPI_Fint * index, MPI_Fint * edges, MPI_Fint * newrank, MPI_Fint * ierr) {
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	int* m_nnodes = (int*) nnodes;
	int* m_index = (int*) index;
	int* m_edges = (int*) edges;
	int* m_newrank = (int*) newrank;
	(void) (ierr);

	(*ierr) = MPI_Graph_map(m_comm, *m_nnodes, m_index, m_edges, m_newrank);
}

int  MPI_File_set_size (MPI_File  fh, MPI_Offset  size) {
	(void) (fh);
	(void) (size);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_set_size_ (MPI_Fint * fh, MPI_Offset * size, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_size = (MPI_Offset*) size;
	(void) (ierr);

	(*ierr) = MPI_File_set_size(m_fh, *m_size);
}

int  MPI_Intercomm_create (MPI_Comm  local_comm, int  local_leader, MPI_Comm  bridge_comm, int  remote_leader, int  tag, MPI_Comm * newintercomm) {
	(void) (local_comm);
	(void) (local_leader);
	(void) (bridge_comm);
	(void) (remote_leader);
	(void) (tag);
	(void) (newintercomm);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_intercomm_create_ (MPI_Fint * local_comm, MPI_Fint * local_leader, MPI_Fint * bridge_comm, MPI_Fint * remote_leader, MPI_Fint * tag, MPI_Fint * newintercomm, MPI_Fint * ierr) {
	MPI_Comm m_local_comm = MPI_Comm_f2c (*local_comm);
	int* m_local_leader = (int*) local_leader;
	MPI_Comm m_bridge_comm = MPI_Comm_f2c (*bridge_comm);
	int* m_remote_leader = (int*) remote_leader;
	int* m_tag = (int*) tag;
	MPI_Comm m_newintercomm = MPI_Comm_f2c (*newintercomm);
	(void) (ierr);

	(*ierr) = MPI_Intercomm_create(m_local_comm, *m_local_leader, m_bridge_comm, *m_remote_leader, *m_tag, &m_newintercomm);
	(*newintercomm) = MPI_Comm_c2f (m_newintercomm);
}

int  MPI_Win_get_name (MPI_Win  win, char * win_name, int * resultlen) {
	(void) (win);
	(void) (win_name);
	(void) (resultlen);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_win_get_name_ (MPI_Fint * win, char * win_name, MPI_Fint * resultlen, MPI_Fint * ierr, int  name_len) {
	MPI_Win m_win = MPI_Win_f2c (*win);
	int* m_resultlen = (int*) resultlen;
	(void) (ierr);
	(void) (name_len);

	MPI_Win_get_name(m_win, win_name, m_resultlen);
}

int  MPI_File_read_at_all (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype, MPI_Status * status) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (status);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_read_at_all_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * status, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Status* m_status = (MPI_Status*) status;
	(void) (ierr);

	(*ierr) = MPI_File_read_at_all(m_fh, *m_offset, m_buf, *m_count, m_datatype, m_status);
}

int  MPI_File_iread_at (MPI_File  fh, MPI_Offset  offset, void * buf, int  count, MPI_Datatype  datatype, MPI_Request * request) {
	(void) (fh);
	(void) (offset);
	(void) (buf);
	(void) (count);
	(void) (datatype);
	(void) (request);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_file_iread_at_ (MPI_Fint * fh, MPI_Offset * offset, char * buf, MPI_Fint * count, MPI_Fint * datatype, MPI_Fint * request, MPI_Fint * ierr) {
	MPI_File m_fh = MPI_File_f2c (*fh);
	MPI_Offset* m_offset = (MPI_Offset*) offset;
	void* m_buf = (void*) buf;
	int* m_count = (int*) count;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Request* m_request = (MPI_Request*) request;
	(void) (ierr);

	(*ierr) = MPI_File_iread_at(m_fh, *m_offset, m_buf, *m_count, m_datatype, m_request);
}

int  MPI_Gatherv (void * sendbuf, int  sendcount, MPI_Datatype  sendtype, void * recvbuf, int * recvcounts, int * displs, MPI_Datatype  recvtype, int  root, MPI_Comm  comm) {
	MPI_Group cgroup, dist_group;
   int is_inter = 0;
   LocalNode *ln = LocalNode::get_LN();

   if (!in_loop) {
      return PMPI_Gatherv (sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, root, comm);
   }
   //DLOG (INFO) << "MPI_Gatherv in loop";

   if (ln->step_wants_comm_info()) {
      ln->start_slack();

      // barrier to evaluate slack
      PMPI_Barrier(comm);

      PMPI_Comm_test_inter(comm, &is_inter);

      std::set<int> nodes;
      CommInfo commInfo;

      PMPI_Comm_group(comm, &cgroup);
      getGroupRanks(cgroup, nodes);
      if (is_inter) {
         PMPI_Comm_remote_group(comm, &dist_group);
         getGroupRanks(dist_group, nodes);
      }

      // filter out elements with whom we do not communicate
      unsigned int i = 0;
      commInfo.insert_dst (root);
      for (std::set<int>::iterator it = nodes.begin(); it != nodes.end(); it++) {
         if (recvcounts[i] > 0) {
            commInfo.insert_src (*it);
         }
         i++;
      }

      ln->start_comm(CommInfo::MPI_GATHERV, commInfo.get_src (), commInfo.get_dst ());
   }
   
   // original call
   int cres = PMPI_Gatherv (sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, root, comm);

   ln->start_comp();

   return cres;
}

void mpi_gatherv_ (char * sendbuf, MPI_Fint * sendcount, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcounts, MPI_Fint * displs, MPI_Fint * recvtype, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcount = (int*) sendcount;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcounts = (int*) recvcounts;
	int* m_displs = (int*) displs;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Gatherv(m_sendbuf, *m_sendcount, m_sendtype, m_recvbuf, m_recvcounts, m_displs, m_recvtype, *m_root, m_comm);
}

int  MPI_Alltoall (void * sendbuf, int  sendcount, MPI_Datatype  sendtype, void * recvbuf, int  recvcount, MPI_Datatype  recvtype, MPI_Comm  comm) {
   MPI_Group cgroup, dist_group;
   int is_inter = 0;
   LocalNode *ln = LocalNode::get_LN();

   if (!in_loop) {
      return PMPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
   }
   //DLOG (INFO) << "MPI_Alltoall in loop";

   if (ln->step_wants_comm_info()) {
      ln->start_slack();

      // barrier to evaluate slack
      PMPI_Barrier(comm);

      PMPI_Comm_test_inter(comm, &is_inter);

      std::set<int> nodes;
      std::set<int> empty;

      PMPI_Comm_group(comm, &cgroup);
      getGroupRanks(cgroup, nodes);
      if (is_inter) {
         PMPI_Comm_remote_group(comm, &dist_group);
         getGroupRanks(dist_group, nodes);
      }

      ln->start_comm(CommInfo::MPI_ALLTOALL,
                     (recvcount > 0 ? nodes : empty),
                     (sendcount > 0 ? nodes : empty));
   }
   
   // original call
   int cres = PMPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);

   ln->start_comp();

   return cres;
}

void mpi_alltoall_ (char * sendbuf, MPI_Fint * sendcount, MPI_Fint * sendtype, char * recvbuf, MPI_Fint * recvcount, MPI_Fint * recvtype, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcount = (int*) sendcount;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcount = (int*) recvcount;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Alltoall(m_sendbuf, *m_sendcount, m_sendtype, m_recvbuf, *m_recvcount, m_recvtype, m_comm);
}

int  MPI_Group_intersection (MPI_Group  group1, MPI_Group  group2, MPI_Group * newgroup) {
	(void) (group1);
	(void) (group2);
	(void) (newgroup);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_intersection_ (MPI_Fint * group1, MPI_Fint * group2, MPI_Fint * newgroup, MPI_Fint * ierr) {
	MPI_Group m_group1 = MPI_Group_f2c (*group1);
	MPI_Group m_group2 = MPI_Group_f2c (*group2);
	MPI_Group* m_newgroup = (MPI_Group*) newgroup;
	(void) (ierr);

	(*ierr) = MPI_Group_intersection(m_group1, m_group2, m_newgroup);
}

int  MPI_Sendrecv (void * sendbuf, int  sendcount, MPI_Datatype  sendtype, int  dest, int  sendtag, void * recvbuf, int  recvcount, MPI_Datatype  recvtype, int  source, int  recvtag, MPI_Comm  comm, MPI_Status * status) {
	MPI_Group cgroup, ugroup;
   int dst_urank, src_urank;
   LocalNode *ln = LocalNode::get_LN();
   int cres;

   if (!in_loop) {
      return PMPI_Sendrecv (sendbuf, sendcount, sendtype, dest, sendtag, recvbuf, recvcount, recvtype, source, recvtag, comm, status);
   }
   //DLOG (INFO) << "MPI_Sendrecv in loop";
   
   if (ln->step_wants_comm_info()) {
      PMPI_Comm_group(MPI_COMM_WORLD, &ugroup);
      PMPI_Comm_group(comm, &cgroup);
      PMPI_Group_translate_ranks(cgroup, 1, &dest, ugroup, &dst_urank);
      PMPI_Group_translate_ranks (cgroup, 1, &source, ugroup, &src_urank);
      CommInfo commInfo;

      commInfo.insert_dst (dst_urank);
      commInfo.insert_src (src_urank);

      ln->start_slack();
      ln->start_comm(CommInfo::MPI_SENDRECV, commInfo.get_src (), commInfo.get_dst ());
   }

   cres = PMPI_Sendrecv (sendbuf, sendcount, sendtype, dest, sendtag, recvbuf, recvcount, recvtype, source, recvtag, comm, status);

   ln->start_comp();

   return cres;
}

void mpi_sendrecv_ (char * sendbuf, MPI_Fint * sendcount, MPI_Fint * sendtype, MPI_Fint * dest, MPI_Fint * sendtag, char * recvbuf, MPI_Fint * recvcount, MPI_Fint * recvtype, MPI_Fint * source, MPI_Fint * recvtag, MPI_Fint * comm, MPI_Fint * status, MPI_Fint * ierr) {
	void* m_sendbuf = (void*) sendbuf;
	int* m_sendcount = (int*) sendcount;
	MPI_Datatype m_sendtype = MPI_Type_f2c (*sendtype);
	int* m_dest = (int*) dest;
	int* m_sendtag = (int*) sendtag;
	void* m_recvbuf = (void*) recvbuf;
	int* m_recvcount = (int*) recvcount;
	MPI_Datatype m_recvtype = MPI_Type_f2c (*recvtype);
	int* m_source = (int*) source;
	int* m_recvtag = (int*) recvtag;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Status m_status;
   MPI_Status_f2c (status, &m_status);
	(void) (ierr);

	(*ierr) = MPI_Sendrecv(m_sendbuf, *m_sendcount, m_sendtype, *m_dest, *m_sendtag, m_recvbuf, *m_recvcount, m_recvtype, *m_source, *m_recvtag, m_comm, &m_status);
   MPI_Status_c2f (&m_status, status);
}

int  MPI_Comm_spawn_multiple (int  count, char ** array_of_commands, char *** array_of_argv, int * array_of_maxprocs, MPI_Info * array_of_info, int  root, MPI_Comm  comm, MPI_Comm * intercomm, int * array_of_errcodes) {
	(void) (count);
	(void) (array_of_commands);
	(void) (array_of_argv);
	(void) (array_of_maxprocs);
	(void) (array_of_info);
	(void) (root);
	(void) (comm);
	(void) (intercomm);
	(void) (array_of_errcodes);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_spawn_multiple_ (MPI_Fint * count, char * array_of_commands, char * array_of_argv, MPI_Fint * array_of_maxprocs, MPI_Fint * array_of_info, MPI_Fint * root, MPI_Fint * comm, MPI_Fint * intercomm, MPI_Fint * array_of_errcodes, MPI_Fint * ierr, int  cmd_len, int  argv_len) {
	int* m_count = (int*) count;
	char** m_array_of_commands = (char**) array_of_commands;
	char*** m_array_of_argv = (char***) array_of_argv;
	int* m_array_of_maxprocs = (int*) array_of_maxprocs;
	MPI_Info* m_array_of_info = (MPI_Info*) array_of_info;
	int* m_root = (int*) root;
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	MPI_Comm m_intercomm = MPI_Comm_f2c (*intercomm);
	int* m_array_of_errcodes = (int*) array_of_errcodes;
	(void) (ierr);
	(void) (cmd_len);
	(void) (argv_len);

	MPI_Comm_spawn_multiple(*m_count, m_array_of_commands, m_array_of_argv, m_array_of_maxprocs, m_array_of_info, *m_root, m_comm, &m_intercomm, m_array_of_errcodes);
	(*intercomm) = MPI_Comm_c2f (m_intercomm);
}

int  MPI_Unpack (void * inbuf, int  insize, int * position, void * outbuf, int  outcount, MPI_Datatype  datatype, MPI_Comm  comm) {
   return PMPI_Unpack (inbuf, insize, position, outbuf, outcount, datatype, comm);
}

void mpi_unpack_ (char * inbuf, MPI_Fint * insize, MPI_Fint * position, char * outbuf, MPI_Fint * outcount, MPI_Fint * datatype, MPI_Fint * comm, MPI_Fint * ierr) {
	void* m_inbuf = (void*) inbuf;
	int* m_insize = (int*) insize;
	int* m_position = (int*) position;
	void* m_outbuf = (void*) outbuf;
	int* m_outcount = (int*) outcount;
	MPI_Datatype m_datatype = MPI_Type_f2c (*datatype);
	MPI_Comm m_comm = MPI_Comm_f2c (*comm);
	(void) (ierr);

	(*ierr) = MPI_Unpack(m_inbuf, *m_insize, m_position, m_outbuf, *m_outcount, m_datatype, m_comm);
}

int  MPI_Comm_get_parent (MPI_Comm * parent) {
	(void) (parent);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_comm_get_parent_ (MPI_Fint * parent, MPI_Fint * ierr) {
	MPI_Comm m_parent = MPI_Comm_f2c (*parent);
	(void) (ierr);

	(*ierr) = MPI_Comm_get_parent(&m_parent);
	(*parent) = MPI_Comm_c2f (m_parent);
}

int  MPI_Group_union (MPI_Group  group1, MPI_Group  group2, MPI_Group * newgroup) {
	(void) (group1);
	(void) (group2);
	(void) (newgroup);

	fprintf (stderr, "Unsupported MPI call: %s\n", __func__);
	exit (EXIT_FAILURE);
}

void mpi_group_union_ (MPI_Fint * group1, MPI_Fint * group2, MPI_Fint * newgroup, MPI_Fint * ierr) {
	MPI_Group m_group1 = MPI_Group_f2c (*group1);
	MPI_Group m_group2 = MPI_Group_f2c (*group2);
	MPI_Group* m_newgroup = (MPI_Group*) newgroup;
	(void) (ierr);

	(*ierr) = MPI_Group_union(m_group1, m_group2, m_newgroup);
}

}
