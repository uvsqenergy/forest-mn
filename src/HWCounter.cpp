#include "HWCounter.h"

#include <cstring>
#include <glog/logging.h>
#include <perfmon/pfmlib.h>
#include <perfmon/perf_event.h>
#include <perfmon/pfmlib_perf_event.h>
#include <sys/types.h>
#include <unistd.h>
#include <limits>
#include <cstdlib>

HWCounter::HWCounter(const std::string &cname, int core_num) :
   fd_(0), init_val_(0), is_measuring_(false)
{
   int tmp;

   // libpfm init
   if ((tmp = pfm_initialize ()) != PFM_SUCCESS)
   {
      LOG(FATAL) << "Failed to initialize libpfm: " << pfm_strerror(tmp);
   }

   // initialize the counter
   pfm_perf_encode_arg_t arg;
   struct perf_event_attr attr;

   // initialize the structure
   memset(&attr, 0, sizeof(attr));
   memset(&arg, 0, sizeof(arg));
   arg.attr = &attr;
   arg.fstr = NULL;
   arg.size = sizeof(pfm_perf_encode_arg_t);

   // encode the counter
   tmp = pfm_get_os_event_encoding (cname.c_str(), PFM_PLM3,
                                    PFM_OS_PERF_EVENT_EXT, &arg);
   if (tmp != PFM_SUCCESS) {
      LOG(FATAL) << "Failed to get counter " << cname;
   }

   // request scaling in case of shared counters
   arg.attr->read_format = PERF_FORMAT_TOTAL_TIME_ENABLED
      | PERF_FORMAT_TOTAL_TIME_RUNNING;
                          
   // open the file descriptor 
   if (core_num == -1) {
      fd_ = perf_event_open(&attr, getpid(), -1, -1, 0);
   } else {
      fd_ = perf_event_open(&attr, -1, core_num, -1, 0);
   }

   if (fd_ == -1) {
      LOG(FATAL) << "Cannot open counter " << cname;
   }

   // Activate the counter
   if (ioctl (fd_, PERF_EVENT_IOC_ENABLE, 0)) {
      LOG(FATAL) << "Cannot enable event " << cname;
   }
}

HWCounter::HWCounter(const HWCounter &c) :
   fd_(c.fd_), init_val_(c.init_val_), is_measuring_(c.is_measuring_)
{
}

HWCounter::~HWCounter() {
   close(fd_);
   pfm_terminate();
}

HWCounter &HWCounter::operator= (const HWCounter &c) {
   if (fd_ != c.fd_) {
      close(fd_);
      fd_ = c.fd_;
   }

   init_val_ = c.init_val_;
   is_measuring_ = c.is_measuring_;

   return *this;
}

void HWCounter::start() {
   int tmp;
   uint64_t buf [3];

   if (is_measuring_) {
      DLOG(WARNING) << "Starting a running counter";
   }


   tmp = read (fd_, buf, sizeof (buf));
   if (tmp != sizeof (buf)) {
      LOG(FATAL) << "Failed to read counter";
   }

   // handle scaling to allow PMU sharing
   init_val_ = (uint64_t)((double) buf [0] * buf [1] / buf [2]);
   is_measuring_ = true;
}

uint64_t HWCounter::stop() {
   uint64_t buf [3];
   uint64_t value;
   int tmp;


   if (!is_measuring_) {
      DLOG(WARNING) << "Stopping a stopped hardware counter";
      return 0;
   }

   tmp = read (fd_, buf, sizeof (buf));
   if (tmp != sizeof (buf)) {
      LOG(FATAL) << "Failed to read counter";
   }

   // handle scaling to allow PMU sharing
   value = (uint64_t)((double) buf [0] * buf [1] / (double)buf [2]);
   if (init_val_ <= value) {
      value = value - init_val_;
   }
   else  // overflow
   {
      // Check whether we were close to the boundaries of uint64_t
      //DLOG (WARNING) << "Warning: Overflow";
      if (init_val_ >= 0.9 * std::numeric_limits <uint64_t>::max ()) {
         //DLOG (WARNING) << "Classic overflow handling";
         value = 0xFFFFFFFFFFFFFFFFUL - init_val_ + value;
      } else {
         //DLOG (WARNING) << "Taking abs value...";
         //value = ::abs (init_val_ - value);
         value = 0;
      }
   }
   
   is_measuring_ = false;
   return value;
}


