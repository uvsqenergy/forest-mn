#ifndef FORESTMN_STEPCHECKING_H
#define FORESTMN_STEPCHECKING_H

#include <set>
#include "Step.h"
#include "../LocalNode.h"
#include "../TaskProfiling.h"

class StepChecking : public Step {
   public:
      explicit StepChecking (LocalNode &localNode);
      virtual ~StepChecking ();

      virtual void init ();
      virtual void begin_iter ();
      virtual void end_iter ();
      virtual void begin_task ();
      virtual void end_task();
      virtual void start_slack ();
      virtual void start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst);
      virtual bool notify_new_iters () const { return true; }
      virtual bool notify_new_tasks () const { return true; }
   private:

      /** Index of the current task being executed in the sequence */
      unsigned int task_ind_;

      /** Last iteration check was ok */
      bool itcheck_ok_;

      /** The new task profiling for replacing an existing one */
      TaskProfiling new_profiling_;

      /** Are we dealing with an existing task profiling or are we creating a new one? */
      TaskProfiling *cur_profiling_;
};


#endif
