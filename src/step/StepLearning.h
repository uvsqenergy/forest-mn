#ifndef FORESTMN_STEPLEARNING_H
#define FORESTMN_STEPLEARNING_H

#include <set>

#include "Step.h"
#include "../LocalNode.h"
#include "../Task.h"

class StepLearning : public Step {
   public:
      explicit StepLearning (LocalNode &ln);
      ~StepLearning ();
      void init ();
      void begin_iter ();
      void end_iter ();
      void begin_task ();
      void end_task ();
      void start_slack ();
      void start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst);
      bool notify_new_iters () const { return true; }
      bool notify_new_tasks () const { return true; }
   private:
      Task *cur_task_;
};

#endif

