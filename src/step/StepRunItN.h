#ifndef FORESTMN_STEPRUNITN_H
#define FORESTMN_STEPRUNITN_H

#include <vector>
#include <stdint.h>
#include <set>
#include "mpi.h"
#include "Step.h"
#include "../LocalNode.h"
#include "../Task.h"
#include "../TaskProfiling.h"

class StepRunItN : public Step {
   public:
      explicit StepRunItN(LocalNode &ln);
      virtual ~StepRunItN();

      virtual void init ();
      virtual void begin_iter ();
      virtual void end_iter ();
      virtual void begin_task ();
      virtual void end_task();
      virtual bool notify_new_iters () const { return true; }
      virtual bool notify_new_tasks () const { return true; }
      virtual bool notify_comms () const { return false; }
      inline unsigned int get_task_ind () const { return task_ind_; }
      inline void set_task_ind (unsigned int v) { task_ind_ = v; }

   private:
      /**
       * Speeds up the given task by the requested time amount.
       *
       * @param t The index of the related task in the current task sequence.
       * @param time By how many RDTSC cycles must we speed up
       *
       * @return Whether some task has been accelerated or not
       */
      bool speedup (unsigned int t, uint64_t time);

      /**
       * Once the speedup has been applied on some tasks, we have to recompute
       * the frequencies on the groups
       */
      void adjust_groups ();

      /** Task currently being executed. */
      Task *cur_task_;

      /** Number of tasks executed before this one during our phase */
      unsigned int task_ind_;

      /** 
       * Frequency increase and associated speedup applied to each
       * task because of the message received.
       */
      std::vector<std::pair<unsigned int, uint64_t> > fseq_off_;

      /** Our own communicator to exchange control information */
      MPI_Comm comm_;

      /** Our MPI rank on comm_world */
      int rank_;

      // TODO comment
      unsigned int cur_group_id_;

      TaskProfiling *exec_profiling_;

      std::vector <unsigned int> modified_tasks_;
};

#endif
