#include <glog/logging.h>
#include <mpi.h>

#include "StepChecking.h"
#include "../LocalNode.h"
#include "../Wrapper.h"

StepChecking::StepChecking(LocalNode &ln)
      : Step(ln),
      cur_profiling_ (NULL)
{}

StepChecking::~StepChecking() {}

void StepChecking::init() {
   // At this point, HWC measurement should be off
   DCHECK (!node_utils_.running_thread_->get_hwc ().is_measuring ()) << "Error: HWC is running and should be stopped at this point.";

   // At this point, we should also be running the maximum frequency
   DCHECK (node_utils_.running_thread_->get_freq_id () == DVFSUnitUtils::freq_max_id) << "Error: The running freq should be " << DVFSUnitUtils::freq_max_id << ", running thread is currently running " << node_utils_.running_thread_->get_freq_id ();
}

void StepChecking::begin_iter () {   
   task_ind_ = 0;
   itcheck_ok_ = true;

   // inform the other process that we want to start the checking
   // (and wait for them too before switching the frequency)
   int ready = 0, all_ready; // ready = 0 here means we are not yet ready
   PMPI_Allreduce(&ready, &all_ready, 1, MPI_INTEGER, MPI_BAND, MPI_COMM_WORLD);
}

void StepChecking::end_iter () {
   TaskSequence &tseq = localNode_.task_seq_;

   // If the size of this iteration does not match the previous one
   if (task_ind_ < tseq.size ()) {
      tseq.truncate (task_ind_);
   }

   // Iteration stable, ready to roll out!
   if (itcheck_ok_) {
      // all tasks at the highest freq have a eratio of 1
      for (unsigned int i = 0; i < tseq.size(); i++) {
         TaskProfiling& prof = tseq [i]->get_profiling (DVFSUnitUtils::freq_max_id);
         prof.set_energy_gain (1.);
      }

      localNode_.set_step(STP_WAIT_EVAL);
   }
}

void StepChecking::begin_task() {
   TaskSequence& tseq = localNode_.task_seq_;
   uint64_t t = get_time ();

   if (task_ind_ >= tseq.size ()) {
      LOG(WARNING) << "New task found";
      tseq.append_new_task (t);
      cur_profiling_ = &(tseq.get_last_task ()->get_profiling (DVFSUnitUtils::freq_max_id));
      itcheck_ok_ = false; // There's a new task here, we have to double check this
   } else {
      cur_profiling_ = &new_profiling_;
      cur_profiling_->reset (t);
   }
   node_utils_.running_thread_->get_hwc ().start();
}

void StepChecking::end_task () {
   TaskSequence& tseq = localNode_.task_seq_;
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   
   DCHECK (hwc.is_measuring ()) << "Error: I wasn't measuring...";
   // If we didn't call start_slack and start_comm
   if (cur_profiling_->has_slack_date_yet ()) {
      cur_profiling_->set_comm_instrs (hwc.stop ());
   } else {
      // In this case, we set the comp instrs and 0 for comm
      cur_profiling_->set_comp_instrs (hwc.stop ());
      cur_profiling_->set_comm_instrs (0);
   }
   cur_profiling_->set_end_task (get_time ());
   
   // If we're dealing with an existing task, is it similar to the one previously added to the sequence?
   TaskProfiling& old_prof = tseq [task_ind_]->get_profiling (DVFSUnitUtils::freq_max_id);
   if (task_ind_ < tseq.size ()) {
      if (!new_profiling_.is_similar (old_prof)) {
         LOG(WARNING) << "Unstable task " << task_ind_ << ", task #" << task_ind_;
         itcheck_ok_ = false;
      }
      // Replace the wrong task profiling with the new profiling
      tseq.replace (task_ind_, new_profiling_, DVFSUnitUtils::freq_max_id);
   }
   ++task_ind_;
}

void StepChecking::start_slack () {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   DCHECK (hwc.is_measuring ()) << "I wasn't measuring...";
   cur_profiling_->set_comp_instrs (hwc.stop ());
   cur_profiling_->set_start_slack (get_time ());
}

void StepChecking::start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst) {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   cur_profiling_->set_start_comm (get_time ());
   DCHECK (!hwc.is_measuring ()) << "Error: I was measuring...";
   hwc.start ();
   int old_comm_name = localNode_.task_seq_ [task_ind_]->get_comm_name ();

   if (comm_name != old_comm_name) {
      itcheck_ok_ = false;
      CommInfo::Name oname = (CommInfo::Name)old_comm_name, nname = (CommInfo::Name)comm_name;

      LOG (WARNING) << "Unstable task " << task_ind_ << ": Old MPIName: " << CommInfo::toString (oname)
         << ", New: " << CommInfo::toString (nname);
   }
   (void) src;
   (void) dst;
}

