#ifndef FORESTMN_STEPWAITRUN_H
#define FORESTMN_STEPWAITRUN_H

#include <set>
#include "Step.h"
#include "../LocalNode.h"

class StepWaitRun: public Step {
   public:
      explicit StepWaitRun(LocalNode &ln);
      ~StepWaitRun() {}

      void init ();
      void begin_iter ();
      void end_iter ();
      bool notify_new_iters () const { return true; }
      bool notify_new_tasks () const { return true; }
};

#endif
