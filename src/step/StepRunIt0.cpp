#include <set>
#include <vector>
#include "glog/logging.h"

#include "StepRunIt0.h"
#include "../Common.h"
#include "../Task.h"
#include "../TaskSequence.h"
#include "../LocalNode.h"
#include "../NodeUtils.h"
#include "../DVFSUnitUtils.h"

#define EXEC_SEQ (DVFSUnitUtils::freq_max_id + 1)
 
StepRunIt0::StepRunIt0(LocalNode &ln) : Step(ln), exec_profiling_ (NULL) {
}

StepRunIt0::~StepRunIt0() {
}

void StepRunIt0::init () {
   DLOG(INFO) << "Starting RUN IT0 phase";
   TaskSequence& tseq = localNode_.task_seq_;
   const unsigned int nb_groups = tseq.get_nb_groups ();

   std::vector<unsigned int> &max_freqs = localNode_.get_max_freqs();
   max_freqs.clear();

   std::vector<unsigned int> &freq_seq = localNode_.get_freq_seq();
   freq_seq.clear();

   unsigned int min_eval_freq = localNode_.task_seq_ [0]->get_min_eval_freq ();

   // Determine the initial frequency to set for each group
   for (unsigned int tgid = 0; tgid < nb_groups; tgid++) {
      TaskGroup& tg = tseq.get_task_group (tgid);
      const unsigned int nb_tasks = tg.get_nb_tasks ();
      unsigned int group_ideal_freq = 0;
      // False if all the tasks in the group are too small to choose a frequency
      bool group_chose_freq = false;

      // For each task in the group
      for (unsigned int tid = 0; tid < nb_tasks; tid++) {
         Task *task = tg.get_task (tid);
         TaskProfiling& max_prof = task->get_profiling (DVFSUnitUtils::freq_max_id);
         float task_best_egain = 1.0f;
         unsigned int task_ideal_freq = DVFSUnitUtils::freq_max_id;
         float last_ips; // Is useful to know whether IPS is reliable or not

         uint64_t min_compt = max_prof.get_comp_time () + max_prof.get_slack_time ();

         // The fraction of the task dependent of the frequency (with fmax profiling values)
         uint64_t fdep_length_max = max_prof.get_fdep_time ();
         unsigned int max_freq = DVFSUnitUtils::freq_max_id;

         // Skip the evaluation of the task if task is not significant enough
         if (fdep_length_max < 40 * FREQ_TRANS_LAT) {
            continue;
         }

         last_ips = task->get_profiling (DVFSUnitUtils::freq_max_id - 1).get_ips (); 
         // For each profiling of the task
         for (int f = DVFSUnitUtils::freq_max_id - 1; f >= (int)min_eval_freq; --f) {
            const TaskProfiling& prof = task->get_profiling (f);
            float cur_ips = prof.get_ips ();
            float fdep_length = prof.get_fdep_time (); 

            // If IPS is not reliable (eg lower freq IPS is too much higher than
            // higher freq IPS, which shouldn't occur)
            // In this case, we play safe and choose freq max
            if (cur_ips > (last_ips + 0.7) || cur_ips < (last_ips - 0.7)) {
               max_freq = task_ideal_freq = DVFSUnitUtils::freq_max_id;
               break;
            }

            /**
             * Small tasks don't participate in the best frequency selection
             * because they might have a very high energy gain while not
             * saving energy at all
             */
            if (fdep_length < 40 * FREQ_TRANS_LAT) {
               continue;
            }
            
            group_chose_freq = true;
            
            /**
             * This is the performance constraint:
             * We want to skip frequencies that would give too much slowdown
             */
            const float slowdown = max_prof.get_ips () / prof.get_ips ();
            if (slowdown > MAX_SLOWDOWN) {
               break;
            }


            // Now find the best energy among profiled frequencies
            const float energy_gain = prof.get_energy_gain ();
            if (energy_gain > task_best_egain) {
               task_best_egain = energy_gain;
               task_ideal_freq = f;
            }

            // Save the maximal frequency that can be used
            // (eg that does not extend the task execution time)
            if (prof.get_comp_time () <= min_compt) {
               max_freq = f;
            }

            // Keep track of maximum IPS evaluated yet
            last_ips = cur_ips;
         }

         // Save the max freq value for future needs (see RunItN)
         task->set_max_freq (max_freq);

         // Save the ideal freq for future needs (see RunItN)
         task->set_virtual_freq_id (std::min (task_ideal_freq, max_freq));

         // Select the task having the highest frequency
         // (best bet we can do with data we have)
         if (task_ideal_freq > group_ideal_freq) {
            group_ideal_freq = task_ideal_freq;
         }
      }

      if (!group_chose_freq) {
         DLOG (INFO) << "Group didn't choose freq, so I'm gonna choose it";
         // If no frequency is chosen by the group (tasks too small)
         // Then we consider the IPS at the group level
         float ips_max = tg.get_ips (DVFSUnitUtils::freq_max_id);
         float best_group_egain = 1.;
         group_ideal_freq = DVFSUnitUtils::freq_max_id;
         for (int f = DVFSUnitUtils::freq_max_id - 1; f >= (int)min_eval_freq; f--) {
            float ips = tg.get_ips (f);
            float slowdown = ips_max / ips;

            DLOG (INFO) << "IPS max = " << ips_max << ", ips = " << ips;

            if (slowdown > MAX_SLOWDOWN) {
               break;
            }
            
            // Pfmax / Pf
            float power_fmax = node_utils_.running_dvfs_unit_->get_power (DVFSUnitUtils::freq_max_id);
            float power_curfreq = node_utils_.running_dvfs_unit_->get_power (f);
            float pwrg = power_fmax / power_curfreq;

            float group_egain = (1. / slowdown) * pwrg;
            DLOG (INFO) << "Group Egain for freq #" << f << " is: " << group_egain;

            if (group_egain > best_group_egain) {
               best_group_egain = group_egain;
               group_ideal_freq = f;
            }
         }
         DLOG (INFO) << "I Finally chose freq #" << group_ideal_freq;
      }

      //tg.set_freq_id (15);
      // Now we have the ideal frequency for the group :)
      tg.set_freq_id (group_ideal_freq);
   }

   tseq.print ();
   
   DCHECK (!node_utils_.running_thread_->get_hwc ().is_measuring ()) << "Error: hwc was running";
}

void StepRunIt0::begin_iter () {
   task_ind_ = 0;
   cur_group_id_ = 0;
}

void StepRunIt0::end_iter () {
   localNode_.set_step(STP_RUN_ITN);
}

void StepRunIt0::begin_task () {
   TaskSequence& tseq = localNode_.task_seq_;
   unsigned int group_id = tseq.get_group_id (task_ind_);
   DCHECK (group_id >= cur_group_id_) << "Error: Invalid group id (" << group_id << " >= " << cur_group_id_ << ")";
   
   const TaskGroup& tg = tseq.get_task_group (group_id);
   exec_profiling_ = &(tseq [task_ind_]->get_profiling (EXEC_SEQ));

   // If we're starting a new group, change the frequency to the group frequency
   if (group_id != cur_group_id_) {
      node_utils_.running_thread_->set_freq (tg.get_freq_id ());
   }
   cur_group_id_ = group_id;

   // Start execution profiling
   node_utils_.running_thread_->get_hwc ().start();
   exec_profiling_->set_start_date (get_time ());
}

void StepRunIt0::end_task() {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   exec_profiling_->set_end_task (get_time());

   DCHECK (hwc.is_measuring ()) << "Error: I wasn't measuring...";
   exec_profiling_->set_comm_instrs (hwc.stop ());

   ++task_ind_;
}

void StepRunIt0::start_slack () {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   exec_profiling_->set_comp_instrs (hwc.stop ());
   exec_profiling_->set_start_slack (get_time ());
}

void StepRunIt0::start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst) {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   exec_profiling_->set_start_comm (get_time ());
   DCHECK (!hwc.is_measuring ()) << "Error: I was measuring...";
   DCHECK (comm_name == localNode_.task_seq_ [task_ind_]->get_comm_name ()) << "Error: invalid MPI name";
   hwc.start ();
   (void) src;
   (void) dst;
}

