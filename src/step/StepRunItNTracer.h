#ifndef FORESTMN_STEPTRACING_H
#define FORESTMN_STEPTRACING_H

#include <fstream>
#include <set>
#include "Step.h"
#include "StepRunItN.h"
#include "../LocalNode.h"
#include "../Task.h"

class StepRunItNTracer : public Step {
   public:
      explicit StepRunItNTracer (LocalNode &ln);
      virtual ~StepRunItNTracer ();

      virtual void init ();
      virtual void begin_iter ();
      virtual void end_iter ();
      virtual void begin_task ();
      virtual void end_task();
      virtual void start_slack ();
      virtual void start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst);
      virtual bool notify_new_iters () const { return true; }
      virtual bool notify_new_tasks () const { return true; }

   private:
      /** The original step we want to decorate with tracing facilities */
      StepRunItN step_;

      /** The file descriptor we will use to trace the step */
      std::ofstream traceFd_;

      /** Number of iterations having been executed in this step
       * since the beginning of the application */
      unsigned int iter_ind_;

      Task *cur_task_;

      TaskProfiling tp_;

      unsigned int task_ind_;
};

#endif
