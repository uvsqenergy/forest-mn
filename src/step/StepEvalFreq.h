#ifndef FORESTMN_STEPEVALFREQ_H
#define FORESTMN_STEPEVALFREQ_H

#include "Step.h"
#include "../Task.h"

// TODO disabling copy like here everywhere in Step*
class StepEvalFreq : public Step {
   public:
      explicit StepEvalFreq (LocalNode &localNode);
      virtual ~StepEvalFreq ();

      virtual void init ();
      virtual void begin_iter ();
      virtual void end_iter ();
      virtual void begin_task ();
      virtual void end_task();
      virtual void start_slack ();
      virtual void start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst);
      virtual bool notify_new_iters () const{ return true; }
      virtual bool notify_new_tasks () const{ return true; }
   private:
      /** Index of the current task being executed in the sequence */
      unsigned int task_ind_;

      /** Task currently running */
      Task *cur_task_;

      // TODO comment
      unsigned int cur_freq_id_;

      // Disable copy of a LocalNode into a StepEvalFreq
      StepEvalFreq operator= (const StepEvalFreq &step);

      // Disable StepEvalFreq instances copy
      StepEvalFreq (const StepEvalFreq& step);
};


#endif
