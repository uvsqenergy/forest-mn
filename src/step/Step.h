#ifndef FORESTMN_STEP_H
#define FORESTMN_STEP_H

#include <set>

#include "../LocalNode.h"
#include "../NodeUtils.h"

// Forward declaration because of circular dependence

// TODO comment
class Step {
   public:
      explicit Step (LocalNode &localNode) : localNode_(localNode), node_utils_ (localNode_.get_node_utils ()) {}
      virtual ~Step () {}

      // TODO comment
      virtual void init () = 0;
      virtual void begin_iter () {}
      virtual void end_iter () {}
      
      virtual void begin_task () {}
      virtual void end_task() {}
      
      virtual void start_slack () {}
      virtual void start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst) {
         (void) comm_name;
         (void) src;
         (void) dst;
      }
      virtual bool notify_new_iters () const = 0;
      virtual bool notify_new_tasks () const = 0;

      virtual bool notify_comms () const {
         return notify_new_tasks();
      }

   protected:
      /** LocalNode we are attached to. */
      LocalNode& localNode_;

      /** CPU related info */
      NodeUtils& node_utils_;

      // Disable default constructor
      Step ();
};

#endif
