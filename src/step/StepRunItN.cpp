#include <cstdlib>
#include <set>
#include <utility>
#include <vector>
#include "glog/logging.h"
#include "mpi.h"

#include "StepRunItN.h"
#include "../Task.h"
#include "../TaskSequence.h"
#include "../LocalNode.h"
#include "../Msg.h"
#include "../NodeUtils.h"
#include "../DVFSUnitUtils.h"

// negligible slack (in RDTSC cycles)
#define NEGLIGIBLE_SLACK ((int) 1e6)

#define EXEC_SEQ (DVFSUnitUtils::freq_max_id + 1)

StepRunItN::StepRunItN(LocalNode &ln) : Step(ln),
   cur_task_ (NULL),
   task_ind_ (0), rank_ (0) {
   PMPI_Comm_dup(MPI_COMM_WORLD, &comm_);
   PMPI_Comm_rank(MPI_COMM_WORLD, &rank_);
}

StepRunItN::~StepRunItN() {
   PMPI_Comm_free(&comm_);
}

void StepRunItN::init () {
   DLOG(INFO) << "Starting RUN ITN phase";
   TaskSequence& tseq = localNode_.task_seq_;

   unsigned int nb_tasks = tseq.size();

   // frequency decrease to apply to every task
   // each couple is (frequency offset / time difference compared to initial freq_seq)
   fseq_off_.clear();
   fseq_off_.resize(nb_tasks, std::pair<unsigned int, uint64_t>(0, 0));

   // ask all nodes provoking slack on our tasks to speedup
   for (unsigned int t = 0; t < nb_tasks; ++t) {
      const TaskProfiling& prof = tseq [t]->get_profiling (EXEC_SEQ);
      uint64_t slack = prof.get_slack_time();

      if (slack <= NEGLIGIBLE_SLACK) {
         continue;
      }

      DLOG (INFO) << "Task #" << t << " has slack = " << slack << ", all senders must speedup";

      // all senders must speedup
      const std::set<int> &t_senders = tseq[t]->get_comm_emitters();
      for (std::set<int>::iterator it = t_senders.begin(); it != t_senders.end(); it++) {
         if (*it == rank_) {
            continue;
         }

         unsigned int tid = tseq.get_nb_tasks_receiving_from(*it, t);

         Msg<uint64_t> msg (tid, rank_, *it, slack);
         DLOG (INFO) << "Task #" << t << " sending a message to " << *it;
         msg.send(comm_);
      }
   }

   task_ind_ = 0;
}

void StepRunItN::begin_iter () {
   int got_mail = 0;
   TaskSequence& tseq = localNode_.task_seq_;

   cur_group_id_ = 0;
   task_ind_ = 0;

   // Did any of the other nodes send us a message requesting for slowdown/speedup?
   PMPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm_, &got_mail, MPI_STATUS_IGNORE);

   while (got_mail) {
      // The task id is identified by the number of communications in the sequence
      // to the node sending this message
      int tid = 0;

      // Receive the message
      Msg<uint64_t> msg = Msg<uint64_t>::recv(comm_);

      // Retrieve the task id related to the message
      tid = tseq.get_ith_task_sending_to (msg.get_src (), msg.get_tid ());
      //LOG (ERROR) << "Node #" << rank_ << " received message from " << msg.get_src () << ", has to speedup " << tid << " by " << msg.get_data ();


      // Impossible to retrieve the corresponding task id
      if (tid == -1) {
         DLOG (FATAL) << "Impossible to retrieve the task #" << msg.get_tid ()
                      << "from " << msg.get_dst () << "corresponding to the message";
      }

      // Apply the requested speedup and adjust groups if any change
      if (speedup (tid, msg.get_data ())) {
         adjust_groups ();
      }

      // Do we still have a message pending ?
      PMPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, comm_, &got_mail, MPI_STATUS_IGNORE);
   }
   const TaskGroup& tg = tseq.get_task_group (0);
   DLOG (INFO) << "Changing freq from #" << node_utils_.running_thread_->get_cur_freq_id () << " to #" << tg.get_freq_id ();
   node_utils_.running_thread_->set_freq (tg.get_freq_id ());
}

void StepRunItN::end_iter () {
}

void StepRunItN::begin_task () {
   TaskSequence& tseq = localNode_.task_seq_;
   unsigned int group_id = tseq.get_group_id (task_ind_);
   DCHECK (group_id >= cur_group_id_) << "Error: Invalid group id (" << group_id << " >= " << cur_group_id_ << ")";
   
   if (task_ind_ >= tseq.size()) {
      LOG(WARNING) << "Additional task found while running";
      return;
   }
   
   const TaskGroup& tg = tseq.get_task_group (group_id);
   exec_profiling_ = &(tseq [task_ind_]->get_profiling (EXEC_SEQ));

   // Starting a new group, adjust the frequency
   if (group_id != cur_group_id_) {
      DLOG (INFO) << "Changing freq from #" << node_utils_.running_thread_->get_cur_freq_id () << " to #" << tg.get_freq_id ();
      node_utils_.running_thread_->set_freq (tg.get_freq_id ());
   }
   cur_group_id_ = group_id;
}

void StepRunItN::end_task() {
   // Set the current task to the following one
   task_ind_++;
}

bool StepRunItN::speedup (unsigned int t, uint64_t time) {
   uint64_t loc_comp = 0;   
   bool done = false;
   bool modified = false;
   TaskSequence& tseq = localNode_.task_seq_;

   DCHECK(t < tseq.size()) << "Invalid task " << t;

   // WARNING: we only apply additional speedups, i.e. do not accelerate if we 
   // already applied a greater speedup
   if (time <= fseq_off_[t].second) {
      return false;
   }

   // we already applied part of this speedup
   time -= fseq_off_[t].second;

   // is the slowdown significant enough for us to care about it?
   if (time <= NEGLIGIBLE_SLACK) {
      return false;
   }

   int firstReceivingTask = tseq.get_first_receiving_task_before (t);

   //DLOG (INFO) << "#" << rank_ << " wants to speedup task #" << t;
   
   // For each task between t and the first receiving task before us
   // (as it may add slack if receiving tasks are accelerated)
   for (int taskId = t; taskId > firstReceivingTask && !done; taskId--) {
      // speedup achieved for the current task
      int64_t tloc_comp = 0;

      // Frequency we'd like to apply for task taskId
      unsigned int freqOffset = fseq_off_ [taskId].first;
      unsigned int curFreqIdx = tseq [taskId]->get_virtual_freq_id () + freqOffset;

      // task sequence for the current freq 
      const TaskProfiling& cur_prof = tseq [taskId]->get_profiling (curFreqIdx);

      // search for the lowest frequency that exceeds the requested speedup
      for (unsigned int f = curFreqIdx + 1; f <= tseq [taskId]->get_max_freq (); ++f) {
         const TaskProfiling& f_prof = tseq [taskId]->get_profiling (curFreqIdx);

         // adjust the frequency offset to apply tloc_comp speedup
         //DLOG (ERROR) << "#" << rank_ << " speed up task #" << taskId << " from " << (curFreqIdx + fseq_off_ [taskId].first) << " to " << (curFreqIdx + 1 + fseq_off_ [taskId].first);
         fseq_off_[taskId].first++;
         modified_tasks_.push_back (taskId);
         modified = true;

         DCHECK(f == fseq_off_[taskId].first + tseq [taskId]->get_virtual_freq_id ());

         // resulting speedup
         tloc_comp = (int64_t)  cur_prof.get_comp_time() - (int64_t) f_prof.get_comp_time();
         tloc_comp = (tloc_comp < 0 ? 0 : tloc_comp);

         // do we achieve the requested speedup?
         if (f_prof.get_comp_time() + time <= cur_prof.get_comp_time() + loc_comp) {
            done = true;
            break;
         }
      }

      // remember we applied this speedup to make sure we do not cumulate them
      // the speedup applied to task taskId has consequences on the tasks following it
      for (unsigned int i = taskId; i <= t; ++i) {
         fseq_off_[i].second += tloc_comp;
      }

      loc_comp += tloc_comp;
   }

   if (done) {
      return modified;
   }

   // non significant remaining slowdown
   if (time <= loc_comp + NEGLIGIBLE_SLACK) {
      DLOG(INFO) << "Ignoring the remaining " << (int64_t) time - (int64_t) loc_comp;
      return modified;
   }

   // ask the other nodes to help us applying the remaining speedup

   // it is over, no one can help
   if (firstReceivingTask < 0) {
      return modified;
   }

   // for each task that sent us something
   const std::set<int> &rsrc = tseq [firstReceivingTask]->get_comm_emitters();
   std::set<int>::const_iterator it;
   for (it = rsrc.begin(); it != rsrc.end(); it++) {
      unsigned int tid = 0;

      // do not ask ourself to speedup...
      if (*it == rank_) {
         continue;
      }

      tid = tseq.get_nb_tasks_receiving_from (*it, firstReceivingTask);

      // We want to send a message to other people so that they can compensate
      // the slack we can't compensate ourselves
      Msg<uint64_t> msg (tid, rank_, *it, time - loc_comp);
      msg.send(comm_);
   }

   return modified;
}

void StepRunItN::adjust_groups () {
   TaskSequence& tseq = localNode_.task_seq_;

   // For each modified task in the previous speedup call, we have to adjust
   // its corresponding group frequency in case it changes it
   for (unsigned int mid = 0; mid < modified_tasks_.size (); mid++) {
      unsigned int tid = modified_tasks_ [mid];
      const unsigned int group_id = tseq.get_group_id (tid);
      TaskGroup& tg = tseq.get_task_group (group_id);
      const unsigned int task_freq = fseq_off_ [tid].first + tseq [tid]->get_virtual_freq_id ();

      // If the new frequency is higher than the overall group's frequency, change it
      if (task_freq > tg.get_freq_id ()) {
         unsigned int old_tg_fid = tg.get_freq_id ();
         tg.set_freq_id (task_freq);
         DLOG (ERROR) << "#" << sched_getcpu () << ": Group #" << group_id << " from f#" << old_tg_fid << " to f#" << task_freq << " because of task #" << tid;
      }
   }

   // Clear the vector for future speedup calls
   modified_tasks_.clear ();
}
