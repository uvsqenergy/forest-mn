#include <set>
#include <cstddef>
#include "glog/logging.h"

#include "Step.h"
#include "StepLearning.h"
#include "../LocalNode.h"
#include "../Common.h"
#include "../Task.h"
#include "../TaskSequence.h"
#include "../HWCounter.h"
#include "../TaskProfiling.h"

StepLearning::StepLearning (LocalNode& ln) : Step (ln), cur_task_ (NULL) {}

StepLearning::~StepLearning () {}

void StepLearning::init () {
   // Reset all elements in the task sequence
   localNode_.task_seq_.truncate (0);

   // Set maximum frequency
   node_utils_.running_thread_->set_freq (DVFSUnitUtils::freq_max_id);
}

void StepLearning::begin_iter () {}

void StepLearning::end_iter () {
   // Go in checking step at the end of the iteration
   localNode_.set_step (STP_CHECKING);
}

void StepLearning::begin_task () {
   // For each new task, start counting events
   cur_task_ = localNode_.task_seq_.append_new_task (get_time ());
   node_utils_.running_thread_->get_hwc ().start ();
}

void StepLearning::end_task () {
   TaskProfiling& cur_profiling = cur_task_->get_profiling (DVFSUnitUtils::freq_max_id);
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   
   cur_profiling.set_end_task (get_time ());

   DCHECK (hwc.is_measuring ()) << "Error: I wasn't measuring...";
   cur_profiling.set_comm_instrs (hwc.stop ());
}

void StepLearning::start_slack () {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   TaskProfiling& cur_profiling = cur_task_->get_profiling (DVFSUnitUtils::freq_max_id);
   
   cur_profiling.set_comp_instrs (hwc.stop ());
   cur_profiling.set_start_slack (get_time ());
}

void StepLearning::start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst) {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   TaskProfiling& cur_profiling = cur_task_->get_profiling (DVFSUnitUtils::freq_max_id);

   cur_task_->set_comm_name (comm_name);
   cur_task_->set_comm (src, dst);
   cur_profiling.set_start_comm (get_time ());

   DCHECK (!hwc.is_measuring ()) << "Error: I don't want hwc to be measuring right now";
   hwc.start ();
}
