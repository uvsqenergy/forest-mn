#include <fstream>
#include <sstream>
#include <iomanip>
#include "glog/logging.h"

#include "StepRunItNTracer.h"
#include "../LocalNode.h"
#include "../NodeUtils.h"
#include "../DVFSUnitUtils.h"
#include "StepRunItN.h"

#define EXEC_SEQ (DVFSUnitUtils::freq_max_id + 1)

StepRunItNTracer::StepRunItNTracer (LocalNode& ln) :
   Step (ln), step_ (ln), iter_ind_ (0), cur_task_ (NULL) {
   std::ostringstream filename;

   // Open the file
   filename << "tracing/trace_" << node_utils_.running_thread_->get_global_mpi_rank () << ".txt";
   traceFd_.open (filename.str ().c_str (), std::ofstream::out | std::ofstream::trunc);
   DCHECK (traceFd_.is_open ()) << "Error while opening tasks tracing file";
}

StepRunItNTracer::~StepRunItNTracer () {
   traceFd_.close ();
}

void StepRunItNTracer::init () {
   step_.init ();
}

void StepRunItNTracer::begin_iter () {
   step_.begin_iter ();
   task_ind_ = 0;
}

void StepRunItNTracer::end_iter () {
   TaskSequence& tseq = localNode_.task_seq_;
   step_.end_iter ();

   for (unsigned int i = 0; i < tseq.get_nb_groups (); i++) {
      TaskGroup& tg = tseq.get_task_group (i);
      traceFd_ << std::setw (2) << tg.get_freq_id () << " ";
   }
   traceFd_ << std::endl;

   // Increment the index of the current iteration
   iter_ind_++;
}

void StepRunItNTracer::begin_task () {
   step_.begin_task ();
   tp_.reset (get_time ());
   node_utils_.running_thread_->get_hwc ().start ();
}

void StepRunItNTracer::end_task () {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   step_.end_task ();
   tp_.set_end_task (get_time ());

   DCHECK (hwc.is_measuring ()) << "Error: I wasn't measuring...";
   tp_.set_comm_instrs (hwc.stop ());

   DLOG (INFO) << "task #" << task_ind_ << ": IPS = " << tp_.get_ips () << "@f#" << localNode_.task_seq_.get_task_group (localNode_.task_seq_.get_group_id (task_ind_)).get_freq_id ();
   task_ind_++;
}

void StepRunItNTracer::start_slack () {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   step_.start_slack ();
   tp_.set_comp_instrs (hwc.stop ());
   tp_.set_start_slack (get_time ());
}

void StepRunItNTracer::start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst) {
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   tp_.set_start_comm (get_time ());
   DCHECK (!hwc.is_measuring ()) << "Error: I was measuring...";
   hwc.start ();
   (void) src;
   (void) dst;
}

