#include <glog/logging.h>
#include <mpi.h>

#include "StepEvalFreq.h"
#include "../LocalNode.h"
#include "../Common.h"
#include "../Task.h"
#include "../TaskSequence.h"

StepEvalFreq::StepEvalFreq(LocalNode &ln) : Step(ln), 
   cur_task_(NULL), cur_freq_id_ (DVFSUnitUtils::freq_max_id)
{
}

StepEvalFreq::~StepEvalFreq() {
}

void StepEvalFreq::init() {
   DLOG(INFO) << "Starting EVAL phase";
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   
   cur_freq_id_ = DVFSUnitUtils::freq_max_id - 1;

   // Reset HWC
   if (hwc.is_measuring()) {
      hwc.stop();
   }
}

void StepEvalFreq::begin_iter () {
   task_ind_ = 0;

   // Setting frequency to fmax - 1
   node_utils_.running_thread_->set_freq (cur_freq_id_);

   DLOG(INFO) << "Evaluating frequency #" << cur_freq_id_;
}

void StepEvalFreq::end_iter () {
   TaskSequence& tseq = localNode_.task_seq_;
   if (task_ind_ != tseq.size ()) {
      LOG (WARNING) << "Unstable behavior detected while evaluating frequencies" << std::endl;
      localNode_.set_step (STP_LEARNING);
   } else {
      int eval_next = 0;
      for (unsigned int t = 0; t < tseq.size(); ++t) {
         TaskProfiling& cur_profile = tseq [t]->get_profiling (cur_freq_id_);
         TaskProfiling& max_profile = tseq [t]->get_profiling (DVFSUnitUtils::freq_max_id);
         uint64_t cur_fdep_length = cur_profile.get_fdep_time ();

         // ignore short tasks
         if (cur_fdep_length < 40 * FREQ_TRANS_LAT) {
            continue;
         }
         
         // tfmax / tf
         float spdup = cur_profile.get_ips () / max_profile.get_ips ();

         // Pfmax / Pf
         float power_fmax = node_utils_.running_dvfs_unit_->get_power (DVFSUnitUtils::freq_max_id);
         float power_curfreq = node_utils_.running_dvfs_unit_->get_power (cur_freq_id_);
         float pwrg = power_fmax / power_curfreq;

         cur_profile.set_energy_gain (spdup * pwrg);

         // stop if we do not save energy anymore or if we slow down too much...
         // We evaluate one frequency less IF and ONLY if
         // At least ONE iteration has an energy increase AND a speedup that
         // respects the slowdown constraint...
         eval_next |= spdup * pwrg > 1 && 1.0f / spdup <= MAX_SLOWDOWN;
         
         // ... except if we did not compensate the slack at fmax so far
         //A AND if the original slack
         eval_next |= max_profile.get_slack_time () > 
            (cur_profile.get_comp_time() - max_profile.get_comp_time());
      }

      // no more frequency to evaluate anyway
      if (cur_freq_id_ == 0) {
         eval_next = 0;
         LOG (WARNING) << "Change because cannot go to a lower frequency";
      }

      int oth_eval_next;
      PMPI_Allreduce(&eval_next, &oth_eval_next, 1, MPI_INTEGER, MPI_BOR, MPI_COMM_WORLD);
      
      if (eval_next) {
         cur_freq_id_--;
      } else if (oth_eval_next) {
         localNode_.set_step (STP_WAIT_RUN);
      } else {
         localNode_.set_step (STP_RUN_IT0);
      }
   }
}

void StepEvalFreq::begin_task() {
   cur_task_ = localNode_.task_seq_ [task_ind_];
   TaskProfiling& cur_profiling = cur_task_->get_profiling (cur_freq_id_);
   
   // Set the start date for the considered profiling
   cur_profiling.set_start_date (get_time ());

   // Start counting events
   node_utils_.running_thread_->get_hwc ().start ();
}

void StepEvalFreq::end_task () {
   TaskProfiling& cur_profiling = cur_task_->get_profiling (cur_freq_id_);
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   cur_profiling.set_end_task (get_time ());

   DCHECK (hwc.is_measuring ()) << "Error: I wasn't measuring";
   cur_profiling.set_comm_instrs(hwc.stop());

   task_ind_++;
}

void StepEvalFreq::start_slack () {
   TaskProfiling& cur_profiling = cur_task_->get_profiling (cur_freq_id_);
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   cur_profiling.set_comp_instrs (hwc.stop ());
   cur_profiling.set_start_slack (get_time ());
}

void StepEvalFreq::start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst) {
   TaskProfiling& cur_profiling = cur_task_->get_profiling (cur_freq_id_);
   HWCounter& hwc = node_utils_.running_thread_->get_hwc ();
   cur_profiling.set_start_comm (get_time ());
   
   DCHECK (!hwc.is_measuring ()) << "Error: I was measuring";
   DCHECK (comm_name == localNode_.task_seq_ [task_ind_]->get_comm_name ()) << "Error: Invalid Comm name";
   hwc.start ();
   (void) src;
   (void) dst;
}
