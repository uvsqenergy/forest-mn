#include <set>
#include "mpi.h"
#include "glog/logging.h"

#include "Step.h"
#include "StepWaitEval.h"
#include "../LocalNode.h"

StepWaitEval::StepWaitEval (LocalNode& ln) : Step(ln) {}

void StepWaitEval::init () {
   DLOG(INFO) << "Starting WAIT EVAL step";

   // Now that we have stable tasks, we can generate the appropriate groups
   localNode_.task_seq_.generate_groups ();
}

void StepWaitEval::begin_iter () {
   int ready = 1, all_ready;
   PMPI_Allreduce(&ready, &all_ready, 1, MPI_INTEGER, MPI_BAND, MPI_COMM_WORLD);
   if (all_ready) {
      localNode_.set_step (STP_EVAL_FREQ);
   }
}

void StepWaitEval::end_iter () {
}


