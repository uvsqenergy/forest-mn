#ifndef FORESTMN_STEPRUNIT0_H
#define FORESTMN_STEPRUNIT0_H

#include <set>
#include "Step.h"
#include "../LocalNode.h"
#include "../Task.h"

class StepRunIt0 : public Step {
   public:
      explicit StepRunIt0(LocalNode &ln);
      virtual ~StepRunIt0();

      virtual void init ();
      virtual void begin_iter ();
      virtual void end_iter ();
      virtual void begin_task ();
      virtual void end_task();
      virtual void start_slack ();
      virtual void start_comm (int comm_name, const std::set<int>& src, const std::set<int>& dst);
      virtual bool notify_new_iters () const { return true; }
      virtual bool notify_new_tasks () const { return true; }

   private:
      /** Task currently being executed. */
      Task *cur_task_;

      /** Number of tasks executed before this one during our phase */
      unsigned int task_ind_;

      /** The id of the current TaskGroup running */
      unsigned int cur_group_id_;
      
      /** The execution profiling */
      TaskProfiling *exec_profiling_;
};

#endif
