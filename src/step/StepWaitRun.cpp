#include "glog/logging.h"
#include "mpi.h"

#include "StepWaitRun.h"
#include "../LocalNode.h"

StepWaitRun::StepWaitRun(LocalNode &ln) : Step(ln) {
}

void StepWaitRun::init () {
   DLOG(INFO) << "Starting WAIT RUN phase";
}

void StepWaitRun::begin_iter () {
   unsigned int cur_freq_id = node_utils_.running_thread_->get_cur_freq_id ();
   if (cur_freq_id > 0) {
      node_utils_.running_thread_->set_freq (cur_freq_id - 1);
   }
}

void StepWaitRun::end_iter () {
   int eval_next = 0, oth_eval_next;
   PMPI_Allreduce(&eval_next, &oth_eval_next, 1, MPI_INTEGER, MPI_BOR, MPI_COMM_WORLD);
   if (!oth_eval_next) {
      localNode_.set_step(STP_RUN_IT0);
      return;
   }
}

