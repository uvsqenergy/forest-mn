#ifndef FORESTMN_STEPWAITEVAL_H
#define FORESTMN_STEPWAITEVAL_H

#include <set>
#include "Step.h"
#include "../LocalNode.h"

// TODO in all steps, disable copy and assignment
// PLUS TODO fix constructors: do Step (ln) in all derived steps
class StepWaitEval : public Step {
   public:
      explicit StepWaitEval (LocalNode &ln);
      ~StepWaitEval () {}
      void init ();
      void begin_iter ();
      void end_iter ();
      void start_slack () {}
      bool notify_new_iters () const{ return true; }
      bool notify_new_tasks () const{ return true; }
};

#endif

