#ifndef FOREST_WRAPPER_H
#define FOREST_WRAPPER_H

#include <set>
#include <mpi.h>
#include <string>

class CommInfo {
public:
   enum Name {
      MPI_NONE = 0, MPI_RECV,
      MPI_SEND, MPI_IRECV, MPI_ISEND,
      MPI_BARRIER, MPI_ALLREDUCE, MPI_REDUCE, 
      MPI_RECV_INIT, MPI_SEND_INIT, MPI_WAITALL,
      MPI_GATHER, MPI_ALLTOALLV, MPI_BCAST,
      MPI_GATHERV, MPI_ALLTOALL, MPI_SENDRECV,
      MPI_ISSEND
   };

   CommInfo () : persist (false) {}

   static inline std::string toString (int comm_name) {
      switch (comm_name) {
         case MPI_NONE: return std::string ("None");
         case MPI_RECV: return std::string ("MPI_Recv");
         case MPI_SEND: return std::string ("MPI_Send");
         case MPI_IRECV: return std::string ("MPI_Irecv");
         case MPI_ISEND: return std::string ("MPI_Isend");
         case MPI_BARRIER: return std::string ("MPI_Barrier");
         case MPI_ALLREDUCE: return std::string ("MPI_Allreduce");
         case MPI_REDUCE: return std::string ("MPI_Reduce");
         case MPI_RECV_INIT: return std::string ("MPI_Recv_init");
         case MPI_SEND_INIT: return std::string ("MPI_Send_init");
         case MPI_WAITALL: return std::string ("MPI_Waitall");
         case MPI_GATHER: return std::string ("MPI_Gather");
         case MPI_ALLTOALLV: return std::string ("MPI_Alltoallv");
         case MPI_BCAST: return std::string ("MPI_Bcast");
         case MPI_GATHERV: return std::string ("MPI_Gatherv");
         case MPI_ALLTOALL: return std::string ("MPI_Alltoall");
         case MPI_SENDRECV: return std::string ("MPI_Sendrecv");
         case MPI_ISSEND: return std::string ("MPI_Issend");
         default: return std::string ("Error_default");
      }
   }

   inline void set_persistent (bool value) {
      persist = value;
   }

   inline bool persistent () const{
      return persist;
   }

   inline void insert_src (const std::set <int>& values) {
      src.insert (values.begin (), values.end ());
   }

   inline void insert_src (int value) {
      src.insert (value);
   }

   inline void insert_dst (const std::set <int>& values) {
      dst.insert (values.begin (), values.end ());
   }

   inline void insert_dst (int value) {
      dst.insert (value);
   }

   inline void set_comm_name (Name cname) {
      comm_name = cname;
   }

   inline Name get_comm_name () const{
      return comm_name;
   }

   const std::set <int>& get_src () const{
      return src;
   }

   const std::set <int>& get_dst () const{
      return dst;
   }
  
private:
   Name comm_name;
   bool persist;
   std::set <int> src;
   std::set <int> dst;
};

#endif
