#include "SlackOvhd.h"

#include <glog/logging.h>
#include <fstream>

#include <mpi.h>

SlackOvhd *SlackOvhd::instance_ = NULL;

SlackOvhd::SlackOvhd() {
   bool p2p_found, coll_found;
   int mpi_csize;

   PMPI_Comm_size(MPI_COMM_WORLD, &mpi_csize);

   std::ifstream fs ("comm_ovhd.dat");

   CHECK(fs.bad()) << "Failed to open 'comm_ovhd' file. Please run the offline communication cost measurement tool.";

   p2p_found = false;
   coll_found = false;
   while ((!p2p_found || !coll_found) && fs.good()) {
      int tmpn;
      uint64_t tmpv;

      fs >> tmpn >> tmpv;

      if (tmpn == 2) {
         p2p_cost_ = tmpv;
         p2p_found = true;
      } else if (tmpn == mpi_csize) {
         coll_cost_ = tmpv;
         coll_found = true;
      }
   }

   CHECK(p2p_found) << "Can't find a communicaiton overhead for p2p comms. Please run again the communication benchmark";
   CHECK(coll_found) << "Can't find a communication overhead for " << mpi_csize << " nodes. Please extend your dataset accordingly";
}
