#ifndef FOREST_COMMON_H
#define FOREST_COMMON_H

#include <stdint.h>

/**
 * Communication direction, relatively to us.
 */
enum commDir {
   CD_SEND,
   CD_RECV
};

// a few utility functions which may be useful at some point
#define likely(x)    __builtin_expect (!!(x), 1)
#define unlikely(x)  __builtin_expect (!!(x), 0)

/** Max allowed slowdown */
#define MAX_SLOWDOWN 1.05f

/** Approximate frequency transition time (in RDTSC cycles) */
#define FREQ_TRANS_LAT 400000UL

/**
 * Returns the current time stamp. Timestamps are arbitrary values monotonically
 * incremented as finely as possible (if posssible at the cycle scale),
 * independantly from the current frequency.
 *
 * @return An arbitrary value representing the current time.
 */
static __inline__ uint64_t get_time() {
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ((unsigned long long) lo) | (((unsigned long long) hi) << 32);
}

#endif
