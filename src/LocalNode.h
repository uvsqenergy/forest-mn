#ifndef FOREST_LOCALNODE_H
#define FOREST_LOCALNODE_H

#include "Task.h"
#include "TaskSequence.h"
#include "HWCounter.h"
#include "NodeUtils.h"

#include <mpi.h>
#include <stdint.h>
#include <set>
#include <vector>

class Step;

/** Current step executed by a local node. */
enum StepLN {
   STP_LEARNING = 0, /*!< learning iteration, task graph unsure yet */
   STP_CHECKING,  /*!< checking stability of task graph */
   STP_WAIT_EVAL, /*!< waiting for the others before reducing the frequency */
   STP_EVAL_FREQ, /*!< evaluate the impact of frequencies on the tasks */
   STP_WAIT_RUN,  /*!< waiting for the others before actually running */
   STP_RUN_IT0,   /*!< everything is fine, we run the first iteration */
   STP_RUN_ITN,   /*!< running the other iterations */
   STP_SIZE       /*!< Variable to keep track of the number of steps */
};

class LocalNode {
   public:
      /**
       * Returns the instance of LocalNode
       *
       * @return The instance of LocalNode that represents this node.
       */
      static LocalNode *get_LN();

      /**
       * Creates the LocalNode instance that can be accessed through getLN.
       */
      static LocalNode *instantiate ();

      /**
       * Ends the preceding iteration and starts a new one.
       */
      void new_iter();

      /**
       * We're out of the loop.
       */
      void end_loop();

      /**
       * Get the string equivalent of the current step
       */
      const char *get_step_str ();

      /**
       * Called when the task starts to wait for something.
       */
      void start_slack();

      /**
       * Called when a tasks starts to communicate.
       *
       * @param dir Is it a send or a recv?
       * @param ids The list of related senders or dests.
       */
      void start_comm(int comm_name, const std::set<int>& src, const std::set<int>& dst);
      
      /**
       * Called when the task starts to compute.
       */
      void start_comp();

      /**
       * Destroys the singleton LocalNode instance it it exists.
       */
      static inline void destroy () {
         if (instance_ != NULL) {
            DLOG(INFO) << "DESTROY CALLED, deleting instance" << std::endl;
            delete instance_;
            instance_ = NULL;
         }
      }

      /**
       * Empty the task sequences associated to the given frequency range.
       * freqMin is inclusive, freqMax is exclusive : the range tasks within 
       * the range [freqMin, freqMax) will be truncated.
       *
       * @param freqMin The frequency of the first sequence to empty.
       * @param freqMax The frequency after the last sequence to empty.
       */
      void delete_tasks (unsigned int freqMin, unsigned int freqMax);

      /**
       * Empty the task sequence associated to the given frequency.
       *
       * @param freq The frequency of the task sequence to empty.
       */
      void delete_tasks (unsigned int freq);

      /**
       * Empty all the tasks sequences.
       */
      void delete_tasks();

      /**
       * Set the new step for the algorithm.
       *
       * @param step the new step to be set as the current one
       * @sa steps_ step_
       */
      void set_step (StepLN step);
     
      /**
       * Returns the best frequency sequence built so far for the task sequence.
       *
       * @return The frequency sequence.
       */
      inline std::vector<unsigned int> &get_freq_seq() {
         return freq_seq_;
      }

      /**
       * Returns the ideal frequency sequence for the task sequence. Ideal
       * frequencies are the ones minimizing energy consumption for each 
       * individual task; they are not necessarily globally optimal.
       *
       * @return The list of ideal frequency to apply to each task. The 
       * frequencies are in the same order as in the task sequence, i.e. the
       * first frequency corresponds to the first task in the sequence.
       */
      inline std::vector<unsigned int> &get_max_freqs() {
         return max_freqs_;
      }

      /**
       * Returns true if the step currently in effect wants to receive events
       * related to tasks communications and slack.
       */
      bool step_wants_comm_info() const;

      NodeUtils& get_node_utils () {
         return node_utils_;
      }
 
      /**
       * The task sequence, representing every task in order of execution
       * in one iteration
       */
      TaskSequence task_seq_;

   private:
      /** Sole private constructor */
      LocalNode();

      /** Private destructor */
      ~LocalNode();

      /** Prevent copy */
      LocalNode(const LocalNode &c);

      /** Prevent affectation */
      LocalNode &operator= (const LocalNode &c);

      /** Array of steps the program is going to go through. Involves
       * initializing the frequency scaler, evaluating frequencies
       * and running the decisions **/
      Step *steps_ [STP_SIZE];

      /** Sole instance of the local node */
      static LocalNode *instance_;

      /** Id of the iteration currently running */
      long unsigned int iter_;

      /** Current step being executed */
      StepLN step_;
     
      /** Sequence of maximal frequency to set per task.
       * One entry per task, in the same order as in task sequence. */
      std::vector<unsigned int> max_freqs_;

      /** Currently set frequency sequence. One frequency id per task. */
      std::vector<unsigned int> freq_seq_;

      NodeUtils node_utils_;
};

#endif
