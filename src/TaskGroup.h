#ifndef FORESTMN_TASKGROUP_H
#define FORESTMN_TASKGROUP_H

#include <vector>
#include <stdint.h>
#include "glog/logging.h"

#include "Task.h"
#include "DVFSUnitUtils.h"

class TaskGroup {
public:
   TaskGroup ();
   
   /**
    * Destructor: Deletes the TaskGroup, and call the destructor on all
    * the Task objects in the tasks_ vector
    */
   ~TaskGroup ();

   static const unsigned int time_threshold;
   
   inline Task *get_task (unsigned int task_id) {
      CHECK (task_id < tasks_.size ()) << "Error: Invalid task id (" << task_id << "/" << tasks_.size () << ")";
      return tasks_ [task_id];
   }

   inline uint64_t get_duration () const{
      return total_duration_;
   }
 
   inline unsigned int get_nb_tasks () const{
      return tasks_.size ();
   }

   inline void set_freq_id (unsigned int freq_id) {
      freq_id_ = freq_id;
   }

   inline unsigned int get_freq_id () const{
      return freq_id_;
   }

   // TODO comment
   float get_ips (unsigned int f) const;

   void resize (unsigned int s);
   bool append (Task *t);

private:
   std::vector <Task*> tasks_;
   uint64_t total_duration_;
   unsigned int freq_id_;
};

#endif
