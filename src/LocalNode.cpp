#include "Common.h"
#include "LocalNode.h"
#include "step/StepLearning.h"
#include "step/StepChecking.h"
#include "step/StepWaitEval.h"
#include "step/StepEvalFreq.h"
#include "step/StepWaitRun.h"
#include "step/StepRunIt0.h"
#ifdef TRACING
#include "step/StepRunItNTracer.h"
#else
#include "step/StepRunItN.h"
#endif

#include <algorithm>
#include <glog/logging.h>
#include <cassert>
#include <cstring> // defines NULL
#include <iostream>
#include <set>
#include <mpi.h>

#ifndef NDEBUG
#define UNW_LOCAL_ONLY
#include <libunwind.h>
#endif

LocalNode *LocalNode::instance_ = NULL;

LocalNode::LocalNode() :
   iter_ (0), step_(STP_LEARNING)
{
   // Get power information from the running dvfs unit from offline
   node_utils_.running_dvfs_unit_->read_power ();

   // Create step elements
   LocalNode &ln = *this;
   steps_ [STP_LEARNING] = new StepLearning (ln);
   steps_ [STP_CHECKING] = new StepChecking (ln);
   steps_ [STP_WAIT_EVAL] = new StepWaitEval (ln);
   steps_ [STP_EVAL_FREQ] = new StepEvalFreq (ln);
   steps_ [STP_WAIT_RUN] = new StepWaitRun (ln);
   steps_ [STP_RUN_IT0] = new StepRunIt0 (ln);
#ifdef TRACING
   steps_ [STP_RUN_ITN] = new StepRunItNTracer (ln);
#else
   steps_ [STP_RUN_ITN] = new StepRunItN (ln);
#endif
}

LocalNode::~LocalNode() {
   for (unsigned int i = 0; i < STP_SIZE; i++) {
      delete steps_ [i];
   }
}

LocalNode *LocalNode::get_LN() {
   return instantiate ();
}

LocalNode *LocalNode::instantiate () {
   if (LocalNode::instance_ == NULL) {
       LocalNode::instance_ = new LocalNode ();
   }
   return LocalNode::instance_;
}

void LocalNode::new_iter() {
   // We do that because we want to start the step learning phase
   // every time we start a new loop (there could be several loops
   // in the program)
   if (iter_ == 0) {
      set_step (STP_LEARNING);
   } else {
      // End the previous iteration (if any)
      if (steps_ [step_]->notify_new_tasks ()) {
         LOG (WARNING) << "End Task " << get_step_str () << std::endl;
         steps_ [step_]->end_task ();
      }
      LOG (WARNING) << "End iter " << get_step_str () << std::endl;
      steps_ [step_]->end_iter ();
   }

   // Launch the beginning of the iteration
   // while we want to change it
   StepLN oldStep;
   do {
      oldStep = step_;
      LOG (WARNING) << "Begin Iter " << get_step_str () << std::endl;
      steps_ [step_]->begin_iter (); // Not sure we should call begin_iter every time we change a step...
      if (oldStep == step_ && steps_ [step_]->notify_new_tasks ()) {
         LOG (WARNING) << "Begin Task " << get_step_str () << std::endl;
         steps_ [step_]->begin_task ();
      }
   } while (oldStep != step_);

   // Increment the number of iterations we have handled so far
   iter_++;
}

const char *LocalNode::get_step_str () {
   switch (step_) {
      case STP_LEARNING:   return "Learning";
      case STP_CHECKING:   return "Checking";
      case STP_WAIT_EVAL:  return "WaitEval";
      case STP_EVAL_FREQ:  return "EvalFreq";
      case STP_WAIT_RUN:   return "WaitRun";
      case STP_RUN_IT0:    return "RunIt0";
#ifdef TRACING
      case STP_RUN_ITN:    return "RunItNTracer";
#else
      case STP_RUN_ITN:    return "RunItN";
#endif
      default:             return "Unknown";
   }
}

void LocalNode::end_loop() {
   // At the end of the loop, we want to
   // end the last task and the last iteration,
   // but only if there were at least one iteration running
   if (iter_ != 0) {
      if (steps_ [step_]->notify_new_tasks ()) {
         steps_ [step_]->end_task ();
      }
      steps_ [step_]->end_iter ();
   }

   // Reset the frequency to maximum frequency, for remaining parts of the code
   node_utils_.running_thread_->set_freq (DVFSUnitUtils::freq_max_id);

   // Reset the number of iterations in case there are several
   // loops in the program
   iter_ = 0;
}

void LocalNode::start_slack() {
   if (steps_ [step_]->notify_new_tasks ()) {
      steps_ [step_]->start_slack ();
   }
}

void LocalNode::start_comm(int comm_name, const std::set<int>& src, const std::set<int>& dst) {
   if (steps_ [step_]->notify_new_tasks ()) {
      steps_ [step_]->start_comm (comm_name, src, dst);
   }
}

void LocalNode::start_comp() {
   if (steps_ [step_]->notify_new_tasks ()) {
      steps_ [step_]->end_task ();
      steps_ [step_]->begin_task ();
   }
}

void LocalNode::set_step (StepLN step) {
   step_ = step;
   LOG (WARNING) << "Initializing step " << get_step_str () << std::endl;
   steps_ [step_]->init ();
}

bool LocalNode::step_wants_comm_info () const{
   return steps_ [step_]->notify_comms ();
}

