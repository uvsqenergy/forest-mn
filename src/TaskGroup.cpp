#include <cstdlib>
#include <vector>
#include "glog/logging.h"

#include "TaskGroup.h"
#include "Task.h"
#include "DVFSUnitUtils.h"

const unsigned int TaskGroup::time_threshold = 1e7;

TaskGroup::TaskGroup () :
   total_duration_ (0),
   freq_id_ (0) {
}

TaskGroup::~TaskGroup () {
   for (unsigned int i = 0; i < tasks_.size (); i++) {
      delete tasks_ [i], tasks_ [i] = NULL;
   }
}

void TaskGroup::resize (unsigned int s) {
   DCHECK (s <= tasks_.size ()) << "Error: Invalid requested size (" << s << "/" << tasks_.size () << ")";
   tasks_.resize (s);

   // We also have to recompute the new duration
   total_duration_ = 0;
   for (unsigned int i = 0; i < tasks_.size (); i++) {
      TaskProfiling& prof = tasks_ [i]->get_profiling (DVFSUnitUtils::freq_max_id);
      total_duration_ += prof.get_task_length ();
   }
}

float TaskGroup::get_ips (unsigned int freq_id) const{
   DCHECK (freq_id <= DVFSUnitUtils::freq_max_id) << "Error: Invalid frequency id (" << freq_id << "/" << DVFSUnitUtils::freq_max_id << ")";
   uint64_t total_fdep_time = 0, total_fdep_instrs = 0;
   for (unsigned int i = 0; i < tasks_.size (); i++) {
      TaskProfiling& tp = tasks_ [i]->get_profiling (freq_id);
      total_fdep_time += tp.get_fdep_time ();
      total_fdep_instrs += tp.get_fdep_instrs ();
   }
   if (total_fdep_time == 0) {
      return 0;
   }

   return (float)total_fdep_instrs / (float)total_fdep_time;
}

bool TaskGroup::append (Task *t) {
   // If the TaskGroup duration is too large already
   if (total_duration_ > TaskGroup::time_threshold) {
      return false;
   }
   
   // Otherwise, push it back in the vector
   tasks_.push_back (t);

   // Add its duration to the total duration
   TaskProfiling& prof = t->get_profiling (DVFSUnitUtils::freq_max_id);
   total_duration_ += prof.get_task_length ();
   
   return true;
}

