#include <cstdlib>
#include <sched.h>
#include "mpi.h"
#include "libdvfs/libdvfs.h"
#include "glog/logging.h"

#include "ThreadUtils.h"
#include "DVFSUnitUtils.h"

ThreadUtils::ThreadUtils (const DVFSUnitUtils& dvfs_unit, dvfs_core *core,
                              unsigned int local_mpi_rank) :
   local_mpi_rank_ (local_mpi_rank), core_ (core),
   dvfs_unit_ (dvfs_unit),
   cur_freq_id_ (0),
   hwc_ ("INST_RETIRED:ANY_P") {
   // Paranoid
   CHECK (core != NULL) << "Error: Invalid thread creation";

   // Get global MPI rank (as in MPI_COMM_WORLD)
   int initialized;
   MPI_Initialized (&initialized);

   if (initialized) {
      int global_mpi_rank;
      MPI_Comm_rank (MPI_COMM_WORLD, &global_mpi_rank);
      global_mpi_rank_ = global_mpi_rank;
   } else {
      global_mpi_rank_ = 0;
   }
}

int ThreadUtils::pin () const{
   cpu_set_t set;
   CPU_ZERO (&set);
   CPU_SET (local_mpi_rank_, &set);

   return sched_setaffinity (0, sizeof(set), &set);
}
