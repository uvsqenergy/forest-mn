#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include "glog/logging.h"

#include "Task.h"
#include "TaskProfiling.h"
#include "DVFSUnitUtils.h"

Task::Task() :
   profilings_ (DVFSUnitUtils::freq_max_id + 2, TaskProfiling ()),
   comm_name_ (0),
   max_freq_id_ (0),
   virtual_freq_id_ (0)
{}

Task::~Task() {}

void Task::set_comm(const std::set<int>& src, const std::set<int>& dst) {
   src_ = src;
   dst_ = dst;
}

std::string Task::printString () const{
   std::ostringstream oss;

   oss << "T< ";
   for (std::set<int>::iterator it = src_.begin(); it != src_.end(); it++) {
      oss << *it << ' ';
   }

   oss << "T> ";
   for (std::set<int>::iterator it = dst_.begin(); it != dst_.end(); it++) {
      oss << *it << ' ';
   }
   oss << std::endl;

   return oss.str ();
}

void Task::print() const{
   const TaskProfiling& prof = profilings_ [DVFSUnitUtils::freq_max_id];
   bool toosmall = (prof.get_comm_time () + prof.get_comp_time ()) < 40 * FREQ_TRANS_LAT;

   DLOG (INFO) << "Vfreqid = " << get_virtual_freq_id ();
   if (toosmall) {
      DLOG (INFO) << "TOO SMALL";
   }
   for (unsigned int i = get_min_eval_freq (); i < DVFSUnitUtils::freq_max_id+1; i++) {
      DLOG (INFO) << "For freq id #" << i;
      profilings_ [i].print ();
   }
   DLOG (INFO) << "Comm: " << printString ();
}

