#ifndef FORESTMN_NODEUTILS_H
#define FORESTMN_NODEUTILS_H

#include "libdvfs/libdvfs.h"
#include "DVFSUnitUtils.h"
#include "ThreadUtils.h"

class NodeUtils {
   public:
      NodeUtils ();
      ~NodeUtils ();
      
      /** The structure containing information about the frequency
       * domain of the considered thread we are running on */
      DVFSUnitUtils *running_dvfs_unit_;

      /** The structure containing information about the physical thread
       * we are running on */
      ThreadUtils *running_thread_;

      /** The list of dvfs units available on the local node */
      std::vector <DVFSUnitUtils*> dvfs_units_;
      
      /** The list of threads available on the local node */
      std::vector <ThreadUtils*> threads_;
      
      /**
       * Returns the MPI rank on the local node (as given by OpenMPI)
       */
      inline unsigned int get_local_mpi_rank () const{
         return local_mpi_rank_;
      }

      // TODO comment
      inline unsigned int nb_dvfs_units () const{
         return dvfs_units_.size ();
      }

      inline unsigned int nb_threads () const{
         return threads_.size ();
      }

      inline DVFSUnitUtils *get_dvfs_unit (unsigned int id) const{
         CHECK (id < dvfs_units_.size ()) << "Error: invalid dvfs unit id (" << id << "/" << dvfs_units_.size () << ")";
         return dvfs_units_ [id];
      }

      inline ThreadUtils *get_thread (unsigned int id) const{
         CHECK (id < threads_.size ()) << "Error: invalid dvfs unit id (" << id << "/" << threads_.size () << ")";
         return threads_ [id];
      }

   private:
      /** The local MPI rank (as given by OpenMPI) */
      unsigned int local_mpi_rank_;

      /**
       * Retrieves the MPI rank on the local Node thanks to OpenMPI
       * environment variables. This function sets the local_mpi_rank_
       * class variable.
       */
      void find_local_mpi_rank ();
      dvfs_ctx *dvfs_context_;
};

#endif
