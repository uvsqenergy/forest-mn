#ifndef FOREST_SLACKOVHD_H
#define FOREST_SLACKOVHD_H

#include <cstddef>
#include <stdint.h>
#include <map>

/**
 * @class The SlackOvhd is able to determine an average cost of a communication
 * between N nodes. It accounts for the time spent in the MPI library plus an
 * average communication overhead.
 */
class SlackOvhd {
   public:
      /**
       * Returns the slack overhead instance. Builds the instance if needed.
       *
       * @return The singleton instance of SlackOvhd
       */
      static const SlackOvhd &get_instance() {
         if (SlackOvhd::instance_ == NULL) {
            SlackOvhd::instance_ = new SlackOvhd();
         }

         return *SlackOvhd::instance_;
      }

      /**
       * Cleans the slack overhead instance if it has been created.
       */
      static void destroy() {
         if (SlackOvhd::instance_) {
            delete SlackOvhd::instance_;
            SlackOvhd::instance_ = NULL;
         }
      }

      /**
       * Returns an average communication time between two nodes
       *
       * @return The cost of a communication between two nodes (rdtsc cycles).
       */
      inline uint64_t get_p2p_cost() const { return p2p_cost_; }

      /**
       * Returns an average global communication cost.
       *
       * @return The cost of a global communication (rdtsc cycles).
       */
      inline uint64_t get_coll_cost() const { return coll_cost_; }

   private:
      /** Default constructor */
      SlackOvhd();

      /** Sole instance */
      static SlackOvhd *instance_;

      /** Buffer for p2p cost */
      uint64_t p2p_cost_;

      /** Buffer for collective cost */
      uint64_t coll_cost_;
};

#endif

