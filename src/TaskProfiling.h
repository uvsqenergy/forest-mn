#ifndef FORESTMN_TASKPROFILING_H
#define FORESTMN_TASKPROFILING_H

#include <stdint.h>
#include <cstdlib>
#include "glog/logging.h"


class TaskGroup;

class TaskProfiling {
public:
   TaskProfiling () :
      start_ (0), sslack_ (0),
      scomm_ (0), end_ (0),
      iexec_comp_ (0),
      iexec_comm_ (0), egain_ (0.)
   {}
   ~TaskProfiling () {}

   TaskProfiling& operator= (const TaskProfiling& tp);

   /**
    * Sets a new start date at a given frequency
    *
    * @param start The new start date to consider for the considered profiling.
    */
   inline void set_start_date (uint64_t t) {
      reset (t);
   }
 
   /**
    * Resets the task and set a new start date.
    *
    * @param start The new start date to consider for the considered profiling.
    */
   inline void reset(uint64_t t) {
      start_ = t;
      sslack_ = t;
      scomm_ = t;
      end_ = t;
      iexec_comp_ = 0;
      iexec_comm_ = 0;
   }
 
   /**
    * Sets the time when the task starts to wait for others.
    *
    * @param t The start sack date.
    */
   inline void set_start_slack(uint64_t t) {
      DCHECK (t >= start_) << "Error: Start slack date (" << t << ") cannot be smaller than the corresponding task start date (" << start_ << ")";
   
      sslack_ = t;
   }

   /**
    * Sets the time when the task starts to communicate.
    *
    * @param t The date when the task starts to communicate.
    */
   inline void set_start_comm (uint64_t t) {
      if (sslack_ == start_) {
         set_start_slack (t);
      }
   
      DCHECK (t >= sslack_) << "Error: Start comm date (" << t << ") cannot be smaller than the corresponding start slack date (" << sslack_ << ")";
   
      scomm_ = t;
   
      // WARNING: This is a trick which compensates irrealistic scomm_ start time
      // value because it cannot be measured otherwise
      //
      // TODO Another method, which could improve the efficiency of this code would be
      // to measure in an offline phase the overhead of the communication part
      /*if (scomm_ - sslack_ > 140000) {
         scomm_ -= 140000;
      }*/
   }

   /**
    * Sets the energy ratio over the energy of the same task running at the
    * highest frequency.
    *
    * @param egain The energy gain to set.
    */
   inline void set_energy_gain(float egain) {
      egain_ = egain;
   }

   /**
    * Sets the date when the tasks ends (including comms and slack).
    *
    * @param t The task end date.
    */
   void set_end_task(uint64_t t);

   /**
    * Sets the number of instructions executed in the computation phase
    * of the task
    *
    * @param iexec The number of executed instructions.
    */
   inline void set_comp_instrs(uint64_t iexec) {
      iexec_comp_ = iexec;
   }

   /**
    * Sets the number of instructions executed in the communication phase
    * of the task
    *
    * @param iexec The number of executed instructions.
    */
   inline void set_comm_instrs (uint64_t iexec) {
      iexec_comm_ = iexec;
   }

   inline uint64_t get_fdep_instrs () const {
      return iexec_comp_ + iexec_comm_;
   }

   /**
    * Returns the time spent in the computation.
    *
    * @return The computation time for this task.
    */
   inline uint64_t get_comp_time() const {
      return sslack_ - start_;
   }

   inline uint64_t get_fdep_time () const{
      return get_comp_time () + get_comm_time ();
   }

   /**
    * Returns the start date of the task computation.
    *
    * @return The computation start date for this task.
    */
   inline uint64_t get_comp_start_date () const {
      return start_;
   }

   /**
    * Returns the slack time for this task.
    *
    * @return The task slack time.
    */
   inline uint64_t get_slack_time() const {
      DCHECK (scomm_ >= sslack_) << " Error: slack cannot be negative, scomm (" << scomm_ << ") or sslack_ (" << sslack_ << ") were not set properly";

      return scomm_ - sslack_;
   }

   /**
    * Returns the start date of the slack for this task.
    *
    * @return The task slack time.
    */
   inline uint64_t get_slack_start_date () const {
      return sslack_;
   }

   /**
    * Returns the communication time.
    *
    * @return The time spent in communications.
    */
   inline uint64_t get_comm_time() const {
      return end_ - scomm_;
   }

   /**
    * Returns the start date of the task communication
    *
    * @return The start dateof the task communication.
    */
   inline uint64_t get_comm_start_date () const {
      return scomm_;
   }
   
   /**
    * Returns the start date of the task communication
    *
    * @return The start dateof the task communication.
    */
   inline uint64_t get_end_date () const {
      return end_;
   }
   
   /**
    * Returns the total duration of the task (including comms and slack).
    *
    * @return The task duration
    */
   inline uint64_t get_task_length() const {
      return end_ - start_;
   }

   /**
    * Returns a metric proportional to IPS for this task (computed for the
    * computation period only: slack and communications times are ignored).
    *
    * @return The task IPS (in instructions per cycles at max freq).
    */
   inline float get_ips() const{
      return (float) get_fdep_instrs () / get_fdep_time ();
   }

   /**
    * Returns the energy ratio of this task of the same one running at the 
    * highest frequency.
    *
    * @return The energy ratio of this task.
    */
   inline float get_energy_gain() const {
      return egain_;
   }

   /**
    * Returns whether a slack date has been set since the last reset (or since
    * the Task object creation)
    */
   inline bool has_slack_date_yet () const{
      return start_ != sslack_;
   }

   /**
    * Compares two tasks and returns true if their execution time match. No 
    * comparision is performed on communication destinations/sources.
    *
    * @param t The other task to compare with.
    */
   inline bool is_similar(const TaskProfiling &tp) const{
      uint64_t slack, ddiff, sdiff;
   
      uint64_t m_dur = get_task_length ();
      uint64_t tp_dur = tp.get_task_length ();
      uint64_t ips_diff = ::abs (get_ips () - tp.get_ips ());

      if (m_dur < 1e8 && tp_dur < 1e8) {
         return true;
      }

      // Chekc that IPS remains similar
      if (m_dur > 1e6 && tp_dur > 1e6
          && ips_diff > 0.7) {
         DLOG (INFO) << "Unstable IPS: new: " << get_ips () << ", old = " << tp.get_ips ();
         print ();
         tp.print ();
         return false;
      }

      // Checks that task length remains similar
      ddiff = ::abs((int64_t) tp_dur - (int64_t) m_dur);
      if (ddiff > 1.5e7 && ddiff > m_dur * 0.1f) {
         DLOG (INFO) << "Unstable duration: new: " << m_dur << ", old = " << tp_dur;
         print ();
         tp.print ();
         return false;
      }
   
      // Check that slacks remains similar
      slack = get_slack_time();
      sdiff = ::abs((int64_t) tp.get_slack_time() - (int64_t) slack);
      if (sdiff > 1e8 && sdiff > slack * 0.1f) {
         DLOG (INFO) << "Unstable slack: new: " << slack << ", old = " << tp.get_slack_time ();
         print ();
         tp.print ();
         return false;
      }

      return true;
   }

   void print () const;

private:
   uint64_t start_; /** Start date */
   uint64_t sslack_; /** Start date of slack */
   uint64_t scomm_; /** Start date of communication */
   uint64_t end_;  /** Task end date */
   uint64_t iexec_comp_; /** Number of instructions executed during the computation */
   uint64_t iexec_comm_; /** Number of instructions executed during the communication */
   float egain_; /** Energy gain over the same task running at the highest freq. */
};

#endif
