#ifndef FORESTMN_THREADUTILS_H
#define FORESTMN_THREADUTILS_H

#include <errno.h>

#include "glog/logging.h"
#include "libdvfs/libdvfs.h"

#include "DVFSUnitUtils.h"
#include "HWCounter.h"

class ThreadUtils {
public:
   /**
    * Constructor
    *
    * @param dvfs_unit The DVFS Unit the thread is attached to (as given by libdvfs)
    * @param core The dvfs_core structure corresponding to the running thread
    * @param local_mpi_rank The MPI rank in the local node (as given by OpenMPI)
    */
   ThreadUtils (const DVFSUnitUtils& dvfs_unit, dvfs_core *core, unsigned int local_mpi_rank_);
   
   /**
    * Sets a frequency to the thread given a frequency id corresponding to
    * the entry id in the sorted list of available frequencies on the DVFS Unit
    * the thread belongs to
    *
    * @param freq_id The frequency id corresponding to the entry id in the
    * sorted list of available frequencies on the DVFS Unit the thread
    * belongs to
    */
   void set_freq (unsigned int freq_id) {
      unsigned int freq = dvfs_unit_.get_freq (freq_id);
      unsigned int ret = dvfs_core_set_freq (core_, freq);
      CHECK (ret) << "Error: LocalMPIRank #" << local_mpi_rank_ << " cannot set frequency " << freq << ": " << strerror (errno);

      cur_freq_id_ = freq_id;
   }

   /**
    * Pin down the current MPI process to the physical thread
    * corresponding to its local MPI rank (as given by OpenMPI)
    *
    * @return Returns the internal call to sched_setaffinity. Therefore,
    * on success, it returns 0. On error, -1 is returned and errno
    * is set appropriately. See the sched_setaffinity manpage for more
    * details.
    */
   int pin () const;

   /**
    * Returns the MPI rank in the local node as described by OpenMPI
    */
   inline unsigned int get_local_mpi_rank () const{
      return local_mpi_rank_;
   }

   inline unsigned int get_cur_freq_id () const{
      return cur_freq_id_;
   }

   /**
    * Returns the Hardware counter structure corresponding to the thread
    */
   inline HWCounter& get_hwc () {
      return hwc_;
   }

   inline unsigned int get_global_mpi_rank () const{
      return global_mpi_rank_;
   }

   /**
    * Returns the frequency id currently applied on the thread
    */
   inline unsigned int get_freq_id () const{
      return cur_freq_id_;
   }

private:
   /** The local MPI rank (as given by OpenMPI) */
   unsigned int local_mpi_rank_;

   /** The global MPI rank (in MPI_COMM_WORLD) */
   unsigned int global_mpi_rank_;

   /** The dvfs_core structure corresponding to the running thread
    * ( as given by libdvfs)
    */
   dvfs_core *core_;
   
   /** The DVFS Unit to which the thread belongs to */
   const DVFSUnitUtils &dvfs_unit_;

   /** The id of the frequency currently set on the machine */
   unsigned int cur_freq_id_;

   /** The Hardware counter data structure getting information from this
    * physical thread
    */
   HWCounter hwc_;
};

#endif
