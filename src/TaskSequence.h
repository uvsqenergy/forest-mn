#ifndef FORESTMN_TASKSEQUENCE_H
#define FORESTMN_TASKSEQUENCE_H

#include <cassert>
#include <vector>
#include <sstream>
#include <sched.h>
#include <map>
#include <utility>
#include "glog/logging.h"

#include "TaskGroup.h"
#include "Task.h"
#include "DVFSUnitUtils.h"

struct TaskToGroupEntry {
   Task *task;
   unsigned int group_id;
   unsigned int id_in_group;
};

class TaskSequence {
   public:
      TaskSequence() {}
      ~TaskSequence() {
         // Deleting each group, all the tasks will be destroyed by the groups
         for (unsigned int i = 0; i < tasks_groups_.size (); i++) {
            delete tasks_groups_ [i];
         }
      }

      /**
       * Access the elements in the sequence.
       *
       * @param i The element index.
       * @return A reference to the designated element.
       */
      inline Task *&operator[] (unsigned int i) {
         DCHECK(i < size()) << "Error: Invalid task requested: (" << i << "/" << size() << ")";
         return tasks_to_groups_ [i].task;
      }

      // TODO comment
      inline const Task *operator [] (unsigned int i) const{
         DCHECK (i < size ()) << "Error: Invalid task id (" << i << "/" << size () << ")";
         return tasks_to_groups_ [i].task;
      }

      /**
       * Returns the number of tasks in the sequence.
       *
       * @return The number of tasks.
       */
      inline unsigned int size() const {
         return tasks_to_groups_.size();
      }

      /**
       * Appends a new task in the TaskGroup sequence
       *
       * @param start_date The start date of the new task
       */
      inline Task *append_new_task (uint64_t start_date) {
         // Locally-created new task with requested start date
         Task *new_task = new Task ();

         // We set the start date to the max freq profiling id, as we only
         // add new tasks in steps dealing with maximum frequency
         new_task->get_profiling (DVFSUnitUtils::freq_max_id).set_start_date (start_date);

         // Append it to the sequence
         append (new_task);

         return new_task;
      }

      inline Task *get_last_task () {
         DCHECK (tasks_to_groups_.size () > 0) << "Error: Tried to get the last task in an empty tasks list";
         return tasks_to_groups_.back ().task;
      }

      inline void replace (unsigned int task_id, const TaskProfiling& tp, unsigned int profiling_id) {
         DCHECK (task_id < size ()) << "Error: Invalid task id (" << task_id << "/" << size () << ")";
         tasks_to_groups_ [task_id].task->replace_profiling (tp, profiling_id);
      }

      /**
       * Truncates the sequence to the given size.
       * 
       * @param s The new sequence size.
       */
      inline void truncate (unsigned int s) {
         DCHECK (s <= tasks_to_groups_.size()) << "Error: Invalid size (" << s << "/" << tasks_to_groups_.size () << ")";
         // No need to do anything here
         if (tasks_to_groups_.size () == 0 || tasks_groups_.size () == 0) {
            return;
         }

         // This is the last task in the new list, we want to erase
         // all elements beyond this limit
         unsigned int first_group_to_delete;
         if (s == 0) {
            first_group_to_delete = 0;
         } else {
            unsigned int last_task_id = s - 1;
            const TaskToGroupEntry& taskEntry = tasks_to_groups_ [last_task_id];
            unsigned int id_in_group = taskEntry.id_in_group;

            // If the considered task is the first task in a TaskGroup
            if (id_in_group == 0) {
               // We want to delete the task group itself, as we won't have remaining elements
               // inside the structure
               first_group_to_delete = taskEntry.group_id;
            } else {
               // Otherwise, we want to keep it and delete other possible tasks in this group
               first_group_to_delete = taskEntry.group_id + 1;
            
               // Let the TaskGroup handle the resizing itself
               tasks_groups_ [taskEntry.group_id]->resize (id_in_group + 1);
            }
         }

         // Delete allocated data in all following groups
         for (int i = tasks_groups_.size () - 1; i >= (int)first_group_to_delete; i--) {
            delete tasks_groups_ [i];
            tasks_groups_.pop_back ();
         }

         // The easy part: resize the task_to_groups_ vector
         tasks_to_groups_.resize(s);
      }

      /**
       * Prints the task sequence on the standard output.
       */
      void print() const;

      /**
       * Returns the first receiving task before t. If no task with a receiving
       * communication precedes t, -1 is returned. Note that bidirectional 
       * communications are also considered as receiving.
       */
      inline int get_first_receiving_task_before (unsigned int t) {
         DCHECK (t < size ()) << "Out of bounds task #" << t << " (size = " << size () << ")";
         
         int res;
         for (res = t - 1; res >= 0 && tasks_to_groups_ [res].task->is_receiving (); res--);

         return res;
      }

       /**
       * Returns the number of tasks in the sequence receiving information
       * from @p from until it reaches the task id @p end (included).
       *
       * @param from The MPI Rank corresponding to the source of the communications
       * @param end The last task id to be considered in the sequence for calculating
       * the desired number of communications
       *
       */
      inline unsigned int get_nb_tasks_receiving_from (unsigned int from,
                                                    unsigned int end) {
         unsigned int number_of_comms = 0;
         unsigned int taskId;

         // For each task preceding the end task in the sequence
         for (taskId = 0; taskId <= end; taskId++) {
            const std::set<int>& taskSrc = tasks_to_groups_ [taskId].task->get_comm_emitters();

            // If there is a match for the desired source, then increment the
            // corresponding number of comms
            if (taskSrc.find (from) != taskSrc.end ()) {
               number_of_comms++;
            }
         }
         
         LOG (INFO) << "#" << sched_getcpu () << ": I found #" << from << " sent me " << number_of_comms << " before " << end;
         return number_of_comms;
      }

      /**
       * Returns the ith task in the sequence ampong those sending data to
       * @p dest. The desired task itself is the ith one, thus @p i must be > 0.
       *
       * @param dest The MPI rank corresponding to the destination of the communications
       * @param i The number of tasks we want to consider sending data to @p dest from
       * the beginning of the sequence
       *
       * @return Returns the corresponding task id upon success. A fail occurs there is
       * no task corresponding to the number of communications requested, and -1 is
       * returned in this case. If i is 0, -1 is also returned.
       */
      inline int get_ith_task_sending_to (unsigned int dest, unsigned int i) const{
         if (i < 1) {
            return -1;
         }

         unsigned int taskId;
         for (taskId = 0; taskId < size(); taskId++) {
            const std::set<int>& taskDst = tasks_to_groups_ [taskId].task->get_comm_receivers ();

            // is the task sending to dest?
            if (taskDst.find (dest) != taskDst.end ()) {

               // decrement the number of remaining tasks to find
               i--;
               if (i == 0) {
                  break;
               }
            }
         }

         // the requested task cannot be found
         if (taskId >= size()) {
            LOG (ERROR) << "#" << sched_getcpu () << ": I was searching for dest = " << dest << " at position i = " << i;
            return -1;
         }

         return taskId;
      }

      inline TaskGroup& get_task_group (unsigned int group_id) {
         DCHECK (group_id < tasks_groups_.size ()) << "Error: Invalid group id (" << group_id << "/" << tasks_groups_.size () << ")";
         return *tasks_groups_ [group_id];
      }

      inline unsigned int get_nb_groups () const{
         return tasks_groups_.size ();
      }

      inline unsigned int get_group_id (unsigned int task_id) const{
         DCHECK (task_id < size ()) << "Error: Invalid task id (" << task_id << "/" << size () << ")";
         return tasks_to_groups_ [task_id].group_id;
      }

      inline void generate_groups () {
         for (unsigned int i = 0; i < tasks_to_groups_.size (); i++) {
            Task *t = tasks_to_groups_ [i].task;
            if (tasks_groups_.size () == 0 || !tasks_groups_.back ()->append (t)) {
               TaskGroup *new_tg = new TaskGroup ();
               // Append the task in the new TaskGroup, this operation must succeed
               bool hasAppended = new_tg->append (t);
               DCHECK (hasAppended) << "Error: Could not append new task to new TaskGroup";
               // Push back the new taskgroup in the sequence
               tasks_groups_.push_back (new_tg);
            }
            tasks_to_groups_ [i].group_id = tasks_groups_.size () - 1;
            tasks_to_groups_ [i].id_in_group = tasks_groups_.back ()->get_nb_tasks () - 1;
         }
      }

   private:
      inline void append (Task *t) {
         // If the task doesn't fit in the last TaskGroup element,
         // let's create a new one
         /*if (tasks_groups_.size () == 0 || !tasks_groups_.back ()->append (t)) {
            TaskGroup *new_tg = new TaskGroup ();
            // Append the task in the new TaskGroup, this operation must succeed
            bool hasAppended = new_tg->append (t);
            DCHECK (hasAppended) << "Error: Could not append new task to new TaskGroup";
            // Push back the new taskgroup in the sequence
            tasks_groups_.push_back (new_tg);
         }*/
         // Refer the new entry in the task vector to keep track of which task is attached
         // to which group
         TaskToGroupEntry taskEntry;
         taskEntry.task = t;
         taskEntry.group_id = 0;
         taskEntry.id_in_group = 0;
         tasks_to_groups_.push_back (taskEntry);
      }

      std::vector <TaskGroup*> tasks_groups_;
      std::vector <TaskToGroupEntry> tasks_to_groups_;
};

#endif

