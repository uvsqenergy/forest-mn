#!/usr/bin/env python

import re, sys, os
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
import itertools as it

traceData = {}
traceDir = "tracing/cg/"

class V:
    freq = 0
    comp_time = 1
    slack_time = 2
    comm_time = 3
    end_time = 4

freqColors = {
        15: "#FF0000",
        14: "#F2000D",
        13: "#E6001A",
        12: "#D90026",
        11: "#CC0033",
        10: "#BF0040",
         9: "#B2004C",
         8: "#A60059",
         7: "#990066",
         6: "#8C0073",
         5: "#800080",
         4: "#73008C",
         3: "#660099",
         2: "#5900A6",
         1: "#4D00B2",
         0: "#3300CC"
}

# for each entry in the tracing directory
for entry in sorted (os.listdir (traceDir)):
    # Each file has two useful info in their name:
    # trace_i_j.txt corresponds to the jth iteration
    # of the ith node
    entryIdx = re.search ("^trace_hpc-n(\d+)_(\d+)_(\d+).txt$", entry)
    if entryIdx is None:
        print "Error: Cannot parse entry \"" + entry + "\", exiting..."
        sys.exit (0)

    # Get the corresponding indexes and store it in the traceData structure
    nodeIdx = int (entryIdx.group (1))
    threadIdx = int (entryIdx.group (2))
    iterIdx = int(entryIdx.group (3))

    print "Entry: " + entry + ", Node #" + str (nodeIdx) + ", Thread #" + str (threadIdx) + ", Iter #" + str (iterIdx)

    # Open the file, and store its content appropriately
    fd = open (traceDir + entry, "r")
    content = fd.read ()
    fd.close ()
    lines = content.splitlines ()

    # Adding entry only if file is not empty
    if len (lines) > 0:
        # Instantiate a new entry in the dict if not already in there
        if nodeIdx not in traceData:
            traceData [nodeIdx] = {}
        if threadIdx not in traceData:
            traceData [nodeIdx][threadIdx] = {}
        traceData [nodeIdx][threadIdx][iterIdx] = {}
        
        # For each line in the file content, we store the values back
        for taskIdx, line in enumerate (lines):
            # Split elements by space in each line
            data = line.split (" ")
            traceData [nodeIdx][threadIdx][iterIdx][taskIdx] = [float (entry) for entry in data]

# Time elapsed since the beginning of the application
timeElapsed = 0

# Number of nodes traced
nodesNumber = len (traceData)

print "nodesNumber = " + str (nodesNumber)

# Number of threads traced
firstNode = traceData.itervalues().next()
threadsNumber = len (firstNode)

print "threadsNumber = " + str (threadsNumber)

# Number of iterations
firstIter = firstNode.itervalues ().next ()
iterNumber = len (firstIter)

print "iterNumber = " + str (iterNumber)

# Number of tasks per iteration
firstTask = firstIter.itervalues ().next ()
tasksNumber = len (firstTask)

# Normalize time with current iteration
# For each node
for nodeIdx, nodeData in traceData.iteritems ():
    for threadIdx, threadData in nodeData.iteritems ():
        # For each iter
        for iterIdx, iterData in threadData.iteritems ():
            iterStartTime = iterData [0][V.comp_time]
            # For each task
            for taskIdx, taskData in iterData.iteritems ():
                taskData [V.comp_time] = taskData [V.comp_time] - iterStartTime
                taskData [V.slack_time] = taskData [V.slack_time] - iterStartTime
                taskData [V.comm_time] = taskData [V.comm_time] - iterStartTime
                taskData [V.end_time] = taskData [V.end_time] - iterStartTime

# For each iter
for iterIdx in range (iterNumber):
    # The end time of the iteration is necessarily the end time of the last task
    endTime = max ([traceData [i][j][iterIdx][tasksNumber - 1][V.end_time] for i in it.product (range (nodesNumber), range (threadsNumber))])
    
    # Setup the figure
    fig = plt.figure ()
    ax = fig.add_subplot (111) # No idea what that means
    ax.set_xlim (0, nodesNumber)
    ax.set_ylim (0, endTime)

    # For each node
    for nodeIdx in range (nodesNumber):
        print "Node #" + str (nodeIdx) + ", iter #" + str (iterIdx)
        
        # For each task
        for taskIdx, taskData in traceData [nodeIdx][iterIdx].iteritems ():
            # Printing of the computation time
            #print "LB (" + str (nodeIdx) + ", " + str (10 * taskData [V.comp_time] / endTime) + ")"
 
            comp_verts = [
                    (nodeIdx, taskData [V.comp_time]), # left, bottom
                    (nodeIdx, taskData [V.slack_time]), # left, top
                    (nodeIdx + 1, taskData [V.slack_time]), # right, top
                    (nodeIdx + 1, taskData [V.comp_time]), # right, bottom
                    (0., 0.), # ignored
            ]
            slack_verts = [
                    (nodeIdx, taskData [V.slack_time]), # left, bottom
                    (nodeIdx, taskData [V.comm_time]), # left, top
                    (nodeIdx + 1, taskData [V.comm_time]), # right, top
                    (nodeIdx + 1, taskData [V.slack_time]), # right, bottom
                    (0., 0.), # ignored
            ]
            comm_verts = [
                    (nodeIdx, taskData [V.comm_time]), # left, bottom
                    (nodeIdx, taskData [V.end_time]), # left, top
                    (nodeIdx + 1, taskData [V.end_time]), # right, top
                    (nodeIdx + 1, taskData [V.comm_time]), # right, bottom
                    (0., 0.), # ignored
            ]
            codes = [Path.MOVETO,
                     Path.LINETO,
                     Path.LINETO,
                     Path.LINETO,
                     Path.CLOSEPOLY,
                     ]
            if int (taskData [V.freq]) not in freqColors:
                comp_color = "#F200FF"
            else:
                comp_color = freqColors [int (taskData [V.freq])]

            comp = Path(comp_verts, codes)
            slack = Path(slack_verts, codes)
            comm = Path(comm_verts, codes)
            comp_patch = patches.PathPatch (comp, facecolor=comp_color, lw=0)
            slack_patch = patches.PathPatch (slack, facecolor="green", lw=0)
            comm_patch = patches.PathPatch (comm, facecolor="yellow", lw=0)
            ax.add_patch (comp_patch)
            ax.add_patch (slack_patch)
            ax.add_patch (comm_patch)
    plt.show ()

