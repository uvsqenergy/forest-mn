#!/usr/bin/env python
# -*- encoding: utf8 -*-

import urllib2, re

website_root = 'http://www.open-mpi.org/doc/current/'

ignore = ['_f2c$', '_c2f$', 'Errhandler_[^_]+$', '^MPI_Type_.*', 'MPI_Info_.*', '^MPI_Is_thread_main$', 'MPI_Attr_.*', 'MPI_Cart_.*', 'MPI_Status_.*', '.*_create_errhandler$', '^MPI_Initialized$', '^MPI_Finalized$', 'rank[s]?$', '(?:Group|Comm)_size$']

code = {\
    '': '\tprintf("Unsupported MPI call: %s\\n", __func__);\n\texit(1);',\
    'MPI_Init': '\treturn PMPI_Init(argc, argv);\n',\
    'MPI_Finalize': '\treturn PMPI_Finalize();\n'\
    }

resp = urllib2.urlopen(website_root)
main_html = resp.read()

# parse all links
fns = {}
for m in re.finditer('<\s*a\s+href\s*=\s*"([^"]*)"\s*>([^<]*)<\s*/\s*a\s*>', main_html):
    if not m.group(1).startswith('man3/') or not m.group(2).startswith('MPI_'):
        continue

    if re.search('(?:' + '|'.join(ignore) + ')', m.group(2)) is not None:
        continue

    # fetch link dest
    url = m.group(1)
    fname = m.group(2)
    resp = urllib2.urlopen(website_root + url)
    subhtml = resp.read()

    # search for a C syntax signature
    sm = re.search("C\s*Syntax[^#]*#include\s*&lt;mpi\.h&gt;(?P<sig>.*)<\s*/\s*pre\s*>", subhtml, re.DOTALL)
    if sm is None:
        print 'Cannot find signature for ' + fname
        continue

    # pick the full correct signature
    first_ln_found = False
    for sig in sm.group('sig').splitlines():
        sig = re.sub('(?:<tt>|</tt>|&nbsp;)', ' ', sig)
        sig += ' '
        sig = re.sub('\s+', ' ', sig)
        if fname in sig:
            fns[fname] = sig
            first_ln_found = True
            continue

        if not first_ln_found:
            continue

        # signature is already complete
        if re.search('[)]\s*$', fns[fname], re.MULTILINE) is not None:
            break

        # they forgot to close the parenthesis...
        ssm = re.match('([^<]*)<\s*/\s*pre\s*>', sig)
        if ssm is not None:
            fns[fname] += ssm.group(1).strip() + ')'
            break

        fns[fname] += sig

    if not first_ln_found:
        print 'Cannot find valid signature for ' + m.group(2)
        #print sm.group('sig').splitlines()

# print result
print '''
#include "LocalNode.h"

#include <mpi.h>
#include <cassert>
#include <cstdlib>

extern "C" {

void FoRESTmn_newIter() {
   LocalNode::getLN().newIter();
}
'''

for f, s in sorted(fns.items()):
    print ''

    # missing return type, arbitrarly use int
    if '(' in s.split()[0]:
        s = 'int ' + s

    if f in code:
        print s + ' {'
        print code[f]
    else:
        print s + ' {'
        for att in (att.split()[-1].strip().replace('*', '').replace('[]', '') for att in s[s.index('(') + 1:s.index(')')].split(',') if att.strip() != '' and att.strip() != '...'):
            print '\t(void) (' + att + ');'
        print code['']
    print '}\n'

print '}'
    
