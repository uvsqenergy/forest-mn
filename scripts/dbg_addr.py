#!/usr/bin/env python
# -*- encoding: utf8 -*-

import sys, subprocess as sp, re, os

if len(sys.argv) < 3:
    print 'Displays the source code for the given symbol in the file'
    print 'Usage: {0} bin_file symbol'.format(sys.argv[0])
    sys.exit(1)

ex = sp.Popen(['objdump', '-d', '-l', '-S', sys.argv[1]], stdout=sp.PIPE)
data = ex.communicate()[0]

symb = sys.argv[2].split('+')[0]
if '+' in sys.argv[2]:
    off = int(sys.argv[2].split('+')[1], 16)
else:
    off = 0;

lastUsrFile = ''
inAsm = False
infn = False
for ln in data.splitlines():

    # check if we are in the function
    if not infn:
        m = re.match('([0-9a-f]+)\s+<' + re.escape(symb) + '>:\s*', ln)
        if m is not None:
            infn = True
            src = ''
            saddr = int(m.group(1), 16)
            addr = saddr
        continue

    # is it an assembly line
    m = re.match('\s*([0-9a-f]+):\s*(?:[0-9a-f][0-9a-f]\s)+', ln)
    if m is not None:
        # remember where we are
        inAsm = True
        addr = int(m.group(1), 16)
            
        # are we done?
        if addr == saddr + off:
            print 'Found ' + hex(addr) + ':'
            print src
            print 'Last user file reference: ' + lastUsrFile
            sys.exit(0)

        continue

    # we are in a source block, store it and, if needed, forget about the older one
    if inAsm:
        inAsm = False
        src = ''

    # remember the last path that does not look like a system file
    m = re.match('((?:{0}[^{0}]*)+):[0-9]+'.format(os.sep), ln)
    if m is not None and not any([m.group(1).startswith(p) for p in ['/usr/include']]):
        lastUsrFile = ln

    src += ln + '\n'

