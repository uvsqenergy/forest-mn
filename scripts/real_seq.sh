#!/bin/bash

echo "This script is using the tracing/seq_comm_*.txt files generated with the
debug mode of FoREST-mn. Make sure to have run something in debug mode after
having cleaned the tracing directory"

echo "" > tracing/real_seq_0.txt
for((i=0;i<$(echo $(cat tracing/seq_comm_0_0.txt) | tr ' ' '\n' | wc -l);i++)); do
   column=i;
   cat tracing/seq_comm_0_*.txt | awk -v column=$i '{print $column}' | sort -r | head -n 1 | tr '\n' ' ' >> tracing/real_seq_0.txt
done;
echo "" >> tracing/real_seq_0.txt

echo "" > tracing/real_seq_1.txt
for((i=0;i<$(echo $(cat tracing/seq_comm_0_0.txt) | tr ' ' '\n' | wc -l);i++)); do
   column=i;
   cat tracing/seq_comm_1_*.txt | awk -v column=$i '{print $column}' | sort -r | head -n 1 | tr '\n' ' ' >> tracing/real_seq_1.txt
done;
echo "" >> tracing/real_seq_1.txt


