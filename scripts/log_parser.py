#!/usr/bin/env python

import re, sys, argparse, os

aparser = argparse.ArgumentParser()
aparser.add_argument('--list-freqs', '--lf', action="store_true", help="Lists the frequencies read in the log file")
aparser.add_argument('--freq', '-f', nargs=1, type=int, help="Only ouput the data for the given frequency")
aparser.add_argument('--list-pids', '--lp', action="store_true", help="Lists the pids found in the file")
aparser.add_argument('--pid', '-p', nargs=1, type=int, help="Only output the data for a specified pid")
aparser.add_argument('--nb-repets', '--nr', action="store_true", help="Prints the number of repeted execution found in the log")
aparser.add_argument('--repet', '-r', nargs=1, type=int, help="Repetition to extract")
aparser.add_argument('--output', '-o', nargs=1, help="Directory where to write the extracted the log", default="FoREST_logs")
aparser.add_argument('file', nargs=1, type=file, help="Log file", metavar='FILE')
args = vars(aparser.parse_args())

if args['list_freqs']:
    freqs = set()
    for ln in args['file'][0]:
        m = re.search('Setting frequency \d+: (\d+)$', ln)
        if m is not None:
            freqs.add(int(m.group(1)))

    print "Frequencies in log:"
    for f in sorted(list(freqs)):
        print str(f) + " (" + str(f / 1e6) + " GHz)"

    sys.exit(0)

if args['list_pids']:
    pids = set()
    for ln in args['file'][0]:
        m = re.search("^[IWE]\d\d\d\d\s+\S+\s+(\d+)\s+\S+:\d+\]", ln)
        if m is not None:
            pids.add(int(m.group(1)))

    print "Pids in log:"
    for nid, n in enumerate(sorted(list(pids))):
        print str(nid) + "\t" + str(n)

    sys.exit(0)

if args['nb_repets']:
    nb_rep = 0
    for ln in args['file'][0]:
        m = re.search("Initializing step Learning$", ln)
        if m is not None:
            nb_rep += 1

    print "Nb repetitions in the log: " + str(nb_rep)
    sys.exit(0)

outdir = args['output']

freq = {}   # one frequency per pid
pid = -1
nrep = -1
data = []   # data[nrep][pid][freq] = sorted list of log entries

for ln in args['file'][0]:
    m = re.search("Initializing step Learning$", ln)
    if m is not None:
        nrep += 1
        data.append({})
        continue
    
    # cannot classify that
    if nrep < 0:
        continue

    # not the requested repetition
    if args['repet'] is not None and nrep != args['repet'][0]:
        continue

    # get the pid if possible
    m = re.search("^[IWE]\d\d\d\d\s+\S+\s+(\d+)\s+\S+:\d+\]", ln)
    if m is None:   # invalid line
        continue
    
    pid = int(m.group(1))

    # not the requested pid
    if args['pid'] is not None and pid != args['pid'][0]:
        continue
    
    # first time we meet you, pid
    if pid not in data[-1]:
        data[-1][pid] = {}

    # frequency change
    m = re.search('Setting frequency \d+: (\d+)$', ln)
    if m is not None:
        freq[pid] = int(m.group(1))

        # not the requested frequency
        if args['freq'] is not None and freq[pid] != args['freq'][0]:
            continue

        # first time we see the frequency
        if freq[pid] not in data[-1][pid]:
            data[-1][pid][freq[pid]] = [ln]
        continue

    # cannot classify that
    if pid not in freq:
        continue

    # not the requested frequency
    if args['freq'] is not None and freq[pid] != args['freq'][0]:
        continue

    data[-1][pid][freq[pid]].append(ln)

# for all repetitions
for nrep, itr in enumerate(data):
    # for all pids
    for pid, itp in itr.items():
        # for all frequencies
        for freq, itf in itp.items():
            tmpoutdir = os.path.join(outdir, str(nrep), str(freq))
            out = os.path.join(tmpoutdir, str(pid))

            try:
                os.makedirs(tmpoutdir)
            except:
                pass

            fd = open(out, "w")
            fd.write(''.join(itf))
            fd.flush()
            fd.close
