#include <cstdlib>
#include <iostream>
#include <cmath>
#include <mpi.h>

extern "C" {
void FoRESTmn_newIter();
void FoRESTmn_endLoop();
}

double a, b, c;

int main(int argc, char *argv[]) {
   int rank;
   int recv;
   int send;
   MPI_Request req, req2;
   MPI_Status status;

   MPI_Init (&argc, &argv);
   MPI_Comm_rank (MPI_COMM_WORLD, &rank);
   int other = rank == 1 ? 0 : 1;

   MPI_Send_init (&send, 1, MPI_INT, other, 0, MPI_COMM_WORLD, &req);
   MPI_Recv_init (&recv, 1, MPI_INT, other, 0, MPI_COMM_WORLD, &req2);

   // Start loop
   for (unsigned int i = 0; i < 50; i++) {
      std::cout << "FoREST new iter" << std::endl;
      FoRESTmn_newIter();

      std::cout << "First computation loop" << std::endl;
      for (unsigned int j = 0; j < 1000000; ++j) a += sqrt(j);
   
      MPI_Barrier(MPI_COMM_WORLD);
      if (rank == 0) {
         send = 5;
         MPI_Start (&req);
         MPI_Start (&req2);
      } else {
         send = 8;
         MPI_Start (&req2);
         MPI_Start (&req);
      }

      std::cout << "#" << rank << ": Sending " << send << std::endl;

      for (unsigned int j = 0; j < 1000000; ++j) b += sqrt(j + a);

      std::cout << "#" << rank << ": Now waiting for requests to be done" << std::endl;

      if (rank == 0) {
         std::cout << "#" << rank << ": Waiting for send" << std::endl;
         MPI_Wait (&req, &status);
         std::cout << "#" << rank << ": Waiting for recv" << std::endl;
         MPI_Wait (&req2, &status);
         std::cout << "#" << rank << ": Passed" << std::endl;
      } else {
         std::cout << "#" << rank << ": Waiting for recv" << std::endl;
         MPI_Wait (&req2, &status);
         std::cout << "#" << rank << ": Waiting for send" << std::endl;
         MPI_Wait (&req, &status);
         std::cout << "#" << rank << ": Passed" << std::endl;
      }

      std::cout << "#" << rank << ": Received " << recv << std::endl;
           
      for (unsigned int j = 0; j < 1000000; ++j) c += sqrt(j + b);
     
      std::cout << "Barrier";
      MPI_Barrier (MPI_COMM_WORLD);

      std::cout << i << std::endl;
   }
   std::cout << "endloop" << std::endl;
   FoRESTmn_endLoop();

   MPI_Request_free (&req);
   MPI_Request_free (&req2);

   std::cout << "Finalize" << std::endl;
   MPI_Finalize();
   std::cout << "END" << std::endl;

   return (int) c;
}
