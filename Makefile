CXX=g++
MPICXX=mpic++
MPICXX_FLAGS=-msse4 -Wall -Wextra -fPIC -I.
MPICXX_FLAGS_DEBUG=-O2 -g -DTRACING
MPICXX_FLAGS_RELEASE=-O3 -DNDEBUG
LD=$(CXX)
MPILD=$(MPICXX)
MPILD_FLAGS=-msse4 -fPIC -shared -lglog -lpfm -ldl -ldvfs -Llibdvfs/ -lrt
MPILD_FLAGS_DEBUG=-O2 -g -lunwind
MPILD_FLAGS_RELEASE=-O3
OUTPUT=libFoRESTmn.so

EXCEPT="Offline.*"
HDR=$(shell find src -type f -name '*.h' | grep -v $(EXCEPT))
SRC=$(shell find src -type f -name '*.cpp' | grep -v $(EXCEPT))
OFFLINE_HDR=$(shell find src/offline -type f -name '*.h')
OFFLINE_SRC=$(shell find src/offline -type f -name '*.cpp')
OBJ_RELEASE=$(addprefix obj/, $(patsubst %.cpp, %.o, $(SRC)))
OBJ_DEBUG=$(addprefix obj/, $(patsubst %.cpp, %_debug.o, $(SRC)))

ALL_OBJ=$(OBJ_RELEASE) $(OBJ_DEBUG)

.PHONY: debug release clean distclean api libdvfs doc comm_ovhd

all: libdvfs api src/offline/add lPowerProbe/lPowerProbe debug

src/offline/add:
	$(CXX) -O3 -Wall -Wextra src/offline/add.c -o $@

lPowerProbe/lPowerProbe:
	$(MAKE) -C lPowerProbe/

offline: lPowerProbe/lPowerProbe
	$(MPICXX) -msse4 -Wall -Wextra -I. $(MPICXX_FLAGS_DEBUG) $(OFFLINE_SRC) src/NodeUtils.cpp src/DVFSUnitUtils.cpp src/ThreadUtils.cpp src/HWCounter.cpp -lglog -lpfm -ldl -ldvfs -Llibdvfs/ -lrt -o offline -Llibdvfs/
	@echo "Super-user privileges are required to perform offline computation."
	sudo ./offline

api:
	$(MAKE) -C API/

comm_ovhd:
	$(MAKE) -C CommOvhd

libdvfs:
	$(MAKE) -C libdvfs/

release: $(OBJ_RELEASE) libdvfs api lPowerProbe/lPowerProbe offline
	$(MPILD) $(MPILD_FLAGS) $(MPILD_FLAGS_RELEASE) $(OBJ_RELEASE) -o $(OUTPUT)

debug: $(OBJ_DEBUG) libdvfs api lPowerProbe/lPowerProbe offline
	$(MPILD) $(MPILD_FLAGS) $(MPILD_FLAGS_DEBUG) $(OBJ_DEBUG) -o $(OUTPUT)

test: debug test.cpp
	$(MPICXX) -O3 -g -Wall -Wextra -msse4 -I /usr/include/mpi -c test.cpp -o obj/test.o
	$(MPILD) -O3 -g -lm -lFoRESTmn -L. -lglog -lpfm -ldvfs -Llibdvfs/ obj/test.o -o $@

doc:
	doxygen forest-mn.doxy

clean:
	rm -rf obj
	rm -rf tracing/*.txt
	$(MAKE) -C API/ clean
	$(MAKE) -C libdvfs/ clean

distclean: clean
	rm -f $(OUTPUT)
	rm -rf offline
	rm -rf power.cfg
	rm -rf doc
	$(MAKE) -C API/ distclean
	$(MAKE) -C libdvfs/ distclean

obj/%.d: %.cpp
	mkdir -p $(@D)
	$(MPICXX) -MM $(MPICXX_FLAGS) $< -MF $@

#-include $(patsubst %.o, %.d, $(ALL_OBJ))

obj/%_debug.o: %.cpp obj/%.d
	@echo '$(MPICXX) $(MPICXX_FLAGS) $(MPICXX_FLAGS_DEBUG) $<'
	@mkdir -p $(@D)
	@$(MPICXX) $(MPICXX_FLAGS) $(MPICXX_FLAGS_DEBUG) -c $< -o $@

obj/%.o: %.cpp obj/%.d
	@echo '$(MPICXX) $(MPICXX_FLAGS) $(MPICXX_FLAGS_RELEASE) $<'
	@mkdir -p $(@D)
	@$(MPICXX) $(MPICXX_FLAGS) $(MPICXX_FLAGS_RELEASE) -c $< -o $@

