#include <iostream>
#include <cstdlib>
#include <mpi.h>
#include <fstream>

#include "../src/Common.h"

#define NREPET 10

int main(int argc, char **argv) {
   int mpi_csize, mpi_crank;
   uint64_t ovhd = 0;

   if (argc > 1) {
      std::cout << "Call this program with mpirun with an arbitrary number of processors to evaluate the communication cost with this number of nodes." << std::endl;
      return EXIT_SUCCESS;
   }

   MPI_Init(&argc, &argv);

   MPI_Comm_size(MPI_COMM_WORLD, &mpi_csize);
   MPI_Comm_rank(MPI_COMM_WORLD, &mpi_crank);

   MPI_Barrier(MPI_COMM_WORLD);

   for (unsigned int i = 0; i < NREPET; i++) {
      uint64_t start = get_time(), stop;
   
      MPI_Barrier(MPI_COMM_WORLD);

      stop = get_time();
      ovhd += stop - start;
   }

   ovhd /= NREPET;

   if (mpi_crank == 0) {
      std::ofstream fs("comm_ovhd.dat", std::ios_base::app | std::ios_base::out);

      std::cout << "Overhead for " << mpi_csize << " nodes: " << ovhd << std::endl;

      fs << mpi_csize << ' ' << ovhd << ' ';
      fs.flush();
      fs.close();
   }

   MPI_Finalize();
   return EXIT_SUCCESS;
}
