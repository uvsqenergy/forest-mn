### What is FoREST-mn? ###

FoREST-mn is a new runtime DVFS controller developped at the University of Versailles, France. It is able to exploit the low CPU usage and communication slack of parallel and iterative programs using to achieve significant energy savings, given a customizable slowdown constraint. FoREST-mn is a research effort aiming at improving energy efficiency with balanced workloads, where communication slack is negligible, by slowing down the program in a controllable way.

### About this repository ###

This repository contains the source code of the experimental program used to investigate our approach. It is built for MPI communication and consists of a dynamic library overloading MPI calls. It works with the C and Fortran MPI function calls. Note this is experimental software, and should be used as such.

You will find the main software in the *master* branch, a re-implementation of the Adagio DVFS controller (Roundtree et al., ICS'09)  for comparison purposes in the *adagio* branch, and FoREST-mn evaluation results in the *Downloads* section.

### Building ###

After downloading the archive, you must download dependent git submodules using:
```
#!bash
git submodule update --init
```

Then use make to build the program. Note the building process will require administrative privileges to run a benchmark program. This benchmark phase will generate a configuration file for future use by the tool.

### Using FoREST-mn with your programs ###

To use FoREST-mn with your MPI, iterative programs, you must annotate your program with function calls defining the iterative loop. FoREST-mn uses these function calls to determine the loop edge. The structure of the annotation is as follows:

```
#!c++
while (iterative_condition) {
   FoRESTmn_newIter ();
}
FoRESTmn_endLoop ();
```

```
#!Fortran
do it 1,1
   call FoRESTmn_newIter ()
enddo
call FoRESTmn_endLoop ()
```
To build the program, please add the MPI hook library defined in ~/forest-mn/API/libforestmn_api.so as described below:

```
#!bash
mpicc my_program.c -L~/forest-mn/API/ -lforestmn_api -o my_program
```
Launching the program with the command "mpirun -np 8 ./my_program" will not launch the program with the FoREST-mn library. In fact, the hook MPI library used earlier is an empty library built-in to allow compilation of programs with the above-mentioned annotation. To effectively launch FoREST-mn on your program, you will need to use the following command:

```
#!bash
LD_PRELOAD="path_to_forest/forest-mn/libFoRESTmn.so" mpirun -np 8 ./my_program
```

### Submodules information ###

Information about the submodules used in FoREST-mn are listed below.

* lPowerProbe: A simple tool to analyse the performance of a program. We have been using it to measure the energy efficiency and the slowdown constraint fulfilment in our tool. https://code.google.com/p/lpowerprobe/
* libdvfs: A minimal library giving facilities to easily change the frequency of a processor. https://code.google.com/p/libdvfs/