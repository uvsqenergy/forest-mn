#ifndef FOREST_MN_API_H
#define FOREST_MN_API_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Call this function at every iteration of a loop to inform FoREST-mn that
 * a new loop iteration just started.
 */
void FoRESTmn_newIter();
void forestmn_newiter_();

/**
 * Call this function when exiting an instrumented loop.
 */
void FoRESTmn_endLoop();
void forestmn_endloop_();

#ifdef __cplusplus
}
#endif

#endif
